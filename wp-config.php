<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
define('PRODUCTION_MODE_IS_ACTIVE', false);
define('DEV_MODE', true);


if (PRODUCTION_MODE_IS_ACTIVE) {
    /** The name of the database for WordPress */
    define('DB_NAME', 'iteambiz_kreatize_demo');

    /** MySQL database username */
    define('DB_USER', 'iteambiz_kreatize_demo');

    /** MySQL database password */
    define('DB_PASSWORD', '!8O7(I}J(EE[');
} else {
    /** The name of the database for WordPress */
    define('DB_NAME', 'iteambiz_kreatize_demo');

    /** MySQL database username */
    define('DB_USER', 'iteambiz_kreatize_demo');

    /** MySQL database password */
    define('DB_PASSWORD', '!8O7(I}J(EE[');
}


/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'gr5nn59arsrcrwl1n66bxxbzcuog8kdeohxmz1qnziyhtqxaokxscng6enl5d93t');
define('SECURE_AUTH_KEY',  'yxlhcf1kj43tmpgucvzlyjrgs8z2wau1e4jceymfw9j2tputdujwyiptr3jcccnb');
define('LOGGED_IN_KEY',    'vxjzdq7uqqzyr4s20ta0ii5alxtdnmn7xm3kxe2tgqcgtvtyxnstfd7exmnpzpe5');
define('NONCE_KEY',        '6y4ssk1xs8xzfwcx5fy3hgwhwlcwsc2comxd6o2iraf8pdt6lote7fhkvu97uxlt');
define('AUTH_SALT',        'bbxpprg4xieifeeafmra4jxrb6ci8meuzmmrnx4stzg3j5rtwxtg7s1g9ovw8xvd');
define('SECURE_AUTH_SALT', 'me8gdmk7njcvspai6f7przx0733rsjryrmbymyxpbgdvvpnvvarvt4dztggix7cq');
define('LOGGED_IN_SALT',   '21kbfsmss5qj32ymizc9y9lis3li2fitdhp0aqaoukvccpeh2jgrlkygymgbrzqd');
define('NONCE_SALT',       'vhclm8qikau9gme4tkilo8fdx01eybjuti3jhi0b1ojcpq02alqbqpn0ksfcz2za');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp4c_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);
define( 'WP_DEBUG_LOG', true );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
