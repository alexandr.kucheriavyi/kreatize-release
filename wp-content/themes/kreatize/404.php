<?php
$lang = WPGlobus::Config()->language;

?>

<?php get_header(); ?>
<div id="content" class="thk">
    <section class="container-fluid">
        <div class="container">
            <div class="col-md-12 ">
                <h1 class="cl-titil-40 text-center"><?php echo WPGlobus_Core::text_filter('{:en}404 page not found{:}{:de}404 Seite nicht gefunden{:}', $lang)?></h1>
            </div>
            <div class="col-md-6 col-md-offset-2 col-sm-8 col-xs-8 thk-block">
                <p class="cl-titil-20"><?php echo WPGlobus_Core::text_filter('{:en}We’re Sorry. The page you’re looking for could not be found. {:}{:de}Die gewünschte Seite wurde leider nicht gefunden{:}', $lang)?></p>
                <p class="cl-titil-20"><strong>Ihr KREATIZE-Team</strong></p>
            </div>
        </div>
    </section>
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
