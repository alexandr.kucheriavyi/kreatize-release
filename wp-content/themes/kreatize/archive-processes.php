<?php
require_once 'core/App.php';
use kreatize\App as App;
$kreatize = App::getInstance();
$pageOptions = $kreatize->service('ProcessArchivePageOptions');
?>

<?php get_header(); ?>



    <div id="content" class="process">
        <section class="manu-process">
            <div class="container">
                <h1 class="text-center cl-titil-40"><?php echo $pageOptions->archiveName(); ?></h1>
                <h2 class="text-center cl-titil-20"><?php echo $pageOptions->archiveTitle(); ?></h2>
                <div role="tabpanel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs nav-pills" role="tablist">
                        <li role="presentation">
                            <a href="#panel-1"  id="additive" class="cl-titil-28 rolling unsere-panel" aria-controls="panel-1" role="tab" data-toggle="tab"><?php echo $pageOptions->listTagName(1); ?></a>
                        </li>
                        <li role="presentation">
                            <a href="#panel-2" id="CNC" class="cl-titil-28 rolling unsere-panel" aria-controls="panel-2" role="tab" data-toggle="tab"><?php echo $pageOptions->listTagName(2); ?></a>
                        </li>
                        <li role="presentation" class="guss">
                            <a href="#panel-3" class="cl-titil-28" aria-controls="panel-3" role="tab" data-toggle="tab"><?php echo $pageOptions->listTagName(3); ?></a>
                        </li>
<!--                        <li role="presentation" class="active" class="alle">-->
<!--                            <a href="#panel-4" class="cl-titil-28" aria-controls="panel-4" role="tab" data-toggle="tab">--><?php //echo $pageOptions->titleForAllItemsTab(); ?><!--</a>-->
<!--                        </li>-->
                    </ul>
                    <!-- Tab panels -->
                    <div class="tab-content">
                        <!--start of tab 1-->
                        <div role="tabpanel" class="tab-pane" id="panel-1">
                            <div class="row masonry-container">
                                <?php $queryFirst = $pageOptions->listQueryByTag(1); ?>
                                <?php if ($queryFirst->have_posts()) {?>

                                    <?php while ($queryFirst->have_posts() ) { $queryFirst->the_post(); ?>
                                        <a class="process-link" href="<?php echo get_permalink();?>">
                                            <div class="col-md-3 col-sm-6 item">
                                                <?php the_post_thumbnail( 'full' ); ?>
                                                <h3 class="cl-titil-16-22"><?php the_title(); ?></h3>
                                            </div>
                                        </a>
                                    <?php } ?>
                                <?php }?>

                            </div><!--End masonry-container  -->
                        </div><!--End panel-1  -->

                        <div role="tabpanel" class="tab-pane" id="panel-2">

                            <div class="row masonry-container">
                                <?php $querySecond = $pageOptions->listQueryByTag(2); ?>
                                <?php if ($querySecond->have_posts()) {?>

                                    <?php while ($querySecond->have_posts() ) { $querySecond->the_post(); ?>
                                        <a class="process-link" href="<?php echo get_permalink();?>">
                                            <div class="col-md-3 col-sm-6 item">
                                                <?php the_post_thumbnail( 'full' ); ?>
                                                <h3 class="cl-titil-16-22"><?php the_title(); ?></h3>
                                            </div>
                                        </a>
                                    <?php } ?>
                                <?php }?>
                            </div><!--End masonry-container  -->
                        </div><!--End panel-2  -->

                        <div role="tabpanel" class="tab-pane" id="panel-3">
                            <div class="row masonry-container">
                                <?php $queryThird = $pageOptions->listQueryByTag(3); ?>
                                <?php if ($queryThird->have_posts()) {?>

                                    <?php while ($queryThird->have_posts() ) { $queryThird->the_post(); ?>
                                        <a class="process-link" href="<?php echo get_permalink();?>">
                                            <div class="col-md-3 col-sm-6 item">
                                                <?php the_post_thumbnail( 'full' ); ?>
                                                <h3 class="cl-titil-16-22"><?php the_title(); ?></h3>
                                            </div>
                                        </a>
                                    <?php } ?>
                                <?php }?>
                            </div><!--End masonry-container  -->
                        </div><!--End panel-4  -->
                        <div role="tabpanel" class="tab-pane active" id="panel-4">
                            <div class="row masonry-container">

                                <?php $queryFirst = $pageOptions->listQueryByTag(1); ?>
                                <?php if ($queryFirst->have_posts()) {?>

                                    <?php while ($queryFirst->have_posts() ) { $queryFirst->the_post(); ?>
                                        <a class="process-link" href="<?php echo get_permalink();?>">
                                            <div class="col-md-3 col-sm-6 item">
                                                <?php the_post_thumbnail( 'full' ); ?>
                                                <h3 class="cl-titil-16-22"><?php the_title(); ?></h3>
                                            </div>
                                        </a>
                                    <?php } ?>
                                <?php }?>

                                <!--cnc-->
                                <?php $querySecond = $pageOptions->listQueryByTag(2); ?>
                                <?php if ($querySecond->have_posts()) {?>

                                    <?php while ($querySecond->have_posts() ) { $querySecond->the_post(); ?>
                                        <a class="process-link" href="<?php echo get_permalink();?>">
                                            <div class="col-md-3 col-sm-6 item cnc">
                                                <?php the_post_thumbnail( 'full' ); ?>
                                                <h3 class="cl-titil-16-22"><?php the_title(); ?></h3>
                                            </div>
                                        </a>
                                    <?php } ?>
                                <?php }?>

                                                <!--guss-->
                                <?php $queryThird = $pageOptions->listQueryByTag(3); ?>
                                <?php if ($queryThird->have_posts()) {?>

                                    <?php while ($queryThird->have_posts() ) { $queryThird->the_post(); ?>
                                        <a class="process-link" href="<?php echo get_permalink();?>">
                                            <div class="col-md-3 col-sm-6 item guss">
                                                <?php the_post_thumbnail( 'full' ); ?>
                                                <h3 class="cl-titil-16-22"><?php the_title(); ?></h3>
                                            </div>
                                        </a>
                                    <?php } ?>
                                <?php }?>

                                                <?php wp_reset_postdata();?>
                            </div><!--End masonry-container  -->
                        </div><!--End panel-4 -->
                    </div><!--End tab-content  -->
                </div><!--End tabpanel  -->
            </div>
        </section>
    </div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>