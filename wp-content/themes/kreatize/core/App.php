<?php
namespace kreatize;

class App {
    static private $_instance = null;
    public $controller;
    public $textDomain = 'kreatize';

    const VERSION = '1.0.0';

    private $_initAbstracts = array(
        'service'
    );

    private $_initObjects = array(
        'facade',
        'controller'
    );

    private function __construct()
    {
        $this->_onInit();
    } // end __construct

    private function _onInit()
    {
        $this->_onInitAbstractClasses();
        $this->_onInitObjects();
    } // end _onInit

    private function _onInitAbstractClasses()
    {
        $namespace = __NAMESPACE__.'\\abstracts';

        foreach ($this->_initAbstracts as $name) {
            $className = ucfirst($name);
            $this->_onInitClass($className, $namespace, 'abstracts');
        }
    } // end _onInitAbstractClasses

    private function _onInitObjects()
    {
         foreach ($this->_initObjects as $name) {
             $this->_initCoreObject($name);
         }
    } // end _onInitObjects

    public static function run()
    {
        if (self::$_instance) {
            throw new \Exception('Instance has runed');
        }

        self::$_instance = new self();
    } // end run

    public static function getInstance()
    {
        if (!self::$_instance) {
            throw new \Exception('Instance has not runed');
        }

        return self::$_instance;
    } // end getInstance


    private function __clone()
    {
    } // end __clone

    private function _initCoreObject($name)
    {
        $name = lcfirst($name);

        if (isset($this->$name)) {
            return false;
        }

        $this->$name = $this->_getCoreObject($name);
    } // end _initCoreObject

    public function version()
    {
        return self::VERSION;
    } // end version

    public function _getCoreObject($name)
    {
        $className = ucfirst($name);

        $class = $this->_onInitClass($className);

        return new $class($this);
    } // end _getCoreObject

    public function service($name, $params = array())
    {
        $className = ucfirst($name);
        $namespace = __NAMESPACE__.'\\service';

        $class = $this->_onInitClass($className, $namespace, 'service');

        if ($params) {
            return new $class($this, $params);
        } else {
            return new $class($this);
        }
    } // end service

    private function _onInitClass($className, $namespace = '', $path ='')
    {
        if (!$namespace) {
            $namespace = __NAMESPACE__;
        }

        $class = $namespace . '\\' . $className;

        if (class_exists($class)) {
            return $class;
        }

        $path = ($path) ? $path . '/' : '';

        $file = $path . $className . '.php';

        require_once $file;

        if (!class_exists($class)) {
            $message = 'Not found class ' . $class . ' in  file ' . $file;
            throw new \Exception($message);
        }

        return $class;

    } // end _onInitModel

    public function render($template, $vars = array())
    {
        $data = $vars;

        if ($vars) {
            extract($vars);
        }

        $path = $this->_getTemplatePath($template);

        ob_start();

        include $path;

        $content = ob_get_clean();

        return $content;
    } // end render

    private function _getTemplatePath($template = '')
    {
        return get_stylesheet_directory().'/core/views/'.$template;
    } // end _getTemplatePath
}