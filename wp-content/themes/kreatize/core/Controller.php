<?php
namespace kreatize;

class Controller {
    protected $app;

    public function __construct(&$app)
    {
      $this->app = $app;

      $this->_onInit();
    } // end __construct

    private function _onInit()
    {
        $this->_onInitCommon();

        if ($this->app->facade->isBackend()
            || $this->app->facade->isAjax()) {
            $this->_onInitBackend();
        }

        if ((!$this->app->facade->isBackend()
            || $this->app->facade->isAjax())
            && !$this->app->facade->isLoginPage()) {
            $this->_onInitFrontend();
        }


    } // and _onInit

    private function _onInitCommon()
    {
        $this->app->service('processesPostType')->init();
    } // end _onInitCommon

    private function _onInitBackend()
    {
        $this->app->service('optionsMigrator')->init();
        $this->app->service('optionsSwitcher')->init();
        $this->app->service('optionTree')->init();
        $this->app->service('options')->init();
        $this->app->service('css')->addOptionPageCss();
        $this->app->service('js')->addOptionPageJs();
        $this->app->service('js')->addBackendJs();
    } // end _onInitBackend

    public function _onInitFrontend()
    {
        $this->app->service('pressFileDownloader')->init();
        $this->app->service('contactForm')->init();
        $this->app->service('css')->addCommonCss();
        $this->app->service('js')->addCommonJs();
    } // end _onInitFrontend
}