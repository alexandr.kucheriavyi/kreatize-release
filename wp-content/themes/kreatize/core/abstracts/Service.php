<?php
namespace kreatize\abstracts;

abstract class Service
{
    protected $app;
    protected $params;

    public function __construct(&$app, $params = array())
    {
        $this->app = $app;
        $this->params = $params;
        $this->onInit();
    }

    protected function onInit()
    {
    } // end onInit
}