(function($){
    var KreatizeOptionPage = {
        tableRender: {
            start: function (id) {
                return '<table id="' + id + '_editableTable" class="table editable-table">';
            },

            head: function (row) {
                var content = '<thead><tr>';

                $.each(row, function (i, val) {
                    content += '<th class="editable-col" data-row="0" data-col="'+i+'">' + val + '</th>';
                })

                content += '</tr></thead>';

                return content;
            },

            body: function (rows) {
                var content = '<tbody>';

                $.each(rows, function (i, row) {
                    if (i == 0) {
                        return;
                    }

                    content += '<tr>';

                    var idRow = i;

                    $.each(row, function (i, val) {
                        content += '<td class="editable-col" data-row="'+idRow+'" data-col="'+i+'">' + val + '</td>';
                    })

                    content += '</tr>';
                })

                content += '</tbody>';

                return content;
            },

            end: function () {
                return '</table>';
            },
        },

        init: function () {
            var self = KreatizeOptionPage;

            self.initEditableTableFields();
            // var data = $('.text_type_editable_table').val();
            // var rows = jQuery.parseJSON(data);
           //  var rows = [
           //      {
           //          0 : '',
           //          1 : 'Selektives Lasersintern',
           //          2 : 'CNC-Fräsen',
           //          3 : 'Vorteile des Selektiven Lasersintern'
           //      },
           //      {
           //          0 : 'Werkstoff',
           //          1 : 'PA12',
           //          2 : 'PVC',
           //          3 : ''
           //      },
           //      {
           //          0 : 'Lieferzeit',
           //          1 : '2 Tage',
           //          2 : '3 Wochen',
           //          3 : '90% Zeitersparnis'
           //      },
           //      {
           //          0 : 'Gesamtkosten',
           //          1 : '€ 340',
           //          2 : '€ 600',
           //          3: '43% Kostenersparnis'
           //      }
           //  ];

           // self.object.row.push('sdsdf');

           //string = JSON.stringify(rows);
           //  console.log(rows);
        },

        initEditableTableFields: function () {
            var self = KreatizeOptionPage;

            $( ".text_type_editable_table" ).each(function( index ) {
              self.createTable(this);
            });

            $('body').on('dblclick', '.editable-col', self.onDisplayInput)
            $('body').on('click', '.editable-col', self.onHideInputByClick)
            $('body').on('change', '.editable-col-field', self.onSaveFieldValue)
            $('body').on('focusout', '.editable-col-field', self.onHideInput)
        },

        onDisplayInput: function () {
            var self = KreatizeOptionPage;
            if ($(this).find('.editable-col-field').length) {
                return false;
            }

            if ($('.editable-col-field').length) {
                var parent = $('.editable-col-field').parent();
                $(parent).html($('.editable-col-field').val());
            }

            var value = $(this).html();
            var idRow = $(this).attr('data-row');
            var idCol = $(this).attr('data-col');

            var field = '<input value="'+value+'" type="text" data-row-field="'+idRow+'" data-col-field="'+idCol+'"  class="widefat option-tree-ui-input editable-col-field">';
            $(this).html(field);
            $(this).find('.editable-col-field').focus();
        },

        onHideInput: function () {
            var self = KreatizeOptionPage;
            var parent = $('.editable-col-field').parent();
            $(parent).html($('.editable-col-field').val());
        },

        onHideInputByClick: function () {
            var self = KreatizeOptionPage;
            var fieldSelector = $(this).find('.editable-col-field');

            if (fieldSelector.length) {
                return true;
            }

            var parent = $('.editable-col-field').parent();
            $(parent).html($('.editable-col-field').val());
        },

        onSaveFieldValue: function () {
            var self = KreatizeOptionPage;

            var parentTable = $(this).parents('.editable-table');
            var tableID = $(parentTable).attr('id');
            var formFieldID = tableID.replace("_editableTable","");

            self.onHideInput();

            var rows = {};
            var cols = {};
            var currentRow = 0;
            $('#'+tableID+' .editable-col').each(function( index ) {
                var row = $(this).attr('data-row');
                var col = $(this).attr('data-col');

                console.log(typeof(rows[row]));

                if (typeof(rows[row]) == "undefined") {
                    rows[row] = {};
                }

                rows[row][col] = $(this).html();
            });

            string = JSON.stringify(rows);

            $('#'+formFieldID).val(string);
        },

        createTable: function (obj) {
            var self = KreatizeOptionPage;

            var data = $(obj).val();
            var id = $(obj).attr('id');
            var rows = {};

            if (!data) {
                return false;
            }


            try
            {
                rows = $.parseJSON(data);
            }
            catch(e)
            {
                var tableSize = data.split('x');

                if ( typeof tableSize === 'string' ) {
                    return false;
                }

                if (tableSize.length != 2) {
                    return false;
                }

                for (var row = 0; row < tableSize[0]; row++) {
                    if (typeof(rows[row]) == "undefined") {
                        rows[row] = {};
                    }

                    for (var col = 0; col < tableSize[1]; col++) {
                        rows[row][col] = '-';
                    }
                }
            }

            console.log(rows);



            var content = '';
            content += self.tableRender.start(id);
            content += self.tableRender.head(rows[0]);
            content += self.tableRender.body(rows);
            content += self.tableRender.end(rows);

            $(content).insertAfter(obj);
        },
    }

    $(document).ready(function () {
        KreatizeOptionPage.init();
    })
})(jQuery);