(function($){
    $(document).ready(function (){
        $('.download-file').on('click', function () {
            var url = $(this).attr('data-file-url');

            window.location.replace(url);

            return false;
        })

        $('form #files').attr('onchange', 'changes();');
    })



})(jQuery);