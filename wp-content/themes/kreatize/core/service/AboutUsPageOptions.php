<?php
namespace kreatize\service;
require_once 'Options.php';

class AboutUsPageOptions extends Options
{
    private $_optionIdent = 'about_us_page';
    private $_values = array();

    protected function onInit()
    {
        $this->_values = $this->getFrontendOption($this->_optionIdent);
    } // end onInit

    public function headerImage()
    {
        return $this->current($this->_values['about_us_page_header_img']);
    } // end headerImage

    public function headerQuote()
    {
        return $this->current($this->_values['about_us_page_header_quote']);
    } // end headerQuote

    public function headerQuoteAuthor()
    {
        return $this->current($this->_values['about_us_page_header_quote_author']);
    } // end headerQuoteAuthor

    public function ourStoryTitle()
    {
        return $this->current($this->_values['about_us_page_our_story_title']);
    } // end ourStoryTitle

    public function ourStoryTextBlock($id)
    {
        return $this->current($this->_values['about_us_page_our_story_text_block_'.$id]);
    } // end ourStoryTextBlock

    public function ourStoryTextPhoto($id)
    {
        return $this->current($this->_values['about_us_page_our_story_photo_'.$id]);
    } // end ourStoryTextPhoto

    public function whoWeAreTitle()
    {
        return $this->current($this->_values['about_us_page_who_we_are_title']);
    } // end whoWeAreTitle

    public function whoWeAreText()
    {
        return $this->current($this->_values['about_us_page_who_we_are_text']);
    } // end whoWeAreText

    public function whoWeAreDefaultPhoto()
    {
        return $this->current($this->_values['about_us_page_who_we_are_default_photo']);
    } // end whoWeAreDefaultPhoto

    public function whoWeAreButtonText()
    {
        return $this->current($this->_values['about_us_page_who_we_are_button_text']);
    } // end whoWeAreButtonText

    public function whoWeAreButtonPage()
    {
        return $this->current($this->_values['about_us_page_who_we_are_button_page']);
    } // end whoWeAreButtonPage

    public function whoWeAreButtonUrl()
    {
        return $this->current($this->_values['about_us_page_who_we_are_button_url']);
    } // end whoWeAreButtonUrl

    public function whoWeAreApplyUrl()
    {
        $customUrl = $this->whoWeAreButtonUrl();

        if ($customUrl) {
            return $customUrl;
        }

        $pageID = $this->whoWeAreButtonPage();

        if (!$pageID) {
            return '#';
        }

        $url = $this->app->facade->getPermalink($pageID);

        return $url;
    } // end whoWeAreApplyUrl

    public function getEmployeesData()
    {
        $list = $this->current($this->_values['about_us_page_who_we_are_employees_list']);

        $this->_prepareEmployeesData($list);

        return $list;
    } // end getEmployeesData

    private function _prepareEmployeesData(&$menu)
    {
        foreach ($menu as $ident => &$item) {
            $item['url'] = '#';

            if (array_key_exists('photo', $item)
                && $item['photo'] == false) {
                $item['photo'] = $this->whoWeAreDefaultPhoto();
            }
        }
    } // end _prepareEmployeesData


    public function footerQuote()
    {
        return $this->current($this->_values['about_us_page_footer_quote']);
    } // end footerQuote

    public function footerQuoteAuthor()
    {
        return $this->current($this->_values['about_us_page_footer_quote_author']);
    } // end footerQuoteAuthor
}