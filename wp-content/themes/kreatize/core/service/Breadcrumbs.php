<?php
namespace kreatize\service;

use kreatize\abstracts\Service as Service;

class Breadcrumbs extends Service
{
    public function processPage()
    {
       $data = array(
           'homePage' => $this->_getArchivePageData(),
           'currentPage' => $this->_getCurrentPageData(),
       );

       echo $this->app->render('frontend/breadcrumbs/process_page.php', $data);
    } // end processPage

    private function _getArchivePageData()
    {
        $process = $this->app->service('processesPostType');
        $processesOptions = $this->app->service('ProcessArchivePageOptions');

        $postType = $process->getPostTypeKey();

        $data = array(
            'title' => $processesOptions->archiveName(),
            'url' => get_post_type_archive_link($postType)
        );

        return $data;
    } // end _getArchivePageData

    private function _getCurrentPageData()
    {
        $data = array(
            'title' => get_the_title(),
        );

        return $data;
    } // end _getCurrentPageData

    private function _getHomePageData()
    {
        $data = array(
            'title' => __('KREATIZE', $this->app->textDomain),
            'url' => get_home_url()
        );

        $frontPageID = get_option('page_on_front');

        if ($frontPageID) {
            $data['title'] = get_the_title($frontPageID);
        }

        return $data;
    } // end _getHomePageData

    public function page()
    {
        $data = array(
            'homePage' => $this->_getHomePageData(),
            'currentPage' => $this->_getCurrentPageData(),
        );

        echo $this->app->render('frontend/breadcrumbs/page.php', $data);
    } // end processPage
}