<?php
namespace kreatize\service;
require_once 'Options.php';

class CareerPageOptions extends Options
{
    private $_optionIdent = 'career';
    private $_values = array();

    protected function onInit()
    {
        $this->_values = $this->getFrontendOption($this->_optionIdent);
    } // end onInit

    public function headerImage()
    {
        return $this->current($this->_values['career_page_header_image']);
    } // end headerImage

    public function headerTitle()
    {
        return $this->current($this->_values['career_page_header_title']);
    } // end headerTitle

    public function vacanciesTitle()
    {
        return $this->current($this->_values['career_page_vacancies_title']);
    } // end vacanciesTitle

    public function vacanciesText()
    {
        return $this->current($this->_values['career_page_vacancies_text']);
    } // end vacanciesText

    public function vacanciesButtonText()
    {
        return $this->current($this->_values['career_page_vacancies_button_text']);
    } // end vacanciesButtonText

    public function vacanciesButtonPage()
    {
        return $this->current($this->_values['career_page_vacancies_button_page']);
    } // end vacanciesButtonPage

    public function vacanciesButtonUrl()
    {
        return $this->current($this->_values['career_page_vacancies_button_url']);
    } // end vacanciesButtonUrl

    public function vacanciesApplyButtonUrl()
    {
        $customUrl = $this->vacanciesButtonUrl();

        if ($customUrl) {
            return $customUrl;
        }

        $pageID = $this->vacanciesButtonPage();

        if (!$pageID) {
            return '#';
        }

        $url = $this->app->facade->getPermalink($pageID);

        return $url;
    } // end vacanciesApplyButtonUrl

    public function getVacanciesData()
    {
        $list = $this->current($this->_values['career_page_vacancies_items_list']);

        $this->_prepareVacanciesData($list);

        return $list;
    } // end getVacanciesData

    private function _prepareVacanciesData(&$vacancies)
    {
        foreach ($vacancies as $ident => &$item) {
            $item['url'] = '#';

            if (array_key_exists('photo', $item)
                && $item['photo'] == false) {

            }
        }
    } // end _prepareVacanciesData

    public function socialFooterTitle()
    {
        return $this->current($this->_values['career_page_social_footer_title']);
    } // end socialFooterTitle

    public function socialFooterFacebookUrl()
    {
        return $this->current($this->_values['career_page_social_footer_facebook_url']);
    } // end socialFooterFacebookUrl

    public function socialFooterLinkedinUrl()
    {
        return $this->current($this->_values['career_page_social_footer_linkedin_url']);
    } // end socialFooterLinkedinUrl

    public function socialFooterTwitterUrl()
    {
        return $this->current($this->_values['career_page_social_footer_twitter_url']);
    } // end socialFooterTwitterUrl

    public function socialFooterYouTubeUrl()
    {
        return $this->current($this->_values['career_page_social_footer_youtube_url']);
    } // end socialFooterYouTubeUrl
}