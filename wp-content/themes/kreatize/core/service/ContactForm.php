<?php
namespace kreatize\service;

use kreatize\abstracts\Service as Service;

class ContactForm extends Service
{
   public function init()
   {
       add_filter(
           'wpcf7_form_elements',
           array(&$this, 'onApplyOptionsValuesToContactPageFormFilter')
       );

       add_filter(
           'wpcf7_form_elements',
           array(&$this, 'onApplyOptionsValuesToContactRPageFormFilter')
       );

       add_filter(
           'wpcf7_form_elements',
           array(&$this, 'onApplyOptionsValuesToSignUpPageFormFilter')
       );

       add_filter(
           'wpcf7_form_elements',
           array(&$this, 'onApplyOptionsValuesToSupplierPageFormFilter')
       );

       add_filter(
           'wpcf7_form_elements',
           array(&$this, 'onApplyOptionsValuesToSamPageFormFilter')
       );

       add_filter(
           'wpcf7_form_elements',
           array(&$this, 'onApplyOptionsValuesToInnovationContactPageFormFilter')
       );

//       add_action(
//           'wpcf7_submit',
//           array(&$this, 'onRedirectSuccessAction'),
//           10,
//           2
//       );
   } // edn init

    public function onRedirectSuccessAction($instance, $result)
    {
        if ($result) {

        }
    } // end test

    public function onApplyOptionsValuesToContactPageFormFilter($content)
    {
        $options = $this->app->service('ContactPageOptions');

        $optionsList = array(
        '%contactPageEmailPlaceholder%' => $options->formFieldEmailPlaceholder(),
        '%contactPageSubjectPlaceholder%' => $options->formFieldSubjectPlaceholder(),
        '%contactPageMessagePlaceholder%' => $options->formFieldMessagePlaceholder(),
        '%contactPageButtonText%' => $options->formButtonText(),
        );

        $this->_replaceValuesInContent($optionsList, $content);

        return $content;
    } // end onApplyOptionsValuesToContactPageFormFilter

    public function onApplyOptionsValuesToContactRPageFormFilter($content)
    {
        $options = $this->app->service('ContactRPageOptions');

        $optionsList = array(
        '%contactRPageNamePlaceholder%' => $options->formFieldNamePlaceholder(),
        '%contactRPageEmailPlaceholder%' => $options->formFieldEmailPlaceholder(),
        '%contactRPageSubjectPlaceholder%' => $options->formFieldSubjectPlaceholder(),
        '%contactRPageMessagePlaceholder%' => $options->formFieldMessagePlaceholder(),
        '%contactRPageButtonText%' => $options->formButtonText(),
        );

        $this->_replaceValuesInContent($optionsList, $content);

        return $content;
    } // end onApplyOptionsValuesToContactRPageFormFilter

    public function onApplyOptionsValuesToSignUpPageFormFilter($content)
    {
        $options = $this->app->service('SignupPageOptions');

        $optionsList = array(
            '%signupPageNamePlaceholder%' => $options->formNamePlaceholder(),
            '%signupPageEmailPlaceholder%' => $options->formEmailPlaceholder(),
            '%signupPagePhonePlaceholder%' => $options->formPhonePlaceholder(),
            '%signupPageCompanyPlaceholder%' => $options->formCompanyPlaceholder(),
            '%signupPageButtonText%' => $options->formButtonText(),
        );

        $this->_replaceValuesInContent($optionsList, $content);

        return $content;
    } // end onApplyOptionsValuesToSignUpPageFormFilter

    public function onApplyOptionsValuesToSupplierPageFormFilter($content)
    {
        $options = $this->app->service('SupplierPageOptions');

        $optionsList = array(
            '%supplierPageNamePlaceholder%' => $options->headerFormNamePlaceholder(),
            '%supplierPageEmailPlaceholder%' => $options->headerFormEmailPlaceholder(),
            '%supplierPagePhonePlaceholder%' => $options->headerFormNamePhonePlaceholder(),
            '%supplierPageCompanyPlaceholder%' => $options->headerFormCompanyPlaceholder(),
            '%supplierPageButtonText%' => $options->headerFormButtonText(),
        );

        $this->_replaceValuesInContent($optionsList, $content);

        return $content;
    } // end onApplyOptionsValuesToSupplierPageFormFilter

    public function onApplyOptionsValuesToSamPageFormFilter($content)
    {
        $options = $this->app->service('SamPageOptions');

        $optionsList = array(
            '%samPageEmailPlaceholder%' => $options->formEmailPlaceholder(),
            '%samPageCompanyPlaceholder%' => $options->formCompanyPlaceholder(),
            '%samPageButtonText%' => $options->formButtonText(),
        );

        $this->_replaceValuesInContent($optionsList, $content);

        return $content;
    } // end onApplyOptionsValuesToSamPageFormFilter

    public function onApplyOptionsValuesToInnovationContactPageFormFilter($content)
    {
        $options = $this->app->service('InnovationContactPageOptions');

        $optionsList = array(
            '%innovationContactPageNamePlaceholder%' => $options->formFieldNamePlaceholder(),
            '%innovationContactPageEmailPlaceholder%' => $options->formFieldEmailPlaceholder(),
            '%innovationContactPageCompanyPlaceholder%' => $options->formFieldCompanyPlaceholder(),
            '%innovationContactPageFileLabel%' => $options->formFieldUploadLabel(),
            '%innovationContactPageFilePlaceholder%' => $options->formFieldUploadPlaceholder(),
            '%innovationContactPageButtonText%' => $options->formButtonText(),
        );

        $this->_replaceValuesInContent($optionsList, $content);

        return $content;
    } // end onApplyOptionsValuesToInnovationContactPageFormFilter


    private function _replaceValuesInContent($optionsList, &$content)
    {
        foreach ($optionsList as $tpl => $value) {
            $content = str_replace($tpl, $value, $content);
        }
    } // end _replaceValuesInContent
}