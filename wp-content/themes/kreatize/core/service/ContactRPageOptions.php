<?php
namespace kreatize\service;
require_once 'Options.php';

class ContactRPageOptions extends Options
{
    private $_optionIdent = 'contact_r';
    private $_values = array();

    protected function onInit()
    {
        $this->_values = $this->getFrontendOption($this->_optionIdent);
    } // end onInit

    public function title()
    {
        return $this->current($this->_values['general_title']);
    } // end title

    public function text($id)
    {
        return $this->current($this->_values['general_text_'.$id]);
    } // end text

    public function formFieldNamePlaceholder()
    {
        return $this->current($this->_values['form_field_name_placeholder']);
    } // end formFieldNamePlaceholder

    public function formFieldEmailPlaceholder()
    {
        return $this->current($this->_values['form_field_email_placeholder']);
    } // end formFieldEmailPlaceholder

    public function formFieldSubjectPlaceholder()
    {
        return $this->current($this->_values['form_field_subject_placeholder']);
    } // end formFieldSubjectPlaceholder

    public function formFieldMessagePlaceholder()
    {
        return $this->current($this->_values['form_field_message_placeholder']);
    } // end formFieldMessagePlaceholder

    public function formButtonText()
    {
        return $this->current($this->_values['form_button_text']);
    } // end formButtonText
}