<?php
namespace kreatize\service;

use kreatize\abstracts\Service as Service;

class Css extends Service
{
    private $_prefix = 'kreatize-';

    public function addCommonCss()
    {
        add_action(
            'wp_print_styles',
            array(&$this, 'printCommonCssAction')
        );
    } // end display

    public function addOptionPageCss()
    {
        add_action(
            'admin_print_styles',
            array(&$this, 'printAdminCssAction')
        );


        $pageHook = $this->app->service('optionTree')->getSettingsPageHook();

        add_action(
            'admin_print_styles-'.$pageHook,
            array(&$this, 'printAdminOptionsPageCssAction')
        );

        $pageHook = $this->app->service('optionTree')->getAdminSettingsPageHook();

        add_action(
            'admin_print_styles-'.$pageHook,
            array(&$this, 'printAdminOptionsPageCssAction')
        );
    } // end addOptionPageCss

    public function printCommonCssAction()
    {
        /* Bootstrap Core CSS */

        $this->app->facade->addCss(
            'bootstrap',
            get_stylesheet_directory_uri().'/vendor/bootstrap/css/bootstrap.min.css'
        );

         /* Custom Fonts */

        $this->app->facade->addCss(
            'font-awesome',
            get_stylesheet_directory_uri().'/vendor/font-awesome/css/font-awesome.min.css'
        );

        $this->app->facade->addCss(
            'titillium-font-css',
            'https://fonts.googleapis.com/css?family=Titillium+Web'
        );

        $this->app->facade->addCss(
            'Source-Sans-Pro-font-css',
            'https://fonts.googleapis.com/css?family=Source+Sans+Pro'
        );

        $this->app->facade->addCss(
            'Titillium-font-css',
            'https://fonts.googleapis.com/css?family=Titillium+Web:400,700'
        );

        /* Theme CSS */
        $this->app->facade->addCss(
            $this->_prefix.'main-styles',
            get_stylesheet_directory_uri().'/css/style.css',
            array(),
            $this->app->version()
        );

        $this->app->facade->addCss(
            $this->_prefix.'kreatize',
            get_stylesheet_directory_uri().'/core/assets/css/frontend/kreatize.css',
            array(),
            $this->app->version()
        );
    } // end printCommonCssAction

    public function printAdminOptionsPageCssAction()
    {
        $filename = (PRODUCTION_MODE_IS_ACTIVE) ? 'demo_options_page.css' : 'options_page.css';

        $this->app->facade->addCss(
            $this->_prefix.'options-page',
            get_stylesheet_directory_uri().'/core/assets/css/backend/'.$filename,
            array(),
            $this->app->version()
        );
    } // end printAdminOptionsPageCssAction

    public function printAdminCssAction()
    {
        $filename = (PRODUCTION_MODE_IS_ACTIVE) ? 'demo_admin.css' : 'admin.css';

        $this->app->facade->addCss(
            $this->_prefix.'admin',
            get_stylesheet_directory_uri().'/core/assets/css/backend/'.$filename,
            array(),
            $this->app->version()
        );
    } // end printAdminCssAction


}