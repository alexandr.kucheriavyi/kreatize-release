<?php
namespace kreatize\service;
require_once 'Options.php';

class FaqPageOptions extends Options
{
    private $_optionIdent = 'faq';
    private $_values = array();

    protected function onInit()
    {
        $this->_values = $this->getFrontendOption($this->_optionIdent);
    } // end onInit

    public function headerImage()
    {
        return $this->current($this->_values['header_image']);
    } // end headerImage

    public function headerTitle()
    {
        return $this->current($this->_values['header_title']);
    } // end headerTitle

    public function tabName($id)
    {
        return $this->current($this->_values['tab_'.$id.'_name']);
    } // end tabName

    public function tabTitle($id)
    {
        return $this->current($this->_values['tab_'.$id.'_title']);
    } // end tabTitle

    public function tabItems($id)
    {
        return $this->current($this->_values['tab_'.$id.'_items_list']);
    } // end tabItems

    public function tabFourText()
    {
        return $this->current($this->_values['tab_4_text']);
    } // end tabFourText

    public function tabFourPaymentsMethods()
    {
        $data = $this->current($this->_values['tab_4_payment_methods']);

        $this->_preparePaymentsMethods($data);

        return $data;
    } // end tabFourPaymentsMethods

    private function _preparePaymentsMethods(&$data)
    {
        foreach ($data as $ident => &$item) {
            foreach ($item as $key => &$value) {
                if ($key == 'images') {
                    $value = explode(',' , $value);
                    $imagesHeight = explode(',' , $item['image_height']);
                    $this->_prepareImages($value, $imagesHeight);
                }
            }
        }
    } // end _preparePaymentsMethods

    private function _prepareImages(&$images, $imagesHeight)
    {
        foreach ($images as $ident => &$image) {
            $url = wp_get_attachment_image_src($image, 'full');
            $height= '';

            if (array_key_exists($ident, $imagesHeight)) {
                $height = $imagesHeight[$ident];
            }

            $image = array(
                'url' => $url[0],
                'height' => $height.'px',
            );
        }
    } // end _prepareImages
}