<?php
namespace kreatize\service;
require_once 'Options.php';

class FooterOptions extends Options
{
    private $_optionIdent = 'footer';
    private $_values = array();

    protected function onInit()
    {
        $this->_values = $this->getFrontendOption($this->_optionIdent);
    } // end onInit

    public function logo()
    {
        $url = $this->current($this->_values['footer_logo']);

        if (!$url) {
            $url = get_stylesheet_directory_uri().'/img/logo-white.png';
        }

        return $url;
    } // end logo

    public function facebookUrl()
    {
        return $this->current($this->_values['footer_facebook_url']);
    } // end facebookUrl

    public function linkedinUrl()
    {
        return $this->current($this->_values['footer_linkedin_url']);
    } // end linkedinUrl

    public function twitterUrl()
    {
        return $this->current($this->_values['footer_twitter_url']);
    } // end twitterUrl

    public function youTubeUrl()
    {
        return $this->current($this->_values['footer_yotube_url']);
    } // end youTubeUrl

    public function formLabel()
    {
        return $this->current($this->_values['footer_form_label_text']);
    } // end formLabel

    public function formEmailFieldPlaceholder()
    {
        return $this->current($this->_values['footer_form_email_field_placeholder']);
    } // end formEmailFieldPlaceholder

    public function formSubmitButtonCaption()
    {
        return $this->current($this->_values['footer_form_submit_button_caption']);
    } // end formSubmitButtonCaption

    public function menuBlockCaption($id)
    {
        return $this->current($this->_values['footer_menu_block_'.$id.'_title']);
    } // end menuBlockCaption

    public function getMenuData($id)
    {
        $menu = $this->current($this->_values['footer_menu_block_'.$id.'_items']);

        $this->_prepareMenu($menu);

        return $menu;
    } // end getMenuData

    private function _prepareMenu(&$menu)
    {
        foreach ($menu as $ident => &$item) {
            $item['url'] = '#';

            if (array_key_exists('custom_url', $item)
                && $item['custom_url'] != false) {
                $item['url'] = $item['custom_url'];
                continue;
            }

            if (array_key_exists('page', $item)
                && $item['page'] != false) {
                $item['url'] = $this->app->facade->getPermalink($item['page']);

                if (!$item['title']) {
                    $item['title'] = get_the_title($item['page']);
                }

                continue;
            }
        }

    } // end _prepareMenu
}