<?php
namespace kreatize\service;
require_once 'Options.php';

class HeaderOptions extends Options
{
    private $_optionIdent = 'header';
    private $_values = array();

    protected function onInit()
    {
        $this->_values = $this->getFrontendOption($this->_optionIdent);
    } // end onInit

    public function betaText()
    {
        return $this->current($this->_values['header_beta_text']);
    } // end betaText

    public function phone()
    {
        return $this->current($this->_values['header_phone']);
    } // end betaText

    public function clearePhone()
    {
        $phone = $this->phone();
        $phone = preg_replace("/[^\+?0-9]/", '', $phone);

        return $phone;
    } // end betaText

    public function contactTitle()
    {
        return $this->current($this->_values['header_contact_link_title']);
    } // end contactTitle

    public function contactPageUrl()
    {
        $pageID = $this->current($this->_values['header_contact_page']);

        return $this->app->facade->getPermalink($pageID);
    } // end contactPageUrl

    public function logo()
    {
        $url = $this->current($this->_values['header_logo']);

        if (!$url) {
            $url = get_stylesheet_directory_uri().'/img/logo.png';
        }

        return $url;
    } // end logo

    public function getMenuData()
    {
        $menu = $this->current($this->_values['header_menu_items']);

        $this->_prepareMenu($menu);

        return $menu;
    } // end getMenuData

    private function _prepareMenu(&$menu)
    {
        $currentPageID = $this->app->facade->getPageID();

        $currentSet = false;

        foreach ($menu as $ident => &$item) {
            $item['url'] = '#';

            if (array_key_exists('custom_url', $item)
                && $item['custom_url'] != false) {
                $item['url'] = $item['custom_url'];
                continue;
            }

            if (array_key_exists('page', $item)
                && $item['page'] != false) {
                $item['url'] = $this->app->facade->getPermalink($item['page']);

                if ($currentPageID == $item['page']) {
                    $item['current'] = true;
                    $currentSet = true;
                }

                if (!$item['title']) {
                    $item['title'] = get_the_title($item['page']);
                }

                continue;
            }
        }

        if (!$currentSet) {
            $menu[0]['current'] = true;
        }

    } // end _prepareMenu

    public function loginButtonCaption()
    {
        return $this->current($this->_values['header_action_button_caption']);
    } // end loginButtonCaption

    public function loginFieldPlaceholder()
    {
        return $this->current($this->_values['header_login_field_placeholder']);
    } // end loginFieldPlaceholder

    public function passwordFieldPlaceholder()
    {
        return $this->current($this->_values['header_pasword_field_placeholder']);
    } // end passwordFieldPlaceholder

    public function submitButtonCaption()
    {
        return $this->current($this->_values['header_submit_button_caption']);
    } // end submitButtonCaption

    public function forgotPasswordText()
    {
        return $this->current($this->_values['header_forgot_password']);
    } // end forgotPasswordText

    public function forgotPasswordUrl()
    {
        return $this->current($this->_values['header_forgot_password_url']);
    } // end forgotPasswordUrl

}