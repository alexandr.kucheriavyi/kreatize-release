<?php
namespace kreatize\service;
require_once 'Options.php';

class HomePageOptions extends Options
{
    private $_optionIdent = 'home_page';
    private $_values = array();

    protected function onInit()
    {
        $this->_values = $this->getFrontendOption($this->_optionIdent);
    } // end onInit


    public function videoText()
    {
        return $this->current($this->_values['home_page_video_text']);
    } // end videoText

    public function videoUrl()
    {
        return $this->current($this->_values['home_page_video_url']);
    } // end videoUrl

    public function videoImage()
    {
        return $this->current($this->_values['home_page_video_image']);
    } // end videoImage

    public function videoButtonText()
    {
        return $this->current($this->_values['home_page_video_button_text']);
    } // end videoButtonText

    public function videoButtonUrl()
    {
        return $this->current($this->_values['home_page_video_button_url']);
    } // end videoButtonUrl

    public function partnersTitle()
    {
        return $this->current($this->_values['home_page_partners_title']);
    } // end partnersTitle

    public function partnersLogo($id)
    {
        return $this->current($this->_values['home_page_partners_partner_block_'.$id.'_logo']);
    } // end partnersLogo

    public function partnersQuote($id)
    {
        return $this->current($this->_values['home_page_partners_partner_block_'.$id.'_quote']);
    } // end partnersQuote

    public function partnersAuthor($id)
    {
        return $this->current($this->_values['home_page_partners_partner_block_'.$id.'_author']);
    } // end partnersAuthor

    private function _getResultsOptions($id)
    {
        $idnents = array(
            1 => 'emails_and_phone_calls',
            2 => 'faster_offers',
            3 => 'reduced_processing_costs',
            4 => 'calibrated_inquiries',
        );

        return $idnents[$id];
    } // end _getResultsOptions

    public function partnersResultPercent($id, $makeFloat = false)
    {
        $ident = $this->_getResultsOptions($id);

        $result =  $this->current($this->_values['home_page_results_'.$ident.'_percent']);

        if ($makeFloat) {
            $result = $result / 100;
        }

        return $result;
    } // end partnersResultPercent

    public function partnersResultText($id)
    {
        $ident = $this->_getResultsOptions($id);

        return $this->current($this->_values['home_page_results_'.$ident.'_text']);
    } // end partnersResultText

    public function partnersButtonText()
    {
        return $this->current($this->_values['home_page_partners_button_text']);
    } // end partnersButtonText

    public function partnersButtonUrl()
    {
        return $this->current($this->_values['home_page_partners_button_url']);
    } // end partnersButtonUrl

    public function customersSayTitle()
    {
        return $this->current($this->_values['home_page_customer_say_title']);
    } // end customersSayTitle

    public function customersSayPhoto()
    {
        return $this->current($this->_values['home_page_customer_say_photo']);
    } // end customersSayPhoto

    public function customersSayQuote()
    {
        return $this->current($this->_values['home_page_customer_say_quote']);
    } // end customersSayQuote

    public function customersSayLogo()
    {
        return $this->current($this->_values['home_page_customer_say_logo']);
    } // end customersSayLogo

    public function customersSayInformation()
    {
        return $this->current($this->_values['home_page_customer_say_information']);
    } // end customersSayInformation

    public function customersSayButtonText()
    {
        return $this->current($this->_values['home_page_customer_say_button_text']);
    } // end customersSayButtonText

    public function customersSayButtonUrl()
    {
        return $this->current($this->_values['home_page_customer_say_button_url']);
    } // end customersSayButtonUrl

    public function processesTitle()
    {
        return $this->current($this->_values['home_page_processes_title']);
    } // end processesTitle

    public function processesMobileButtonText()
    {
        return $this->current($this->_values['home_page_processes_mobile_button_text']);
    } // end processesMobileButtonText

    public function processesMobileButtonUrl()
    {
        return $this->current($this->_values['home_page_processes_mobile_button_url']);
    } // end processesMobileButtonUrl

    public function processesListQueryByTag($id)
    {
        $termId = $this->current($this->_values['home_page_processes_list_'.$id.'_tag']);

        if (!$termId) {
            return false;
        }

        $processes = $this->app->service('ProcessesPostType');

        return $processes->getPostsQueryByTerms(array($termId), 'term_id');
    } // end processesListQueryByTag

    public function processesListTagName($id)
    {
        $termId = $this->current($this->_values['home_page_processes_list_'.$id.'_tag']);

        if (!$termId) {
            return false;
        }

        $processes = $this->app->service('ProcessesPostType');

        return $processes->getTermNameById($termId);
    } // end processesListTagName
}