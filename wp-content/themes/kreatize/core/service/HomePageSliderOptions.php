<?php
namespace kreatize\service;
require_once 'Options.php';

class HomePageSliderOptions extends Options
{
    private $_optionIdent = 'home_page_slider';
    private $_values = array();

    protected function onInit()
    {
        $this->_values = $this->getFrontendOption($this->_optionIdent);
    } // end onInit

    public function slideOneItemTitle($id)
    {
        return $this->current(
            $this->_values['home_page_slider_slide_1_item_'.$id.'_title']
        );
    } // end slideOneItemTitle

    public function slideOneItemImage($id)
    {
        return $this->current(
            $this->_values['home_page_slider_slide_1_item_'.$id.'_image']
        );
    } // end slideOneItemImage

    public function slideOneItemText($id)
    {
        return $this->current(
            $this->_values['home_page_slider_slide_1_item_'.$id.'_text']
        );
    } // end slideOneItemText

    public function slideOneMainTitle()
    {
        return $this->current($this->_values['home_page_slider_slide_1_main_title']);
    } // end slideOneMainTitle

    public function slideOneMainText()
    {
        return $this->current($this->_values['home_page_slider_slide_1_main_text']);
    } // end slideOneMainText

    public function slideTwoMainTitle()
    {
        return $this->current($this->_values['home_page_slider_slide_2_main_title']);
    } // end slideTwoMainTitle

    public function slideTwoItemIcon($id)
    {
    return $this->current($this->_values['home_page_slider_slide_2_item_'.$id.'_icon']);
    } // end slideTwoItemIcon

    public function slideTwoItemTitle($id)
    {
        return $this->current($this->_values['home_page_slider_slide_2_item_'.$id.'_title']);
    } // end slideTwoItemTitle

    public function slideTwoItemText($id)
    {
        return $this->current($this->_values['home_page_slider_slide_2_item_'.$id.'_text']);
    } // end slideTwoItemText

    public function slideTwoItemBgImage($id)
    {
        return $this->current($this->_values['home_page_slider_slide_2_item_'.$id.'_bg_image']);
    } // end slideTwoItemBgImage

    public function slideThreeMainTitle()
    {
        return $this->current($this->_values['home_page_slider_slide_3_main_title']);
    } // end slideThreeMainTitle

    public function slideThreeItemIcon($id)
    {
        return $this->current($this->_values['home_page_slider_slide_3_item_'.$id.'_icon']);
    } // end slideThreeItemIcon

    public function slideThreeItemTitle($id)
    {
        return $this->current($this->_values['home_page_slider_slide_3_item_'.$id.'_title']);
    } // end slideThreeItemTitle

    public function slideThreeItemText($id)
    {
        return $this->current($this->_values['home_page_slider_slide_3_item_'.$id.'_text']);
    } // end slideThreeItemText

    public function slideThreeItemHiddenText($id)
    {
        return $this->current($this->_values['home_page_slider_slide_3_item_'.$id.'_hidden_text']);
    } // end slideThreeItemHiddenText
}