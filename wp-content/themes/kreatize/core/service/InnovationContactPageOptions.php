<?php
namespace kreatize\service;
require_once 'Options.php';

class InnovationContactPageOptions extends Options
{
    private $_optionIdent = 'innovation_contact';
    private $_values = array();

    protected function onInit()
    {
        $this->_values = $this->getFrontendOption($this->_optionIdent);
    } // end onInit

    public function title()
    {
        return $this->current($this->_values['general_title']);
    } // end title

    public function text()
    {
        return $this->current($this->_values['general_text']);
    } // end text
    
    public function image()
    {
        return $this->current($this->_values['general_image']);
    } // end image

    public function formFieldNamePlaceholder()
    {
        return $this->current($this->_values['form_field_name_placeholder']);
    } // end formFieldNamePlaceholder

    public function formFieldEmailPlaceholder()
    {
        return $this->current($this->_values['form_field_email_placeholder']);
    } // end formFieldEmailPlaceholder

    public function formFieldCompanyPlaceholder()
    {
        return $this->current($this->_values['form_field_company_placeholder']);
    } // end formFieldCompanyPlaceholder

    public function formFieldUploadLabel()
    {
        return $this->current($this->_values['form_upload_field_label']);
    } // end formFieldUploadLabel

    public function formFieldUploadPlaceholder()
    {
        return $this->current($this->_values['form_upload_field_placeholder']);
    } // end formFieldUploadPlaceholder

    public function formButtonText()
    {
        return $this->current($this->_values['form_button_text']);
    } // end formButtonText
}