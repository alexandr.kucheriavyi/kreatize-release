<?php
namespace kreatize\service;
require_once 'Options.php';

class InnovationPageOptions extends Options
{
    private $_optionIdent = 'innovation_page';
    private $_values = array();

    protected function onInit()
    {
        $this->_values = $this->getFrontendOption($this->_optionIdent);
    } // end onInit

    public function headerImage()
    {
        return $this->current($this->_values['innovation_header_image']);
    } // end headerImage

    public function headerText()
    {
        return $this->current($this->_values['innovation_header_text']);
    } // end headerText

    public function headerButtonText()
    {
        return $this->current($this->_values['innovation_header_button_text']);
    } // end headerButtonText

    public function headerButtonPage()
    {
        return $this->current($this->_values['innovation_header_button_page']);
    } // end headerButtonPage

    public function headerButtonCustomUrl()
    {
        return $this->current($this->_values['innovation_header_button_url']);
    } // end headerButtonCustomUrl

    public function headerButtonUrl()
    {
        $customUrl = $this->headerButtonCustomUrl();

        if ($customUrl) {
            return $customUrl;
        }

        $pageID = $this->headerButtonPage();

        if (!$pageID) {
            return '#';
        }

        $url = $this->app->facade->getPermalink($pageID);

        return $url;
    } // end headerButtonUrl

    public function sectionTwoTitle()
    {
        return $this->current($this->_values['innovation_section_two_title']);
    } // end sectionTwoTitle

    public function sectionTwoContent()
    {
        return $this->current($this->_values['innovation_section_two_text']);
    } // end sectionTwoContent

    public function cardImage($id)
    {
        return $this->current($this->_values['innovation_cards_card_'.$id.'_image']);
    } // end cardImage

    public function cardText($id, $textId)
    {
        return $this->current($this->_values['innovation_cards_card_'.$id.'_text_'.$textId]);
    } // end cardText

    public function tabName($id)
    {
        return $this->current($this->_values['innovation_tabs_item_'.$id.'_name']);
    } // end tabName

    public function tabContent($id)
    {
        return $this->current($this->_values['innovation_tabs_item_'.$id.'_content']);
    } // end tabContent

    public function lastSectionText()
    {
        return $this->current($this->_values['innovation_last_section_text']);
    } // end lastSectionText

    public function lastSectionButtonText()
    {
        return $this->current($this->_values['innovation_last_section_button_text']);
    } // end lastSectionButtonText

    public function lastSectionButtonPage()
    {
        return $this->current($this->_values['innovation_last_section_button_page']);
    } // end lastSectionButtonPage

    public function lastSectionButtonCustomUrl()
    {
        return $this->current($this->_values['innovation_last_section_button_url']);
    } // end lastSectionButtonCustomUrl

    public function lastSectionButtonUrl()
    {
        $customUrl = $this->lastSectionButtonCustomUrl();

        if ($customUrl) {
            return $customUrl;
        }

        $pageID = $this->lastSectionButtonPage();

        if (!$pageID) {
            return '#';
        }

        $url = $this->app->facade->getPermalink($pageID);

        return $url;
    } // end lastSectionButtonUrl
}