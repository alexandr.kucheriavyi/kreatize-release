<?php
namespace kreatize\service;

use kreatize\abstracts\Service as Service;

class Js extends Service
{
    private $_prefix = 'kreatize-';

    public function addCommonJs()
    {
        add_action(
            'wp_enqueue_scripts',
            array(&$this, 'printCommonJsAction')
        );

        add_action(
            'init',
            array(&$this, 'ModifyJqueryAction')
        );



    } // end addCommonJs

    public function addOptionPageJs()
    {
        $pageHook = $this->app->service('optionTree')->getSettingsPageHook();

        add_action(
            'admin_print_scripts-'.$pageHook,
            array(&$this, 'printAdminOptionsPageJsAction')
        );
    } // end addOptionPageJs

    public function addBackendJs()
    {
        add_action(
            'admin_enqueue_scripts',
            array(&$this, 'printBackendJsAction')
        );
    } // end addBackendJs

    public function ModifyJqueryAction()
    {
        wp_deregister_script('jquery');
    } // end ModifyJqueryAction

    public function printCommonJsAction()
    {
        /* HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries*/

        $this->app->facade->addJs(
            'html5shiv',
            'https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js'
        );
        wp_script_add_data('html5shiv', 'conditional', 'lt IE 9');

        $this->app->facade->addJs(
            'respond',
            'https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js'
        );
        wp_script_add_data('respond', 'conditional', 'lt IE 9');

        /* Jquery Js */

        $this->app->facade->addJs(
            'jquery',
            get_stylesheet_directory_uri().'/vendor/jquery/jquery.min.js',
            array(),
            '1.12.4',
            true
        );

        /* Bootstrap Core Js */

        $this->app->facade->addJs(
            'bootstrap',
            get_stylesheet_directory_uri().'/vendor/bootstrap/js/bootstrap.min.js',
            'jquery',
            '3.3.7',
            true
        );

        /* Contact Form JavaScript */
        $this->app->facade->addJs(
            'jqBootstrapValidation',
            get_stylesheet_directory_uri().'/js/jqBootstrapValidation.js',
            'bootstrap',
            $this->app->version(),
            true

        );

        $this->app->facade->addJs(
            'jquery-validate',
            'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.js',
            'jqBootstrapValidation',
            $this->app->version(),
            true
        );

        $this->app->facade->addJs(
            $this->_prefix.'contact_me',
            get_stylesheet_directory_uri().'/js/contact_me.js',
            'jquery-validate',
            $this->app->version(),
            true
        );

        $this->app->facade->addJs(
            $this->_prefix.'all',
            get_stylesheet_directory_uri().'/js/all.js',
            $this->_prefix.'contact_me',
            $this->app->version(),
            true
        );

        /* jQuery */
        $this->app->facade->addJs(
            'flexslider',
            get_stylesheet_directory_uri().'/js/flexslider.js',
            $this->_prefix.'all',
            $this->app->version(),
            true
        );

        /* Plugin JavaScript */
        $this->app->facade->addJs(
            'jquery-easing',
            'https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js',
            'flexslider',
            $this->app->version(),
            true
        );

        $this->app->facade->addJs(
            'progressbar',
            get_stylesheet_directory_uri().'/js/progressbar.js',
            'jquery-easing',
            $this->app->version(),
            true
        );

        /* Theme JavaScript */
        $this->app->facade->addJs(
            $this->_prefix.'agency',
            get_stylesheet_directory_uri().'/js/agency.min.js',
            'progressbar',
            $this->app->version(),
            true
        );

        $this->app->facade->addJs(
            $this->_prefix.'login',
            get_stylesheet_directory_uri().'/js/login.js',
            'progressbar',
            $this->app->version(),
            true
        );

        $this->app->facade->addJs(
            $this->_prefix.'ga',
            get_stylesheet_directory_uri().'/js/ga.js',
            $this->_prefix.'login',
            $this->app->version(),
            true
        );

        $this->app->facade->addJs(
            $this->_prefix.'kreatize',
            get_stylesheet_directory_uri().'/core/assets/js/frontend/kreatize.js',
            'jquery',
            $this->app->version(),
            true
        );
    } // end printCommonJsAction

    public function printBackendJsAction()
    {
        $this->app->facade->addJs(
            $this->_prefix.'admin',
            get_stylesheet_directory_uri().'/core/assets/js/backend/admin.js',
            'jquery',
            $this->app->version(),
            true
        );
    } // end printBackendJsAction

    public function printAdminOptionsPageJsAction()
    {
        $this->app->facade->addJs(
            $this->_prefix.'options_page',
            get_stylesheet_directory_uri().'/core/assets/js/backend/options_page.js',
            'jquery',
            $this->app->version(),
            true
        );
    } // end printAdminOptionsPageJsAction


}