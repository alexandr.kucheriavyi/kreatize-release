<?php
namespace kreatize\service;

use kreatize\abstracts\Service as Service;

class LanguageSwitcher extends Service
{
    public function mobile()
    {
        $data = array(
            'languageList'  => $this->app->service('WPGlobus')->getEnabledLanguageData()
        );

        return $this->app->render('frontend/header/language_swithcer_mobile.php', $data);
    } // end desktop


    public function desktop()
    {
        $data = array(
            'languageList'  => $this->app->service('WPGlobus')->getEnabledLanguageData()
        );

        return $this->app->render('frontend/header/language_swithcer.php', $data);
    } // end desktop
}