<?php
namespace kreatize\service;

use kreatize\abstracts\Service as Service;

class MailChimpForm extends Service
{
    public function setOptionsValues($content)
    {
        $options = $this->app->service('FooterOptions');

        $optionsList = array(
            '%footerFormEmailPlaceholder%' => $options->formEmailFieldPlaceholder(),
            '%footerFormButtonText%' => $options->formSubmitButtonCaption(),
        );

        $this->_replaceValuesInContent($optionsList, $content);

        return $content;
    } // end setOptionsValues


    private function _replaceValuesInContent($optionsList, &$content)
    {
        foreach ($optionsList as $tpl => $value) {
            $content = str_replace($tpl, $value, $content);
        }
    } // end _replaceValuesInContent

    public function replaceFooterFormValues($content)
    {
        $options = $this->app->service('FooterOptions');

        $contentList = array(
            '%beforeEmail%' => $this->app->render('frontend/mailchimp/footer/before_email.php'),
            '%afterEmail%' => $this->app->render('frontend/mailchimp/footer/after_email.php'),
            '%beforeButton%' => $this->app->render('frontend/mailchimp/footer/before_button.php'),
            '%afterButton%' => $this->app->render('frontend/mailchimp/footer/after_button.php'),
        );

        $fieldValuesList = array(
            '%emailCss%' => 'cl-titil-16-22',
            '%buttonCss%' => 'btn-reg cl-titil-16-22',
            '%formEmailPlaceholder%' => $options->formEmailFieldPlaceholder(),
            '%formButtonText%' => $options->formSubmitButtonCaption(),
        );

        $this->_replaceValuesInContent($contentList, $content);
        $this->_replaceValuesInContent($fieldValuesList, $content);


        return $content;
    } // end replaceFormValues

    public function replacePressFormValues($content)
    {
        $options = $this->app->service('PressPageOptions');

        $contentList = array(
            '%beforeEmail%' => $this->app->render('frontend/mailchimp/press/before_email.php'),
            '%afterEmail%' => $this->app->render('frontend/mailchimp/press/after_email.php'),
            '%beforeButton%' => $this->app->render('frontend/mailchimp/press/before_button.php'),
            '%afterButton%' => $this->app->render('frontend/mailchimp/press/after_button.php'),
        );

        $fieldValuesList = array(
            '%emailCss%' => 'textbox',
            '%buttonCss%' => 'btn btn-primary',
            '%formEmailPlaceholder%' => $options->contactFormEmailPlaceholder(),
            '%formButtonText%' => $options->contactFormButtonText(),
        );

        $this->_replaceValuesInContent($contentList, $content);
        $this->_replaceValuesInContent($fieldValuesList, $content);


        return $content;
    } // end replacePressFormValues
}