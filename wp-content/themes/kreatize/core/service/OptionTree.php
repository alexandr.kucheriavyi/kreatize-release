<?php
namespace kreatize\service;

use kreatize\abstracts\Service as Service;

class OptionTree extends Service
{
   public function init()
   {
       $this->_changeLayout();
       $this->_changeOptions();
   } // edn init
    private function _changeOptions()
    {

        add_filter(
            'ot_options_id',
            array(&$this, 'onChangeOptionsOptionNameAction')
        );

        add_filter(
            'ot_settings_id',
            array(&$this, 'onChangeSettingsOptionNameAction')
        );

        add_filter(
            'ot_layouts_id',
            array(&$this, 'onChangeLayoutsOptionNameAction')
        );
    } // end _changeOptions

    public function onChangeOptionsOptionNameAction($name)
    {
        $currentPage = $this->app->service('optionsSwitcher')->current();

        if ($currentPage == 'default') {
            return $name;
        }

        return $this->app->service('options')->name($currentPage.'_options');
    } // end onChangeOptionsOptionNameAction

    public function onChangeSettingsOptionNameAction($name)
    {
        $currentPage = $this->app->service('optionsSwitcher')->current();

        if ($currentPage == 'default') {
            return $name;
        }

        return $this->app->service('options')->name($currentPage.'_settings');
    } // end onChangeSettingsOptionNameAction

    public function onChangeLayoutsOptionNameAction($name)
    {
        $currentPage = $this->app->service('optionsSwitcher')->current();

        if ($currentPage == 'default') {
            return $name;
        }

        return $this->app->service('options')->name($currentPage.'_layouts');
    } // end onChangeLayoutsOptionNameAction

    private function _changeLayout()
    {
        add_filter(
            'ot_header_logo_link',
            array(&$this, 'onChangeHeaderLogoAction')
        );

        add_filter(
            'ot_header_version_text',
            array(&$this, 'onChangeHeaderVersionAction')
        );

        add_filter(
            'ot_theme_options_page_title',
            array(&$this, 'onChangePageTitleAction')
        );

        add_filter(
            'ot_theme_options_menu_title',
            array(&$this, 'onChangeManuTitleAction')
        );

        add_filter(
            'ot_theme_options_updated_message',
            array(&$this, 'onChangeUpdatedMessageAction')
        );

        add_filter(
            'ot_theme_options_reset_message',
            array(&$this, 'onChangeResetMessageAction')
        );

    } // end _changeLayout

    public function getSettingsPageHook()
    {
        return 'appearance_page_ot-theme-options';
    } // end getSettingsPageHook

    public function getLayoutsOptionName()
    {
        return apply_filters('ot_layouts_id', 'option_tree_layouts');
    } // getLayoutsOptionName

    public function getSettingsOptionName()
    {
        return apply_filters('ot_settings_id', 'option_tree_settings');
    } // getSettingsOptionName

    public function getAdminSettingsPageHook()
    {
        return 'toplevel_page_ot-settings';
    } // end getAdminSettingsPageHook

    public function onChangeHeaderLogoAction($content)
    {
        return $this->app->render('backend/option_tree/logo.php');

    } // end onChangeHeaderLogoAction

    public function onChangeHeaderVersionAction($content)
    {
        return false;

    } // end onChangeHeaderVersionAction

    public function onChangePageTitleAction($content)
    {
        return __('Kreatize Options Page', $this->app->textDomain);
    } // end onChangePageTitleAction

    public function onChangeManuTitleAction($content)
    {
        return __('Kreatize options', $this->app->textDomain);
    } // end onChangeManuTitleAction

    public function onChangeUpdatedMessageAction($content)
    {
        return __('Kreatize options updated.', $this->app->textDomain);
    } // end onChangeUpdatedMessageAction

    public function onChangeResetMessageAction($content)
    {
        return __('Kreatize options reset.', $this->app->textDomain);
    } // end onChangeResetMessageAction1

    public function prepareOptions(&$options)
    {
        $this->decodeOptions($options);
        $this->_addNewKeysToOtherLayouts($options);
    } // end prepareOptions

    public function decodeOptions(&$options)
    {
        $func = 'base64' . '_decode';
        foreach ($options as $ident => $value) {
            if ($ident == 'active_layout') continue;

            $options[$ident] = unserialize($func($value));
        }
    } // end decodeOptions

    public function encodeOptions(&$options)
    {
        $func = 'base64' . '_encode';

        foreach ($options as $ident => $value) {
            if ($ident == 'active_layout') continue;

            $options[$ident] = $func(serialize($value));
        }
    } // end encodeOptions

    private function _addNewKeysToOtherLayouts(&$options)
    {
        $activeLayout = $options['active_layout'];
        $otherLayouts = $options;
        unset($otherLayouts[$activeLayout]);
        unset($otherLayouts['active_layout']);

        foreach ($otherLayouts as $key => &$layoutOptions) {
            $this->_mergeLayouts($options[$activeLayout], $layoutOptions);
        }

        $options = array_merge($options, $otherLayouts);
    } // end _addNewKeysToOtherLayouts

    private function _mergeLayouts(&$activeLayout, &$layoutOptions)
    {
        foreach ($activeLayout as $key => &$value) {
            if (!array_key_exists($key, $layoutOptions)) {
                $layoutOptions[$key] = $value;
                continue;
            }

            if (!is_array($value) && !is_array($layoutOptions[$key])) {
                continue;
            }

            if (!is_array($value) && !is_array($layoutOptions[$key])) {
                continue;
            }

            if (is_array($value) && !is_array($layoutOptions[$key])) {
                $layoutOptions[$key] = $value;
                continue;
            }
        }
    } // end _mergeLayouts

    public function getOptionsStructure()
    {
        $options = get_option($this->getSettingsOptionName());
        $structure = $this->_getSectionsFromOptions($options);
        $this->_addOptionsIdents($structure, $options);

        return $structure;
    } // end getOptionsStructure

    private function _addOptionsIdents(&$structure, $options)
    {
        if (!array_key_exists('settings', $options)) {
            return false;
        }

        foreach ($options['settings'] as $setting) {
            if ($setting['type'] == 'tab') {
                continue;
            }

            $section = 'others';

            if (array_key_exists('section', $setting)) {
                $section = $setting['section'];
            }

            $structure[$section][$setting['id']] = array();
        }
    } // end $structure

    private function _getSectionsFromOptions($options)
    {
        $sections = array(
            'others' => array()
        );

        if (!array_key_exists('sections', $options)) {
            return $sections;
        }

        foreach ($options['sections'] as $section) {
            $sections[$section['id']] = array();
        }

        return $sections;
    } // end _getSectionsFromOptions

    public function getLayoutsListByOptions($options)
    {
        unset($options['active_layout']);
        return array_keys($options);
    } // end getLayoutsListByOptions
}