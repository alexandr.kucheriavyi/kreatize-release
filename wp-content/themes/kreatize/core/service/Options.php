<?php
namespace kreatize\service;

use kreatize\abstracts\Service as Service;
use WPGlobus;

class Options extends Service
{
    protected $prefixOld = 'kreatize_';
    protected $prefix = 'kreatize_options_';
    protected $postfix = '_frontend';

    protected $structureOptionName = 'structure';
    protected $languageRelations = array(
        'en' => 'english',
        'de' => 'deutsch'
    );

    public function name($name) {
        return $this->prefix.$name;
    } // end name

    public function nameOld($name) {
        return $this->prefixOld.$name;
    } // end nameOld

    public function init()
    {
        $optionName = $this->app->service('optionTree')->getLayoutsOptionName();

        add_filter(
            'pre_update_option_'.$optionName,
            array($this, 'onFilterOptionTreeAction'),
            10,
            2
        );
    } // end display

    public function onFilterOptionTreeAction($newValue, $oldValue)
    {

        $optionTreeService =  $this->app->service('optionTree');

        $languagesList = $optionTreeService->getLayoutsListByOptions($newValue);

        $optionTreeService->prepareOptions($newValue);

        $optionsStructure = $optionTreeService->getOptionsStructure();

        $values = $this->initOptionsValues(
            $languagesList,
            $optionsStructure,
            $newValue
        );

        $currentPage = $this->app->service('optionsSwitcher')->current();

        $this->updateFrontendOption($currentPage, $values);

        $optionTreeService->encodeOptions($newValue);

        return $newValue;
    } // end onFilterOptionTreeAction

    public function initOptionsValues($languagesList, $optionsStructure, $options)
    {
        $values = array();

        foreach ($optionsStructure as $ident => $structure) {
            $this->_loadValuesFromOptions($values[], $structure, $options, $languagesList);
        }

        $values[0] = array();

        foreach ($values as $ident => $item) {
            $values[0] = array_merge($values[0], $item);
        }

        $values = $values[0];

        return $values;
    } // end initOptionsValues

    private function _loadValuesFromOptions(&$values, $structure, $options, $languagesList)
    {
        if (!$structure) {
            return false;
        }

       foreach ($structure as $ident => $value) {
            foreach ($languagesList as $language) {
                $values[$ident][$language] = $this->_getValueFromOptions($ident, $language, $options);
            }
       }
    } // end _loadValuesFromOptions

    private function _getValueFromOptions($ident, $language, $options)
    {

        if (!array_key_exists($language, $options)) {
            return false;
        }

        if (!array_key_exists($ident, $options[$language])) {
            return false;
        }

        return $options[$language][$ident];
    } // end _getValueFromOptions

    public function updateOption($name, $value)
    {
        $value = json_encode($value);

        update_option($this->name($name), $value);
    } // end updateOption

    public function getOption($name)
    {
        $value = get_option($this->name($name));

        if ($value) {
            $value = json_decode($value, true) ;
        }

        return $value;
    } // end getOption

    public function updateFrontendOption($name, $value)
    {
        $this->updateOption($name.$this->postfix, $value);
    } // end updateOption

    public function getFrontendOption($name)
    {
        return $this->getOption($name.$this->postfix);
    } // end getFrontendOption


    // Get option for current language
    protected function current($options)
    {
        $currentLangIdent = WPGlobus::Config()->language;
        $currentLang = $this->getLanguageNameByIdent($currentLangIdent);

        if (array_key_exists($currentLang, $options)) {
            return $options[$currentLang];
        }

        $defaulLanguage = $this->getDefaultLanguage();

        if (array_key_exists($defaulLanguage, $options)) {
            return $options[$defaulLanguage];
        }

        return false;
    } // end current

    public function getLanguageNameByIdent($ident)
    {
        if (!array_key_exists($ident, $this->languageRelations)) {
            throw new \Exception('Undefined language ident '.$ident);
        }

        return $this->languageRelations[$ident];
    } // end getLanguageNameByIdent

    public function getDefaultLanguage()
    {
        $defaultLanguage = WPGlobus::Config()->default_language;
        return $this->getLanguageNameByIdent($defaultLanguage);
    } // end getDefaultLanguage

    public function getOptionsPages()
    {
        $options = array(
            'header'    => __('Header', $this->app->textDomain),
            'home_page' => __('Homepage', $this->app->textDomain),
            'home_page_slider' => __('Homepage Slider', $this->app->textDomain),
            'processes_archive_page' => __('Processes Archive Page', $this->app->textDomain),
            'about_us_page' => __('About Us Page', $this->app->textDomain),
            'career' => __('Career Page', $this->app->textDomain),
            'signup_page' => __('Sign up Page', $this->app->textDomain),
            'sam_page' => __('Sam Page', $this->app->textDomain),
            'rapid_prototyping_page' => __('Rapid Prototyping Page', $this->app->textDomain),
            'supplier_page' => __('Supplier Page', $this->app->textDomain),
            'innovation_page' => __('Innovation Page', $this->app->textDomain),
            'press_page' => __('Press Page', $this->app->textDomain),
            'faq' => __('Faq Page', $this->app->textDomain),
            'terms_and_conditions' => __('Terms and Conditions Page', $this->app->textDomain),
            'thankyou' => __('Thank You Page', $this->app->textDomain),
            'innovation_contact' => __('Innovation Contact Page', $this->app->textDomain),
            'contact' => __('Contact Page', $this->app->textDomain),
            'contact_r' => __('Apply Now Page', $this->app->textDomain),
            'footer'    => __('Footer', $this->app->textDomain),
        );

        return $options;
    } // end
}