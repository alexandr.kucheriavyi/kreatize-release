<?php
namespace kreatize\service;

use kreatize\abstracts\Service as Service;

class OptionsMigrator extends Service
{
    private $_replaceCount = 0;

    public function init()
    {
        if (!defined('DEV_MODE') || !DEV_MODE) {
            return false;
        }

        add_action('admin_menu', array(&$this, 'onAddPageAction'));
    } // end display

    public function onAddPageAction()
    {
        add_options_page(
            'Theme Options Migration',
            'Theme Options Migration',
            'manage_options',
            'theme-option-migration',
            array(
                $this,
                'onPageDisplay'
            )
        );
    } // end onAddPageAction

    public function onPageDisplay()
    {
        $this->_doMigrate();

        $vars = array(
            'from' => '',
            'to' => '',
            'mode' => 'test'
        );

        if (array_key_exists('__do_migrate', $_POST)) {
            $vars = array_merge($_POST);
        }

        echo $this->app->render('backend/migration.php', $vars);
    } // end onPageDisplay

    private function _doMigrate()
    {
        if (!array_key_exists('__do_migrate', $_POST)) {
            return false;
        }

        $optionService = $this->app->service('options');

        $options = $optionService->getOptionsPages();

        foreach ($options as $ident => $name) {
            $fullIdent = $optionService->name($ident.'_layouts');

            $value = get_option($fullIdent);

            $this->app->service('optionTree')->decodeOptions($value);

            $this->_doReplaceValues($value);

            if (!$this->_isTestMode()) {
                $this->app->service('optionTree')->encodeOptions($value);
                $value = update_option($fullIdent, $value);
            }
        }

        echo 'replaced '.$this->_replaceCount;
    } // end _doMigrate

    private function _doReplaceValues(&$optionsList)
    {
        foreach ($optionsList as &$item) {
            if (is_array($item)) {
                $this->_doReplaceValues($item);
            } else if ($this->_hasOldDomainInOptionValue($item)) {
                $this->_replaceCount++;
                if (!$this->_isTestMode()) {
                    $item = str_replace($_POST['from'], $_POST['to'], $item);
                }

                $item.' ('. $this->_replaceCount.')<br>';
            }
        }
    } // end _doReplaceValues

    private function _isTestMode()
    {
        return $_POST['mode'] == 'test';
    } // end _isTestMode


    private function _hasOldDomainInOptionValue($value)
    {
        return strpos($value, $_POST['from']) !== false;
    } // end _hasOldDomainInOptionValue
}