<?php
namespace kreatize\service;

use kreatize\abstracts\Service as Service;
use WPGlobus;

class OptionsSwitcher extends Service
{
    protected $optionsSwitcherIdent = 'optionsSwitcher';
    protected $defaultPage = 'default';

    public function init()
    {
        $this->setCurrent();

        add_filter(
            'ot_header_list',
            array($this, 'onAppendOptionsSwitcherAction')
        );

//        $options = get_option('option_tree_settings');
//
//
//        highlight_string(print_r($options, 1));
//        die();
    } // end display

    public function onAppendOptionsSwitcherAction($pageID)
    {
        $data = array(
            'options' => $this->app->service('options')->getOptionsPages(),
            'current' => $this->current()
        );

        echo $this->app->render('backend/options/switcher.php', $data);
    } // end onAppendOptionsSwitcherAction

    public function current()
    {
        if (!session_id())
            session_start();

        if(!isset($_SESSION[$this->optionsSwitcherIdent])) {
            return $this->defaultPage;
        }

        return $_SESSION[$this->optionsSwitcherIdent];
    } // end current

    public function setCurrent()
    {
        if (!array_key_exists('__options_switcher', $_POST)) {
            return false;
        }

        if (!session_id())
            session_start();

        $_SESSION[$this->optionsSwitcherIdent] = $_POST['options_switcher'];
    } // end setCurrent
}