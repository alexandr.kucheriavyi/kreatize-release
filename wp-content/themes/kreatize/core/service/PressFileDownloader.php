<?php
namespace kreatize\service;

use kreatize\abstracts\Service as Service;

class PressFileDownloader extends Service
{
   public function init()
   {
       if (!array_key_exists('download', $_GET) || empty($_GET['download'])) {
           return false;
       }

       $file = $_GET['download'];
       $file = ABSPATH.wp_make_link_relative($file);
       $file = realpath ($file);
       $fileName = basename($file);

       if (file_exists($file)) {
           if (ob_get_level()) {
               ob_end_clean();
           }

           header('Content-Description: File Transfer');
           header('Content-Type: application/octet-stream');
           header('Content-Disposition: attachment; filename='.$fileName);
           header('Content-Transfer-Encoding: binary');
           header('Expires: 0');
           header('Cache-Control: must-revalidate');
           header('Pragma: public');
           header('Content-Length: ' . filesize($file));

           readfile($file);
           exit;
       }
   } // edn init
}