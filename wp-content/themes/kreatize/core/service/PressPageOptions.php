<?php
namespace kreatize\service;
require_once 'Options.php';

class PressPageOptions extends Options
{
    private $_optionIdent = 'press_page';
    private $_values = array();

    protected function onInit()
    {
        $this->_values = $this->getFrontendOption($this->_optionIdent);
    } // end onInit

    public function headerTitle()
    {
        return $this->current($this->_values['press_page_header_title']);
    } // end headerTitle

    public function navigationItem($id)
    {
        return $this->current($this->_values['press_page_navigation_item_'.$id.'_name']);
    } // end headerTitle

    public function pressReviewTitle()
    {
        return $this->current($this->_values['press_page_press_review_title']);
    } // end pressReviewTitle

    public function blogLinkText()
    {
        return $this->current($this->_values['press_page_press_review_blog_link_text']);
    } // end blogLinkText

    public function blogLinkUrl()
    {
        return $this->current($this->_values['press_page_press_review_blog_link_url']);
    } // end blogLinkUrl

    public function pressReviewItemDate($id)
    {
        $id = $this->_getIdByOrder($id);

        return $this->current($this->_values['press_page_press_review_item_'.$id.'_date']);
    } // end blogLinkUrl

    public function pressReviewItemTitle($id)
    {
        $id = $this->_getIdByOrder($id);

        return $this->current($this->_values['press_page_press_review_item_'.$id.'_title']);
    } // end pressReviewItemTitle

    public function pressReviewItemText($id)
    {
        $id = $this->_getIdByOrder($id);

        return $this->current($this->_values['press_page_press_review_item_'.$id.'_text']);
    } // end pressReviewItemText

    private function _getIdByOrder($id)
    {
        $items = array(
            1 => $this->pressReviewItemOrder(1),
            2 => $this->pressReviewItemOrder(2),
            3 => $this->pressReviewItemOrder(3),
        );

        $sortedItems = array();

        foreach ($items as $key => $order) {
            $sortedItems[$order][] = $key;
        }

        ksort($sortedItems);

        $resultItems = array();

        $n = 0;
        foreach ($sortedItems as $key => $order) {
            foreach ($order as $value) {
                $n++;
                $resultItems[$n] = $value;
            }
        }

        return $resultItems[$id];
    } // end _getIdByOrder

    public function pressReviewItemOrder($id)
    {
        return $this->current($this->_values['press_page_press_review_item_'.$id.'_order']);
    } // end pressReviewItemOrder

    public function contactsTitle()
    {
        return $this->current($this->_values['press_page_contacts_contacts_title']);
    } // end contactsTitle

    public function contactsPhoto()
    {
        return $this->current($this->_values['press_page_contacts_contact_photo']);
    } // end contactsPhoto

    public function contactsName()
    {
        return $this->current($this->_values['press_page_contacts_contact_name']);
    } // end contactsName

    public function contactsEmail()
    {
        return $this->current($this->_values['press_page_contacts_contact_email']);
    } // end contactsEmail

    public function contactsPhone()
    {
        return $this->current($this->_values['press_page_contacts_contact_phone']);
    } // end contactsPhone

    public function contactFormTitle()
    {
        return $this->current($this->_values['press_page_contacts_contact_form_title']);
    } // end contactFormTitle

    public function contactFormEmailPlaceholder()
    {
        return $this->current($this->_values['press_page_contacts_contact_form_email_placeholder']);
    } // end contactFormEmailPlaceholder

    public function contactFormButtonText()
    {
        return $this->current($this->_values['press_page_contacts_contact_form_button_text']);
    } // end contactFormButtonText

    public function pressReleasesTitle()
    {
        return $this->current($this->_values['press_page_press_releases_title']);
    } // end pressReleasesTitle

    public function pressReleasesItems()
    {
        $data = $this->current($this->_values['press_page_press_releases_items']);

        $this->_preparePressReleasesItems($data);

        return $data;
    } // end pressReleasesItems

    private function _preparePressReleasesItems(&$data)
    {
        foreach ($data as $ident => &$item) {
            foreach ($item as $key => &$value) {
                if ($key == 'date') {

                    $timestamp = strtotime($value);
                    $value = date("d/m/y", $timestamp);
                }
            }
        }
    } // end _preparePressReleasesItems

    public function downloadsTitle()
    {
        return $this->current($this->_values['press_page_downloads_title']);
    } // end downloadsTitle

    public function downloadsBlockImage($id, $imageID = false)
    {
        $optionName = 'press_page_downloads_block_'.$id.'_image';

        if ($imageID) {
            $optionName = $optionName.'_'.$imageID;
        }

        return $this->current($this->_values[$optionName]);
    } // end downloadsBlockImage

    public function downloadsBlockTitle($id)
    {
        return $this->current($this->_values['press_page_downloads_block_'.$id.'_title']);
    } // end downloadsBlockTitle

    public function downloadsBlockFile($id)
    {
        return $this->current($this->_values['press_page_downloads_block_'.$id.'_file']);
    } // end downloadsBlockFile

    public function downloadsBlockFileUrl($id)
    {
        $fileUrl = $this->downloadsBlockFile($id);

        $url = get_permalink().'?download='.$fileUrl;


        return $url;
    } // end downloadsBlockFileUrl
}