<?php
namespace kreatize\service;
require_once 'Options.php';

class ProcessArchivePageOptions extends Options
{
    private $_optionIdent = 'processes_archive_page';
    private $_values = array();

    protected function onInit()
    {
        $this->_values = $this->getFrontendOption($this->_optionIdent);
    } // end onInit

    public function archiveName()
    {
        return $this->current($this->_values['process_archive_name']);
    } // end name

    public function archiveTitle()
    {
        return $this->current($this->_values['process_archive_title']);
    } // end archiveTitle

    public function titleForAllItemsTab()
    {
        return $this->current($this->_values['process_archive_all_tab_title']);
    } // end titleForAllItemsTab

    public function listTagName($id)
    {
        $termId = $this->current($this->_values['process_archive_list_'.$id.'_tag']);

        $processes = $this->app->service('ProcessesPostType');

        return $processes->getTermNameById($termId);
    } // end listTagName

    public function listQueryByTag($id)
    {
        $terms = array();
        $idList = (is_array($id)) ? $id : array($id);

        foreach ($idList as $id) {
            $terms[] = $this->current($this->_values['process_archive_list_'.$id.'_tag']);
        }

        $processes = $this->app->service('ProcessesPostType');

        return $processes->getPostsQueryByTerms($terms, 'term_id');
    } // end listQueryByTag
}