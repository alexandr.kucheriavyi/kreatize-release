<?php
namespace kreatize\service;

use kreatize\abstracts\Service as Service;

class ProcessesPostType extends Service
{
    private $_processPostTypeKey = 'processes';
    private $_processTaxonomyKey = 'processes_types';

    public function init()
    {
        add_action('init', array(&$this, 'onRegisterInstance'));
    } // end init

    public function onRegisterInstance()
    {
        $this->_registerPostType();
        $this->_registerTaxonomy();
    } // end onRegisterInstance

    public function _registerPostType()
    {
        $labels = array(
            'name'          => 'Processes',
            'singular_name' => 'Process',
        );

        $args = array(
            'labels' => $labels,
            'public' => true,
            'publicly_queryable' => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => array( 'slug' => 'process' ),
            'capability_type'    => 'post',
            'has_archive'        => true,
            'hierarchical'       => false,
            'menu_position'      => null,
            'taxonomies'         => array($this->_processTaxonomyKey),
            'supports'           => array('title', 'editor',  'thumbnail')
        );

        register_post_type($this->_processPostTypeKey, $args);
    } // end _registerPostType

    private function _registerTaxonomy()
    {
        $labels = array(
            'name'              => 'Process Types',
            'singular_name'     => 'Process Type',
        );

        $args = array(
            'label'                 => '',
            'labels'                => $labels,
            'description'           => '',
            'public'                => true,
            'publicly_queryable'    => null,
            'show_in_nav_menus'     => false,
            'show_ui'               => true,
            'show_tagcloud'         => false,
            'show_in_rest'          => null,
            'rest_base'             => null,
            'hierarchical'          => true,
            'rewrite'               => true,
            'capabilities'          => array(),
            'meta_box_cb'           => null,
            'show_admin_column'     => false,
            '_builtin'              => false,
            'show_in_quick_edit'    => null,
        );

        register_taxonomy($this->_processTaxonomyKey, array($this->_processPostTypeKey), $args);
    } // end _registerTaxonomy
    
    public function getNewQueryPostsByTag($currentPageID)
    {
        $tags = wp_get_post_terms($currentPageID, $this->_processTaxonomyKey);

        $tags = wp_list_pluck($tags, 'slug');

        return $this->getPostsQueryByTerms($tags);
    } // end getNewQueryPostsByTag

    public function getPostsQueryByTerms($terms = array(), $field = 'slug')
    {
        $args = array(
            'posts_per_page' => -1,
            'post_type' => $this->_processPostTypeKey,
            'orderby' => 'ID',
            'order'   => 'ASC',
            'tax_query' => array(
                'relation' => 'OR',
                array(
                    'taxonomy' =>  $this->_processTaxonomyKey,
                    'field'    =>  $field,
                    'terms'    =>  $terms,
                    'operator' => 'IN',
                ),
            ),
        );

        return new \WP_Query($args);
    } // end getPostsQueryByTerms

    public function getTermNameById($termID)
    {
        $term = get_term($termID, $this->_processTaxonomyKey);

        if (!$term) {
            return $termID;
        }

        return $term->name;
    } // end getTermNameById

    public function getPostTypeKey()
    {
        return $this->_processPostTypeKey;
    } // end getPostTypeKey

    public function getProcessNameById($processID)
    {
        return get_the_title($processID);
    } // end getProcessNameById

    public function getProcessImageById($processID)
    {
        return get_the_post_thumbnail($processID, 'full');
    } // end getProcessImageById
}