<?php
namespace kreatize\service;
require_once 'Options.php';

class RapidPrototypingPageOptions extends Options
{
    private $_optionIdent = 'rapid_prototyping_page';
    private $_values = array();

    protected function onInit()
    {
        $this->_values = $this->getFrontendOption($this->_optionIdent);
    } // end onInit

    public function headerImage()
    {
        return $this->current($this->_values['rapid_prototyping_header_image']);
    } // end headerImage

    public function headerTitle()
    {
        return $this->current($this->_values['rapid_prototyping_header_title']);
    } // end headerTitle

    public function headerButtonText()
    {
        return $this->current($this->_values['rapid_prototyping_header_button_text']);
    } // end headerButtonText

    public function headerButtonPage()
    {
        return $this->current($this->_values['rapid_prototyping_header_button_page']);
    } // end headerButtonText

    public function headerButtonCustomUrl()
    {
        return $this->current($this->_values['rapid_prototyping_header_button_url']);
    } // end headerButtonCustomUrl

    public function headerButtonUrl()
    {
        $customUrl = $this->headerButtonCustomUrl();

        if ($customUrl) {
            return $customUrl;
        }

        $pageID = $this->headerButtonPage();

        if (!$pageID) {
            return '#';
        }

        $url = $this->app->facade->getPermalink($pageID);

        return $url;
    } // end headerButtonUrl

    public function cardsSectionText()
    {
        return $this->current($this->_values['rapid_prototyping_cards_text']);
    } // end cardsSectionText

    public function cardTitle($id)
    {
        return $this->current($this->_values['rapid_prototyping_card_'.$id.'_title']);
    } // end cardTitle

    public function cardText($id)
    {
        return $this->current($this->_values['rapid_prototyping_card_'.$id.'_text']);
    } // end cardText

    public function rapidTextSectionTitle()
    {
        return $this->current($this->_values['rapid_prototyping_rapid_text_title']);
    } // end rapidTextSectionTitle

    public function rapidTextSectionText($id)
    {
        return $this->current($this->_values['rapid_prototyping_rapid_text_text_'.$id]);
    } // end rapidTextSectionText

    public function rapidTextSectionButtonText()
    {
        return $this->current($this->_values['rapid_prototyping_rapid_text_button_text']);
    } // end rapidTextSectionButtonText

    public function rapidTextSectionButtonPage()
    {
        return $this->current($this->_values['rapid_prototyping_rapid_text_button_page']);
    } // end rapidTextSectionButtonPage

    public function rapidTextSectionCustomUrl()
    {
        return $this->current($this->_values['rapid_prototyping_rapid_text_button_url']);
    } // end rapidTextSectionCustomUrl

    public function rapidTextSectionButtonUrl()
    {
        $customUrl = $this->rapidTextSectionCustomUrl();

        if ($customUrl) {
            return $customUrl;
        }

        $pageID = $this->rapidTextSectionButtonPage();

        if (!$pageID) {
            return '#';
        }

        $url = $this->app->facade->getPermalink($pageID);

        return $url;
    } // end rapidTextSectionButtonUrl

    public function tabName($id)
    {
        return $this->current($this->_values['rapid_prototyping_tabs_section_tab_'.$id.'_name']);
    } // end tabName

    public function tabOneTitle()
    {
        return $this->current($this->_values['rapid_prototyping_tabs_section_tab_1_title']);
    } // end tabOneTitle

    public function tabOneText($id)
    {
        return $this->current($this->_values['rapid_prototyping_tabs_section_tab_1_text_'.$id]);
    } // end tabOneText

    public function tabOneQuote()
    {
        return $this->current($this->_values['rapid_prototyping_tabs_section_tab_1_quote']);
    } // end tabOneQuote

    public function tabOneQuoteAuthor()
    {
        return $this->current($this->_values['rapid_prototyping_tabs_section_tab_1_quote_author']);
    } // end tabOneQuoteAuthor

    public function tabOneImage()
    {
        return $this->current($this->_values['rapid_prototyping_tabs_section_tab_1_image']);
    } // end tabOneImage

    public function tabOneImageDescription()
    {
        return $this->current($this->_values['rapid_prototyping_tabs_section_tab_1_image_description']);
    } // end tabOneImageDescription

    public function tabTwoTitle()
    {
        return $this->current($this->_values['rapid_prototyping_tabs_section_tab_2_title']);
    } // end tabTwoTitle

    public function tabTwoText($id)
    {
        return $this->current($this->_values['rapid_prototyping_tabs_section_tab_2_text_'.$id]);
    } // end tabTwoText

    public function tabTwoButtonText()
    {
        return $this->current($this->_values['rapid_prototyping_tabs_section_tab_2_button_text']);
    } // end tabTwoButtonText

    public function tabTwoButtonPage()
    {
        return $this->current($this->_values['rapid_prototyping_tabs_section_tab_2_button_page']);
    } // end tabTwoButtonPage

    public function tabTwoButtonUrl()
    {
        return $this->current($this->_values['rapid_prototyping_tabs_section_tab_2_button_url']);
    } // end tabTwoButtonUrl

    public function tabTwoButtonContactPageUrl()
    {
        $customUrl = $this->tabTwoButtonUrl();

        if ($customUrl) {
            return $customUrl;
        }

        $pageID = $this->tabTwoButtonPage();

        if (!$pageID) {
            return '#';
        }

        $url = $this->app->facade->getPermalink($pageID);

        return $url;
    } // end tabTwoButtonContactPageUrl

    public function tabTwoProcess($id)
    {
        return $this->current($this->_values['rapid_prototyping_tabs_section_tab_2_process_'.$id]);
    } // end tabTwoProcess

    public function tabTwoProcessImage($id)
    {
        return $this->current($this->_values['rapid_prototyping_tabs_section_tab_2_process_'.$id.'_image']);
    } // end tabTwoProcess

    public function tabTwoProcessUrl($id)
    {
        return $this->current($this->_values['rapid_prototyping_tabs_section_tab_2_process_'.$id.'_url']);
    } // end tabTwoProcessUrl

    public function tabTwoProcessName($id)
    {
        $processes = $this->app->service('ProcessesPostType');
        $processID = $this->tabTwoProcess($id);

        return $processes->getTermNameById($processID);
    } // end tabTwoProcessName

    public function tabThreeColTitle($position)
    {
        return $this->current($this->_values['rapid_prototyping_tabs_section_tab_3_'.$position.'_col_title']);
    } // end tabThreeColTitle

    public function tabThreeColText($position)
    {
        return $this->current($this->_values['rapid_prototyping_tabs_section_tab_3_'.$position.'_col_text']);
    } // end tabThreeColText

    public function tabThreeColTableRows($position)
    {
        $data =  $this->current($this->_values['rapid_prototyping_tabs_section_tab_3_'.$position.'_col_table']);

        $rows = json_decode($data, true);

        return $rows;
    } // end tabThreeColTableRows
}