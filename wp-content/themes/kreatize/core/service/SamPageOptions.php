<?php
namespace kreatize\service;
require_once 'Options.php';

class SamPageOptions extends Options
{
    private $_optionIdent = 'sam_page';
    private $_values = array();

    protected function onInit()
    {
        $this->_values = $this->getFrontendOption($this->_optionIdent);
    } // end onInit

    public function headerImage()
    {
        return $this->current($this->_values['sam_page_header_image']);
    } // end headerImage

    public function headerTitle($id)
    {
        return $this->current($this->_values['sam_page_header_title_'.$id]);
    } // end headerTitle

    public function formEmailPlaceholder()
    {
        return $this->current($this->_values['sam_page_header_form_email_placeholder']);
    } // end formEmailPlaceholder

    public function formCompanyPlaceholder()
    {
        return $this->current($this->_values['sam_page_header_form_company_placeholder']);
    } // end formCompanyPlaceholder

    public function formButtonText()
    {
        return $this->current($this->_values['sam_page_header_form_button_text']);
    } // end formButtonText

    public function cardTitle($id)
    {
        return $this->current($this->_values['sam_page_card_'.$id.'_title']);
    } // end cardTitle

    public function cardText($id)
    {
        return $this->current($this->_values['sam_page_card_'.$id.'_text']);
    } // end cardText

    public function footerTitle()
    {
        return $this->current($this->_values['sam_page_footer_title']);
    } // end footerTitle

    public function footerText()
    {
        return $this->current($this->_values['sam_page_footer_text']);
    } // end footerText

    public function footerButtonText()
    {
        return $this->current($this->_values['sam_page_footer_button_text']);
    } // end footerButtonText

//    public function footerButtonPage()
//    {
//        return $this->current($this->_values['sam_page_footer_button_page']);
//    } // end footerButtonPage
//
//    public function footerButtonUrl()
//    {
//        return $this->current($this->_values['sam_page_footer_button_url']);
//    } // end footerButtonUrl
//
//    public function signUpButtonUrl()
//    {
//        $customUrl = $this->footerButtonUrl();
//
//        if ($customUrl) {
//            return $customUrl;
//        }
//
//        $pageID = $this->footerButtonPage();
//
//        if (!$pageID) {
//            return '#';
//        }
//
//        $url = $this->app->facade->getPermalink($pageID);
//
//        return $url;
//    } // end signUpButtonUrl

    public function samSaisInHeader()
    {
        return $this->current($this->_values['sam_page_sam_sais_in_header']);
    } // end samSaisInHeader

    public function samSaisInFooter()
    {
        return $this->current($this->_values['sam_page_sam_sais_in_footer']);
    } // end samSaisInFooter
}