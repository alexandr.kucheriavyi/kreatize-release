<?php
namespace kreatize\service;
require_once 'Options.php';

class SignupPageOptions extends Options
{
    private $_optionIdent = 'signup_page';
    private $_values = array();

    protected function onInit()
    {
        $this->_values = $this->getFrontendOption($this->_optionIdent);
    } // end onInit

    public function headerImage()
    {
        return $this->current($this->_values['signup_page_header_image']);
    } // end headerImage

    public function headerText()
    {
        return $this->current($this->_values['signup_page_header_text']);
    } // end headerText

    public function formNamePlaceholder()
    {
        return $this->current($this->_values['signup_page_header_form_name']);
    } // end formNamePlaceholder

    public function formEmailPlaceholder()
    {
        return $this->current($this->_values['signup_page_header_form_email']);
    } // end formEmailPlaceholder

    public function formPhonePlaceholder()
    {
        return $this->current($this->_values['signup_page_header_form_phone']);
    } // end formPhonePlaceholder

    public function formCompanyPlaceholder()
    {
        return $this->current($this->_values['signup_page_header_form_company']);
    } // end formCompanyPlaceholder

    public function formButtonText()
    {
        return $this->current($this->_values['signup_page_header_form_button_text']);
    } // end formButtonText

    public function cardsBottomText()
    {
        return $this->current($this->_values['signup_page_header_cards_general_text']);
    } // end cardsBottomText

    public function cardTitle($id)
    {
        return $this->current($this->_values['signup_page_header_cards_'.$id.'_title']);
    } // end cardTitle

    public function cardText($id)
    {
        return $this->current($this->_values['signup_page_header_cards_'.$id.'_text']);
    } // end cardText
}