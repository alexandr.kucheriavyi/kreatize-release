<?php
namespace kreatize\service;
require_once 'Options.php';

class SupplierPageOptions extends Options
{
    private $_optionIdent = 'supplier_page';
    private $_values = array();

    protected function onInit()
    {
        $this->_values = $this->getFrontendOption($this->_optionIdent);
    } // end onInit

    public function headerImage()
    {
        return $this->current($this->_values['supplier_page_header_image']);
    } // end headerImage

    public function headerText()
    {
        return $this->current($this->_values['supplier_page_header_text']);
    } // end headerText

    public function headerFormNamePlaceholder()
    {
        return $this->current($this->_values['supplier_page_header_form_name']);
    } // end headerFormNamePlaceholder

    public function headerFormEmailPlaceholder()
    {
        return $this->current($this->_values['supplier_page_header_form_email']);
    } // end headerFormEmailPlaceholder

    public function headerFormNamePhonePlaceholder()
    {
        return $this->current($this->_values['supplier_page_header_form_phone']);
    } // end headerFormNamePhonePlaceholder

    public function headerFormCompanyPlaceholder()
    {
        return $this->current($this->_values['supplier_page_header_form_company']);
    } // end headerFormCompanyPlaceholder

    public function headerFormButtonText()
    {
        return $this->current($this->_values['supplier_page_header_form_button_text']);
    } // end headerFormButtonText

    public function cardTitle($id)
    {
        return $this->current($this->_values['supplier_page_cards_'.$id.'_title']);
    } // end cardTitle

    public function cardText($id)
    {
        return $this->current($this->_values['supplier_page_cards_'.$id.'_text']);
    } // end cardText

    public function cardsSectionText()
    {
        return $this->current($this->_values['supplier_page_cards_general_text']);
    } // end cardsSectionText

    public function cardsSectionMobileText()
    {
        return $this->current($this->_values['supplier_page_cards_general_text_mobile']);
    } // end cardsSectionMobileText

    public function profileItemTitle($id)
    {
        return $this->current($this->_values['supplier_page_profile_item_'.$id.'_title']);
    } // end profileItemTitle

    public function profileItemText($id)
    {
        return $this->current($this->_values['supplier_page_profile_item_'.$id.'_text']);
    } // end profileItemText

    public function profileItemImage($id)
    {
        return $this->current($this->_values['supplier_page_profile_item_'.$id.'_image']);
    } // end profileItemImage

    public function advantagesItemTitle($id)
    {
        return $this->current($this->_values['supplier_page_advantages_item_'.$id.'_title']);
    } // end advantagesItemTitle

    public function advantagesItemText($id, $idText)
    {
        return $this->current($this->_values['supplier_page_advantages_item_'.$id.'_text_'.$idText]);
    } // end advantagesItemText

    public function advantagesItemImage($id)
    {
        return $this->current($this->_values['supplier_page_advantages_item_'.$id.'_image']);
    } // end advantagesItemImage

    public function advantagesItemImageDescription($id)
    {
        return $this->current($this->_values['supplier_page_advantages_item_'.$id.'_image_description']);
    } // end advantagesItemImageDescription

    public function advantagesItemDisplayButton($id)
    {
        return $this->current($this->_values['supplier_page_advantages_item_'.$id.'_display_button']);
    } // end advantagesItemDisplayButton

    public function isEnabledAdvantagesButton($id)
    {
        $value = $this->advantagesItemDisplayButton($id);

        return $value == 'on';
    } // end isEnabledAdvantagesButton

    public function advantagesItemButtonText($id)
    {
        return $this->current($this->_values['supplier_page_advantages_item_'.$id.'_button_text']);
    } // end advantagesItemButtonText

}