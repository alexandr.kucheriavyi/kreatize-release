<?php
namespace kreatize\service;
require_once 'Options.php';

class TermsAndConditionsPageOptions extends Options
{
    private $_optionIdent = 'terms_and_conditions';
    private $_values = array();

    protected function onInit()
    {
        $this->_values = $this->getFrontendOption($this->_optionIdent);
    } // end onInit

    public function headerImage()
    {
        return $this->current($this->_values['header_image']);
    } // end headerImage

    public function headerTitle()
    {
        return $this->current($this->_values['header_title']);
    } // end headerTitle

    public function tabName($id)
    {
        return $this->current($this->_values['tab_'.$id.'_name']);
    } // end tabName


    public function tabOneItems()
    {
        return $this->current($this->_values['tab_1_items_list']);
    } // end tabOneItems

    public function tabTwoText()
    {
        return $this->current($this->_values['tab_2_text']);
    } // end tabTwoText

    public function tabThreeText()
    {
        return $this->current($this->_values['tab_3_text']);
    } // end tabThreeText
}