<?php
namespace kreatize\service;
require_once 'Options.php';

class ThankyouPageOptions extends Options
{
    private $_optionIdent = 'thankyou';
    private $_values = array();

    protected function onInit()
    {
        $this->_values = $this->getFrontendOption($this->_optionIdent);
    } // end onInit

    public function title()
    {
        return $this->current($this->_values['title']);
    } // end title

    public function text($id)
    {
        return $this->current($this->_values['text_'.$id]);
    } // end text
}