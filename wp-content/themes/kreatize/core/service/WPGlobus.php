<?php
namespace kreatize\service;

use kreatize\abstracts\Service as Service;

class WPGlobus extends Service
{
   public function getEnabledLanguageData()
   {
       $enabledLanguage = \WPGlobus::config()->enabled_languages;
       $current = \WPGlobus::config()->language;

       $url = \WPGlobus_Utils::current_url();

       foreach ($enabledLanguage as $lang) {
           $data[$lang] = array(
               'url' => \WPGlobus_Utils::localize_url($url, $lang)
           );

           if ($lang == $current) {
               $data[$lang]['current'] = true;
           }
       }

       return $data;
   } // end getEnabledLanguageData

    public function isEnglish()
    {
        $current = \WPGlobus::Config()->language;

        return $current == 'en';
    } // end isEnglish
}