<form  method="post" class="form-horizontal">
    <fieldset>

        <div class="form-group">
            <label class="col-md-4 control-label" for="from">From</label>
            <div class="col-md-5">
                <input value="<?php echo $from; ?>" id="from" name="from" type="text" placeholder="" class="form-control input-md" required="">

            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="to">To</label>
            <div class="col-md-5">
                <input id="to" name="to" value="<?php echo $to; ?>" type="text" placeholder="" class="form-control input-md" required="">

            </div>
        </div>

        <!-- Multiple Radios -->
        <div class="form-group">
            <label class="col-md-4 control-label" for="mode">Mode</label>
            <div class="col-md-4">
                <div class="radio">
                    <label for="mode-0">
                        <?php
                        $checked = ($mode == 'test') ? ' checked="checked" ' : '';
                        ?>
                        <input type="radio" name="mode" id="mode-0" value="test" <?php echo $checked; ?>>
                        Test
                    </label>
                </div>
                <div class="radio">
                    <label for="mode-1">
                        <?php
                        $checked = ($mode == 'real') ? ' checked="checked" ' : '';
                        ?>
                        <input type="radio" name="mode" id="mode-1" value="real" <?php echo $checked; ?>>
                        Real
                    </label>
                </div>
            </div>
        </div>


        <!-- Button -->
        <div style="margin-top: 40px" class="form-group">
            <label class="col-md-4 control-label" for="__do_migrate"></label>
            <div class="col-md-4">
                <button value="1" id="__do_migrate" name="__do_migrate" class="btn btn-primary">Run</button>
            </div>
        </div>

    </fieldset>
</form>
