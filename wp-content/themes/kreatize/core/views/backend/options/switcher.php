<li class="options-switcher">
    <span>Select Option</span>
    <form id="options-switcher-form"  method="post">
        <select name="options_switcher">
<?php
            foreach ($options as $ident => $name) {
                $selected = ($ident == $current) ? 'selected="selected"' : '';
?>
                <option <?php echo $selected; ?>  value="<?php echo $ident; ?>"><?php echo $name; ?></option>
<?php
            }
?>
        </select>
        <input value="1" type="hidden" name="__options_switcher">
    </form>
</li>