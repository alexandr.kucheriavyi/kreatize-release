<?php
/* @languageList
 */
?>

<li>
<?php
$isFirts = true;
foreach ($languageList as $ident => $lang) {
    $styles = ($isFirts) ? 'style="padding-right:3px"' : '';
    $active = (array_key_exists('current', $lang)) ? ' active ' : '';
?>
    <?php echo (!$isFirts) ? '|' : '';?>
    <a id="<?php echo $ident; ?>" <?php echo  $styles;?> onclick="changeLanguage('<?php echo $ident; ?>')" class="text-uppercase language <?php echo $active; ?>" href="<?php echo $lang['url']; ?>"><?php echo $ident; ?></a>
<?php
    $isFirts = false;
}
?>
</li>
