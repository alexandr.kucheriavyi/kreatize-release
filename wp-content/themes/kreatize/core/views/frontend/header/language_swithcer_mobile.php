<?php
/* @languageList
 */
?>

<li class="visible-xs language">
    <?php
    $isFirts = true;
    foreach ($languageList as $ident => $lang) {
        $active = (array_key_exists('current', $lang)) ? ' active ' : '';
        ?>
        <?php echo (!$isFirts) ? '|' : '';?>
        <a id="<?php echo $ident; ?>" onclick="changeLanguage('<?php echo $ident; ?>')" class="text-uppercase language <?php echo $active; ?>" href="<?php echo $lang['url']; ?>"><?php echo $ident; ?></a>
        <?php
        $isFirts = false;
    }
    ?>
</li>