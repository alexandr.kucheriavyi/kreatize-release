<?php
$menu = $options->getMenuData();
?>

<?php
foreach ($menu as $item) {
    $active = (array_key_exists('current', $item)) ? 'btn-active' : '';
?>
    <li>
        <a href="<?php echo $item['url']; ?>" class="btn btn-primary btn-sm navbar-btn text-uppercase <?php echo $active; ?>">
            <strong><?php echo $item['title']; ?></strong>
        </a>
    </li>
<?php
}
?>
