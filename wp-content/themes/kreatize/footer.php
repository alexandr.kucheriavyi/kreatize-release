<?php
require_once 'core/App.php';
use kreatize\App as App;
$kreatize = App::getInstance();
$options =  $kreatize->service('footerOptions');
?>

<footer id="footer">
    <div class="container" style="padding-left: 3px;">
        <div class="col-md-3 col-sm-6 col-xs-6 footer-logo" style="text-align: left;">
            <img src="<?php echo $options->logo(); ?>" width="100%" alt="KREATIZE">
        </div>
        <div class="social col-md-3 col-sm-6 col-xs-6 footer-social" style="margin-top: 0px;">
            <div class="col-md-6 social-icons">
                <a href="<?php echo $options->facebookUrl(); ?>" class="fa fa-facebook" title="" target="_blank"></a>
                <a href="<?php echo $options->linkedinUrl(); ?>" class="fa fa-linkedin" title="" target="_blank"></a>
                <a href="<?php echo $options->twitterUrl(); ?>" class="fa fa-twitter" title="" target="_blank"></a>
                <a href="<?php echo $options->youTubeUrl(); ?>" class="fa fa-youtube" title="" target="_blank"></a>
                <!--<a href="https://blog.kreatize.com" class="kr-blog" title="" target="_blank"><img src="/wp-content/themes/kreatize/img/kr-blog.png" width="26" height="26" alt="blog"/></a>-->
            </div>
            <div class="col-md-6 hidden-xs hidden-sm">
                <h3 class="cl-titil-20 news-footer"><?php echo $options->formLabel(); ?></h3>
            </div>
        </div>
        <div class="wrapping col-md-6 col-sm-12 col-xs-12 footer-letter">
            <div class="col-md-10 col-sm-12 col-xs-12" style="margin-top: -5px;"><div class="triangle-right"></div>
                <div class="newsletter-signup col-sm-12 col-xs-12">
                    <?php
                    $mailChimp = $kreatize->service('MailChimpForm');
                    $content =  do_shortcode('[mc4wp_form id="193"]');
                    echo $mailChimp->replaceFooterFormValues($content);
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="container" style="padding-left: 3px;">
        <div class="nav-items">
            <div class="col-md-3">
            </div>
            <div class="col-md-3 social">


                <div class="col-md-6 cl-titil-16-30 first-footer " style="margin-left: -35px;margin-right: 26px;">


                    <strong class="cl-titil-16-22"><?php echo $options->menuBlockCaption(1); ?></strong><br>
<?php
                    $menu = $options->getMenuData(1);
                    foreach ($menu as $item) {
?>
                        <a href="<?php echo $item['url']; ?>">
                            <?php echo $item['title']; ?>
                        </a><br>
<?php
                    }
?>
                </div>
                <div class="col-md-6 cl-titil-16-30 second-footer footer-col">

                    <strong class="cl-titil-16-22"><?php echo $options->menuBlockCaption(2); ?></strong><br>
                    <?php
                    $menu = $options->getMenuData(2);
                    foreach ($menu as $item) {
                        ?>
                        <a href="<?php echo $item['url']; ?>">
                            <?php echo $item['title']; ?>
                        </a><br>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <div class="social col-md-3 cl-titil-16-30 footer-col third-footer">
                <div class="padd col-md-6">
                    <!--<a href="#">About Us</a><br/>-->
                    <strong class="cl-titil-16-22"><?php echo $options->menuBlockCaption(3); ?></strong><br>
                    <?php
                    $menu = $options->getMenuData(3);
                    foreach ($menu as $item) {
                        ?>
                        <a href="<?php echo $item['url']; ?>">
                            <?php echo $item['title']; ?>
                        </a><br>
                        <?php
                    }
                    ?>

                </div>
                <div class="col-md-6 cl-titil-16-30 footer-col">
                    <strong class="cl-titil-16-22"><?php echo $options->menuBlockCaption(4); ?></strong><br>
                    <?php
                    $menu = $options->getMenuData(4);
                    foreach ($menu as $item) {
                        ?>
                        <a href="<?php echo $item['url']; ?>">
                            <?php echo $item['title']; ?>
                        </a><br>
                        <?php
                    }

                    //TODO
                    ?>
<!--                    <a href="agb.html#impressum" onclick="goToDI('#impressum')">Impressum</a><br>-->

                </div>
            </div>

            <div class="col-md-3 col-sm-0">
            </div>
        </div>
    </div>
    <div class="text-center visible-xs-d" style="margin-left: 38px;">
        <p style="display: inline;" >© 2016 KREATIZE® GmbH</p>
        <a href="#" id="to-top-mb" class="back-to-top btn-roll" style="font-size:26px; margin-left:10px;"><i class="fa fa-angle-up"></i></a>
    </div>
    <div class="text-center hidden-xss-d">
        <hr>
        <p class="copyright cl-titil-16-30" style="display: inline;" >© 2016 KREATIZE® GmbH - Rittweg 43, 72070 Tübingen, Germany</p>
        <a href="#" id="to-top" class="back-to-top btn-roll"><i class="fa fa-angle-up"></i></a>
    </div>
</footer>
<?php wp_footer(); ?>

<div class="modal fade" id="play_video" tabindex="-1" role="dialog" aria-labelledby="modal-video-label">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="modal-video">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe width="1280" height="720" src="" frameborder="0" allowfullscreen></iframe>
                        <!--<iframe class="embed-responsive-item" src=""
                                webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>






</body>

</html>