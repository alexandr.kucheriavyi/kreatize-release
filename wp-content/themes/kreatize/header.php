<?php
require_once 'core/App.php';
use kreatize\App as App;
$kreatize = App::getInstance();
$options =  $kreatize->service('headerOptions');
?>


<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" />

    <?php wp_head(); ?>
</head>

<body id="page-top" <?php body_class('index'); ?> >

<!-- Navigation -->


<!-- Header -->
<header id="header">
    <nav class="navbar navbar-default navbar-fixed-top affix-top">
        <div class="navbar-top container-fluid">
            <div class="beta-text">
                <div class="navbar-brand" style="display:inline-block;">
                    <div class="beta">
                       <?php echo $options->betaText();?>
                    </div>
                </div>
                <div class="" style="display:inline-block;">
                    <div class="right-angle"></div>
                </div>
            </div>
            <!--xs-->
            <ul class="batman-navigation navbar-toggle collapsed">
                <li><a href="tel:<?php echo $options->clearePhone();?>"><span class="fa fa-phone fa-rotate-270" aria-hidden="false"></span></a></li>
                <li><a href="<?php echo $options->contactPageUrl();?>"><span class="fa fa-envelope" aria-hidden="false"></span></a></li>
                <li><a href="#" data-toggle="collapse" data-target="#bs-example-navbar-collapse"><span class="fa fa-navicon"></span></a></li>
            </ul>
            <div class="container">
                <!--sm, md, xl-->
                <div class="col-md-12 col-sm-12 hidden-xs hidden-sm" style="display: inline-block; padding: 0px;">
                    <ul class="nav navbar-nav pull-right nav-utilities">
                        <li><a><span class="fa-nav fa fa-phone fa-rotate-270" aria-hidden="false"></span><span class="titles"> <?php echo $options->phone();?></span></a></li>
                        <li><a href="<?php echo $options->contactPageUrl();?>"><span class="fa-nav fa fa-envelope" aria-hidden="false"></span><span class="titles"> <?php echo $options->contactTitle();?></span></a></li>
                        <?php echo $kreatize->service('languageSwitcher')->desktop();?>



                    </ul>
                </div>
            </div>
        </div>
        <div class="container s-row">
            <a class="navbar-brand" href="/">
                <img class="img-responsive" alt="Kreatize" src="<?php echo $options->logo();?>">
            </a>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse" aria-expanded="true">

                <ul class="nav navbar-nav main-nav pull-right" style="position: relative;">
                    <?php echo  $kreatize->render('frontend/header/menu.php', array('options' => $options)); ?>

                    <li class="hidden-xs hidden-sm"><a href="#" id="login" class="btn btn-primary btn-sm navbar-btn text-uppercase loginss"><i class="fa fa-lock" aria-hidden="true"></i> <strong><?php echo $options->loginButtonCaption();?></strong></a></li>
                    <li class="visible-xs visible-sm"><a href="#" id="login-sm"  class="btn btn-primary btn-sm navbar-btn text-uppercase"  data-toggle="modal" data-target="#modal-login"><i class="fa fa-lock" aria-hidden="true"></i> <strong><?php echo $options->loginButtonCaption();?></strong></a></li>

                    <div id="form-login" class="hidden-xs hidden-sm">
                             <form >
                            <i class="fa fa-user" aria-hidden="true" style="position: absolute; left: 16px; top: 17px;"></i>
                            <input type="text" class="form-control" placeholder="<?php echo $options->loginFieldPlaceholder();?>" style="padding-left: 30px; margin-bottom: 5px;">
                            <i class="fa fa-lock" aria-hidden="true" style="position: absolute; left: 16px; top: 57px;"></i>
                            <input type="password" class="form-control" placeholder="<?php echo $options->passwordFieldPlaceholder();?>" style="padding-left: 30px; margin-bottom: 5px;">
                            <button type="submit" class="btn btn-primary btn-active"><i class="fa fa-user" style="font-size: 10px; " aria-hidden="true"></i> <strong><?php echo $options->submitButtonCaption();?></strong></button>
                            <a href="<?php echo $options->forgotPasswordUrl();?>" style="color: #fff; font-size: 10px;"><?php echo $options->forgotPasswordText();?></a>
                        </form>
                    </div>

                    <!--<li><a href="" class="btn btn-primary btn2 btn-sm navbar-btn text-uppercase"><span class="fa fa-lock"></span><strong> Login</strong></a></li>-->
                    <!--<li class="visible-xs"><a href="#" onclick="changeLanguage('de')" class="btn btn-primary btn2 btn-sm navbar-btn text-uppercase"><strong>DEUTSCH</strong></a></li>-->
                    <?php echo $kreatize->service('languageSwitcher')->mobile(); ?>

                </ul>
            </div>
        </div>
    </nav>
</header>

<div id="modal-login" class="modal fade">
    <div class="modal-dialog" >
        <div class="modal-content" >
            <!-- Заголовок модального окна -->
            <div class="modal-header" >
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <!-- Основное содержимое модального окна -->
            <div class="modal-body" >
                <div id="">
                    <form >
                        <i class="fa fa-user" aria-hidden="true" style="position: absolute; left: 25px; top: 27px;"></i>
                        <input type="text" class="form-control" placeholder="<?php echo $options->loginFieldPlaceholder();?>" style="padding-left: 30px; margin-bottom: 5px;">
                        <i class="fa fa-lock" aria-hidden="true" style="position: absolute; left: 25px; top: 67px;"></i>
                        <input type="password" class="form-control" placeholder="<?php echo $options->passwordFieldPlaceholder();?>" style="padding-left: 30px; margin-bottom: 5px;">
                        <button type="submit" class="btn btn-primary btn-active" style="width: 50%;"><i class="fa fa-user" style="font-size: 10px;" aria-hidden="true"></i> <strong><?php echo $options->submitButtonCaption();?></strong></button>
                        <div class="modal-forget"><a href="<?php echo $options->forgotPasswordUrl();?>" style="color: #000;"><?php echo $options->forgotPasswordText();?></a></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

