<?php
require_once 'core/App.php';
use kreatize\App as App;
$kreatize = App::getInstance();
$options =  $kreatize->service('homePageOptions');
?>

<?php get_header(); ?>
    <div id="content" class="services">
        <section class="intro" id="intro" style="
                background: -webkit-linear-gradient(rgba(45, 83, 111, 0.7), rgba(45, 83, 111, 0.7)), url(<?php echo $options->videoImage(); ?>) no-repeat center center;
                background: linear-gradient(rgba(45, 83, 111, 0.7), rgba(45, 83, 111, 0.7)), url(<?php echo $options->videoImage(); ?>) no-repeat center center;
                background-size: cover;
                ">
            <div class="container text-center">
                <h1 class="cl-titil-65"><?php echo $options->videoText(); ?></h1>
                <button class="btn btn-lg video" data-video="<?php echo $options->videoUrl(); ?>" data-toggle="modal" data-target="#play_video"><img class="playbutton" src="/wp-content/themes/kreatize/img/video_button.png"/></button>
                <a href="<?php echo $options->videoButtonUrl(); ?>" class="btn btn-primary navbar-btn text-uppercase btnUpload btn-button"><strong><?php echo $options->videoButtonText(); ?></strong></a>
            </div>
        </section>

        <section class="container-fluid suppliers">
            <div class="flexslider">
                <ul class="slides">
                    <li class="slide-one">
                        <!--slide 1-->
                        <div class="slide-wrap">
                            <h1 class="text-center cl-titil-40">In 4 Schritten zum fertigen Bauteil.</h1>
                            <div class="col-md-8 col-md-offset-2"><h3 class="text-center cl-titil-20">Der intelligenteste Match für Fertigungsverfahren, Werkstoff und Fertigungsdienstleister – einfach, schnell und sicher.</h3></div>
                            <div class="row container">

                                <div class="col-md-3 col-xs-12">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <img src="/wp-content/themes/kreatize/img/img-suppliers-upload.png" alt="">
                                    </div>
                                    <div class="step-texts col-md-6 col-sm-6 col-xs-6">
                                        <h2 class="cl-titil-20">1. Hochladen</h2>
                                        <p>Laden Sie einfach 3D-CAD-Modelle oder Zeichnungen Ihrer Bauteile hoch.</p>
                                    </div>
                                </div>

                                <div class="col-md-3 col-xs-12">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <img src="/wp-content/themes/kreatize/img/img-suppliers-maching.png" alt="">
                                    </div>
                                    <div class="step-texts col-sm-6 col-xs-6">
                                        <h2 class="cl-titil-20">2. Matching</h2>
                                        <p>Passend zu Ihren Bauteilen ordnen wir Fertigungsverfahren und Werkstoffe zu.</p>
                                    </div>
                                </div>

                                <div class="col-md-3 col-xs-12">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <img src="/wp-content/themes/kreatize/img/img-suppliers-select.png" alt="">
                                    </div>
                                    <div class="step-texts col-md-6 col-sm-4 col-xs-4">
                                        <h2 class="cl-titil-20">3. Anbieter</h2>
                                        <p>Wählen Sie den Fertigungsdienstleister, der sich am besten für Ihr Projekt eignet.</p>
                                    </div>
                                </div>

                                <div class="col-md-3 col-xs-12">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <img src="/wp-content/themes/kreatize/img/img-suppliers-shipping.png" alt="">
                                    </div>
                                    <div class="step-texts col-md-6 col-sm-6 col-xs-6">
                                        <h2 class="cl-titil-20">4. Versand</h2>
                                        <p>Ihre Bauteile werden vom Anbieter direkt zu Ihnen versandt.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end of slide 1-->
                    </li>
                    <li class="slide-two">
                        <!--slide 2-->
                        <div class="slide-wrap">
                            <h1 class="text-center cl-titil-40">Online-Verwaltung</h1>
                            <div class="row container online-administration">
                                <div class="col-md-4 col-xs-12">
                                    <img src="/wp-content/themes/kreatize/img/img-order-management-tool.png" alt=""> <h3 class="cl-titil-20">Auftragsmanagement-Tool</h3>
                                    <p class="cl-sans-16-22">Mit den KREATIZE Auftragsmanagementtools können Sie Ihre Bestellungen verfolgen und den Produktionsprozess nachvollziehen.</p>

                                    <img src="/wp-content/themes/kreatize/img/img-order-stacking-tool.png" alt=""> <h3 class="cl-titil-20">Auftragstracking-Tool</h3>
                                    <p class="cl-sans-16-22">Sie können jederzeit den Status Ihrer Bestellungen online abfragen.</p>
                                </div>

                                <div class="col-md-4 col-xs-12">
                                    <img src="/wp-content/themes/kreatize/img/img-supplier-network-tool.png" alt=""> <h3 class="cl-titil-20">Anbieter-Netzwerk-Tool</h3>
                                    <p class="cl-sans-16-22">Über das Anbieter-Netzwerk-Tool können Sie präferierte Fertigungsdienstleister auswählen, die Sie zu Ihrem Anbieter-Netzwerk hinzufügen möchten.</p>

                                    <img src="/wp-content/themes/kreatize/img/img-reporting-tool.png" alt=""> <h3 class="cl-titil-20">Reporting Tool</h3>
                                    <p class="cl-sans-16-22">Über die Reporting-Tools sehen Sie Ihre Aktivitäten auf einen Blick.
                                        Damit Sie Ihre Aktivitäten in Ihr eigenes System integrieren können, stehen alle Reportings auch zum Download als Excel- und CSV-
                                        Dateien für Sie zur Verfügung.
                                    </p>
                                </div>

                                <div class="col-md-4 col-xs-12">
                                </div>
                            </div>
                        </div>
                        <!--end of slide 2-->
                    </li>
                    <li class="slide-three">
                        <!--slide 3-->
                        <div class="slide-wrap">
                            <h1 class="text-center cl-titil-40">Sicherheit und Vertrauen</h1>
                            <div class="row container">

                                <div class="col-md-3 col-xs-12">
                                    <img src="/wp-content/themes/kreatize/img/img-data-security.png" alt=""> <h3 class="cl-titil-20">Datensicherheit</h3>
                                    <p>Mit der Einbindung von modernsten Verschlüsselungstechnologien garantieren wir die Sicherheit Ihrer Daten.</p>
                                    <a class="kr-plus-circle" data-toggle="collapse" data-target="#step-one"><img src="/wp-content/themes/kreatize/img/kr-plus-circle.png"/><img style="display:none;" src="/wp-content/themes/kreatize/img/kr-minus-circle.png"/></a>
                                    <div id="step-one" class="collapse">
                                        <p>We develop our service with a focus on the security of your data from the beginning. All our servers are on German soil and are well-protected by the Federal Data Protection Act. We comply with all guidelines of the data protection act.</p>
                                    </div>
                                </div>

                                <div class="col-md-3 col-xs-12">
                                    <img src="/wp-content/themes/kreatize/img/img-secrecy.png" alt=""> <h3 class="cl-titil-20">Geheimhaltung</h3>
                                    <p>KREATIZE® verpflichtet alle Geschäftspartner zur Geheimhaltung, um die Vertraulichkeit Ihrer Daten zu gewährleisten.</p>
                                    <a class="kr-plus-circle" data-toggle="collapse" data-target="#step-two"><img src="/wp-content/themes/kreatize/img/kr-plus-circle.png"/><img style="display:none;" src="/wp-content/themes/kreatize/img/kr-minus-circle.png"/></a>
                                    <div id="step-two" class="collapse">
                                        <p>With the upload of your data our non-disclosure agreement (NDA) automatically comes into force in order to protect your data. In this manner you are able fully focus on the the realization of your custom parts.</p>
                                    </div>
                                </div>

                                <div class="col-md-3 col-xs-12">
                                    <img src="/wp-content/themes/kreatize/img/img-payment-security.png" alt=""> <h3 class="cl-titil-20">Zahlungssicherheit</h3>
                                    <p>Wenn Sie über KREATIZE®-Payment-Tools bezahlen, sind Sie bei mangelhafter Lieferung komplett abgesichert. </p>
                                    <a class="kr-plus-circle" data-toggle="collapse" data-target="#step-three"><img src="/wp-content/themes/kreatize/img/kr-plus-circle.png"/><img style="display:none;" src="/wp-content/themes/kreatize/img/kr-minus-circle.png"/></a>
                                    <div id="step-three" class="collapse">
                                        <p>KREATIZE® kümmert sich als Treuhand bei jeglichen Mängeln (bspw. bei ausbleibender  Lieferung oder Minderqualität) um eine Lösung für Sie. Sollten Sie sich dennoch entschließen von der Bestellung zurückzutreten, erhalten Sie Ihr Geld unverzüglich zurück.</p>
                                    </div>
                                </div>

                                <div class="col-md-3 col-xs-12">
                                    <img src="/wp-content/themes/kreatize/img/img-certification.png" alt=""> <h3 class="cl-titil-20">Zertifizierung</h3>
                                    <p>KREATIZE® ist überzeugt, dass nur die höchsten Qualitäts- und Sicherheitsstandards gut genug für unsere Kunden sind.</p>
                                    <a class="kr-plus-circle" data-toggle="collapse" data-target="#step-four"><img src="/wp-content/themes/kreatize/img/kr-plus-circle.png"/><img style="display:none;" src="/wp-content/themes/kreatize/img/kr-minus-circle.png"/></a>
                                    <div id="step-four" class="collapse">
                                        <p>For this reason we audit suppliers at KREATIZE® on a regular basis. Suppliers who successfully pass the audit receive a KREATIZE certificate. This may help you with the choice of the right supplier. Besides, many suppliers have common quality management certificates such as ISO 9001 or EN 9100. Should you require special certificates you may state this during the matching process so that you are only proposed suppliers that satisfy your requirements.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end of slide 3-->
                    </li>
                </ul>
            </div>
        </section>

        <section class="manu-process">
            <div class="container">
                <h1 class="text-center cl-titil-40">Über 20 verschiedene Fertigungsverfahren an einem einzigen Ort.</h1>
                <button onclick="location.href='/unsere-verfahren.html';" class="btn btn-primary text-uppercase visible-xs" style="margin:auto;"><strong>unsere Verfahren</strong></button>
                <div role="tabpanel" class="hidden-xs">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs nav-pills" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#panel-1" aria-controls="panel-1" role="tab" data-toggle="tab" class="cl-titil-28">Additive Fertigung</a>
                        </li>
                        <li role="presentation">
                            <a href="#panel-2" aria-controls="panel-2" role="tab" data-toggle="tab" class="cl-titil-28">CNC-Berarbeitung</a>
                        </li>
                        <li role="presentation" class="guss">
                            <a href="#panel-3" aria-controls="panel-3" role="tab" data-toggle="tab" class="cl-titil-28">Guss</a>
                        </li>
                    </ul>
                    <!-- Tab panels -->
                    <div class="tab-content">

                        <div role="tabpanel" class="tab-pane active" id="panel-1">
                            <div class="row masonry-container">

                                <a class="process-link" href="/unsere-verfahren/selektives-laserschmelzen.html">
                                    <div class="col-md-3 col-sm-6 item" onClick="" data>
                                        <img src="/wp-content/themes/kreatize/img/img-selektives-laserschmelzen.svg" alt="">
                                        <h3 class="cl-sans-16-22">Selektives Laserschmelzen</h3>
                                    </div>
                                </a>

                                <a class="process-link" href="/unsere-verfahren/binder-jetting.html">
                                    <div class="col-md-3 col-sm-6 item">
                                        <img src="/wp-content/themes/kreatize/img/img-binder-jetting.svg" alt="">
                                        <h3 class="cl-sans-16-22">Binder Jetting</h3>
                                    </div>
                                </a>

                                <a class="process-link" href="/unsere-verfahren/elektronenstrahl-schmelzen.html">
                                    <div class="col-md-3 col-sm-6 item">
                                        <img src="/wp-content/themes/kreatize/img/img-elektronenstrahl-schmelzen.svg" alt="">
                                        <h3 class="cl-sans-16-22">Elektronenstrahl-Schmelzen</h3>
                                    </div>
                                </a>

                                <a class="process-link" href="/unsere-verfahren/schmelzschichtung.html">
                                    <div class="col-md-3 col-sm-6 item">
                                        <img src="/wp-content/themes/kreatize/img/img-schmelzschichtung.svg" alt="">
                                        <h3 class="cl-sans-16-22">Schmelzschichtung</h3>
                                    </div>
                                </a>

                                <!-- row 2 -->
                                <a class="process-link" href="/unsere-verfahren/multi-jet-modeling.html">
                                    <div class="col-md-3 col-sm-6 item">
                                        <img src="/wp-content/themes/kreatize/img/img-multi-jet-modeling.svg" alt="">
                                        <h3 class="cl-sans-16-22">Multi-Jet Modeling</h3>
                                    </div>
                                </a>

                                <a class="process-link" href="/unsere-verfahren/polyjet.html">
                                    <div class="col-md-3 col-sm-6 item">
                                        <img src="/wp-content/themes/kreatize/img/img-polyjet.svg" alt="">
                                        <h3 class="cl-sans-16-22">PolyJet</h3>
                                    </div>
                                </a>

                                <a class="process-link" href="/unsere-verfahren/selektives-lasersintern.html">
                                    <div class="col-md-3 col-sm-6 item">
                                        <img src="/wp-content/themes/kreatize/img/img-selektives-lasersintern.svg" alt="">
                                        <h3 class="cl-sans-16-22">Selektives Lasersintern</h3>
                                    </div>
                                </a>

                                <a class="process-link" href="/unsere-verfahren/stereolithografie.html">
                                    <div class="col-md-3 col-sm-6 item">
                                        <img src="/wp-content/themes/kreatize/img/img-stereolithografie.svg" alt="">
                                        <h3 class="cl-sans-16-22">Stereolithografie</h3>
                                    </div>
                                </a>
                            </div><!--End masonry-container  -->
                        </div><!--End panel-1  -->

                        <div role="tabpanel" class="tab-pane" id="panel-2">

                            <div class="row masonry-container">
                                <a class="process-link" href="/unsere-verfahren/cnc-frasen.html">
                                    <div class="col-md-3 col-sm-6 item cnc">
                                        <img src="/wp-content/themes/kreatize/img/img-cnc-frasen.svg" alt="">
                                        <h3 class="cl-sans-16-22">CNC Milling</h3>
                                    </div>
                                    <a>

                                        <a class="process-link" href="/unsere-verfahren/cnc-drehen.html">
                                            <div class="col-md-3 col-sm-6 item cnc">
                                                <img src="/wp-content/themes/kreatize/img/img-cnc-drehen.svg" alt="">
                                                <h3 class="cl-sans-16-22">CNC Turning</h3>
                                            </div>
                                            <a>

                                                <a class="process-link" href="/unsere-verfahren/cnc-schleifen.html">
                                                    <div class="col-md-3 col-sm-6 item cnc">
                                                        <img src="/wp-content/themes/kreatize/img/img-cnc-schleifen.svg" alt="">
                                                        <h3 class="cl-sans-16-22">CNC Grinding</h3>
                                                    </div>
                                                </a>
                            </div><!--End masonry-container  -->
                        </div><!--End panel-2  -->

                        <div role="tabpanel" class="tab-pane" id="panel-3">
                            <div class="row masonry-container">
                                <a class="process-link" href="/unsere-verfahren/guss.html">
                                    <div class="col-md-3 col-sm-6 item guss">
                                        <img src="/wp-content/themes/kreatize/img/img-guss.svg" alt="">
                                        <h3 class="cl-sans-16-22">Guss</h3>
                                    </div>
                                </a>
                            </div><!--End masonry-container  -->
                        </div><!--End panel-3  -->

                    </div><!--End tab-content  -->
                </div><!--End tabpanel  -->
            </div>
        </section>

        <section class="container partners">
            <h1 class="text-center cl-titil-40">KREATIZE® hilft seinen Fertigungspartnern zu mehr Erfolg.</h1>
            <div>
                <div class="col-md-4 partner-content">
                    <div class="item">
                        <div class="partner-img">
                            <img src="/wp-content/themes/kreatize/img/logo-tsf.png">
                        </div>
                        <div class="partner-texts ">
                            <span class="quote">&#8221;</span><p class="cl-sans-16-22">Wir sind als Innovationspartner überzeugt, dass KREATIZE® durch seine digitalen Lösungen den Zeitaufwand bei der Realisierung von Prototypen und Kleinserien auf Anbieter- und Kundenseite reduzieren wird und wir von der Digitalisierung gemeinsam profitieren werden.</p><span class="quote-ende">&#8220;</span>
                            <p class="author cl-sans-16-22">Dr.-Ing. Axel Stüber, Leiter Technik bei tsf</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 partner-content">
                    <div class="item">
                        <div class="partner-img">
                            <img src="/wp-content/themes/kreatize/img/logo-jomatik.png">
                        </div>
                        <div class="partner-texts mid">
                            <span class="quote">&#8221;</span><p class="cl-sans-16-22">Mit den digitalen Lösungen von KREATIZE® konnten wir innerhalb kürzester Zeit die Zahl unserer Aufträge erhöhen und diese über das Auftragsmanagement schnell und einfach bearbeiten..</p><span class="quote-ende">&#8220;</span>
                            <p class="author cl-sans-16-22">Johannes Matheis, CEO von Jomatik</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 partner-content">
                    <div class="item">
                        <div class="partner-img">
                            <img src="/wp-content/themes/kreatize/img/logo-rumpel.png">
                        </div>
                        <div class="partner-texts">
                            <span class="quote">&#8221;</span><p class="cl-sans-16-22">Da unser Unternehmen bei der momentanen rapiden Digitalisierung mithalten möchte, sehen wir KREATIZE®als enorm wichtigen strategischen Partner, der uns entscheidend dabei unterstützt, aus der Digitalisierung einen Mehrwert sowohl für unsere Kunden als auch für unsere Mitarbeiter zu gewinnen.</p><span class="quote-ende">&#8220;</span>
                            <p class="author cl-sans-16-22">Geschäftsführer Rumpel Präzisionstechnik</p>
                        </div>
                    </div>
                </div>
            </div>
            <!--end of partner testimony-->
            <!--<div class="handling col-md-12">
                <div class="col-md-3" style="">
                    <h2>45%</h3>
                    <hr style="width:40.5%;"/>
                    <p>weniger Emails und Telefonate</p>
                </div>

                <div class="col-md-3">
                    <h2>80%</h3>
                    <hr style="width:72%;"/>
                    <p>schneller Angebote erstellen</p>
                </div>

                <div class="col-md-3">
                    <h2>95%</h3>
                    <hr style="width:85.5%;"/>
                    <p>reduzierte Angebotsbearbeitungskosten</p>
                </div>

                <div class="col-md-3">
                    <h2>100%</h3>
                    <hr style="width:90%;"/>
                    <p>qualifizierte Anfragen</p>
                </div>
            </div>-->
            <div class="handling col-md-12">
                <div class="col-md-3 col-sm-6 col-xs-12 pad-r-l-0">
                    <span class="cl-titil-40">45%</span>
                    <div class="meter">
                        <span style="width: 45%;"></span>
                    </div>
                    <span class="cl-sans-16-22">weniger Emails und Telefonate</span>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12 pad-r-l-0">
                    <span class="cl-titil-40">80%</span>
                    <div class="meter">
                        <span style="width: 80%;"></span>
                    </div>
                    <span class="cl-sans-16-22">schneller Angebote erstellen</span>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12 pad-r-l-0">
                    <span class="cl-titil-40">95%</span>
                    <div class="meter">
                        <span style="width: 95%;"></span>
                    </div>
                    <span class="cl-sans-16-22">reduzierte Angebotsbearbeitungskosten</span>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12 pad-r-l-0   ">
                    <span class="cl-titil-40">100%</span>
                    <div class="meter">
                        <span style="width: 100%;"></span>
                    </div>
                    <span class="cl-sans-16-22">qualifizierte Anfragen</span>
                </div>
            </div>
            <div class="col-md-12 text-center" style="margin-top: 60px;">
                <a href="/supplier"  class="btn btn-primary text-uppercase btn-button"><strong>Anbieter werden</strong></a>
            </div>
        </section>

        <section class="customer-testimony">
            <div class="container">
                <div class="row">
                    <div class="col-md-2 hidden-xs hidden-sm" style="padding:0px;">
                        <img class="img-circle pull-right" src="/wp-content/themes/kreatize/img/profile_jan_roosen.jpg" alt="Jan Roosen"></img>
                    </div>
                    <div class="col-md-8 text-center">
                        <h1 class="text-center cl-titil-40" >Das sagen Kunden über uns.</h1>
                        <p class="cl-titil-20">»Mit der Hilfe von KREATIZE® konnten wir die Entwicklungszeit eines unserer<br/>Hero-Produkte deutlich reduzieren. Daher sehen wir KREATIZE® als einen starken<br/>strategischen Partner bei der Umsetzung von Prototypen.«</p>
                        <br/><img class="logo-customer hidden-xs" src="/wp-content/themes/kreatize/img/logo-horizn-studios.jpg"><p class="cl-sans-16-22">Jan Roosen, Co-Founder und Managing Partner bei Horizn Studios.</p>
                        <div class="col-md-2 visible-xs visible-sm text-center">
                            <img class="img-circle" src="/wp-content/themes/kreatize/img/profile_jan_roosen.jpg" alt="Jan Roosen">
                        </div>
                        <br class="hidden-sm hidden-xs"/>
                        <a href="/signup" class="btn btn-primary text-uppercase btn-button"><strong>Jetzt testen!</strong></a>
                    </div>
                    <div class="col-md-2 text-center">
                    </div>
                </div>
            </div>
        </section>
    </div>







    </section>
<?php get_sidebar(); ?>
<?php get_footer(); ?>