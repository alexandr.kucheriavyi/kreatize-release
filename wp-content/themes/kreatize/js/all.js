
function changeLanguage(language)
{
	history.pushState("", document.title, window.location.pathname + window.location.search);
	document.getElementById("locale").value = language;
	document.getElementById("language").submit();
}
function clickunsere(thisel)
{
			if (thisel.getAttribute('aria-controls')=="panel-1")
            {
                document.getElementById ("additive").click();   
            }
            else if (thisel.getAttribute('aria-controls')=="panel-2")
            {
                document.getElementById ("CNC").click();   
            }
            else if (thisel.getAttribute('aria-controls')=="panel-3")
            {
                document.getElementById ("Guss").click();   
            }        
}
document.getElementById("page-top").innerHTML = document.getElementById("page-top").innerHTML.replace(/®/ig, '<sup>®</sup>');


function changes()
{
    document.getElementById('namefile').innerHTML=document.getElementById('files').files[0].name;
}
function goToDI(url)
{
	var hash = url;
  	if (hash != "") {
		$('#tabs li').each(function() {
			$(this).removeClass('active');
		});
		$('#my-tab-content div').each(function() {
			$(this).removeClass('active');
		});
		var link = "";
		$('#tabs li').each(function() {
			link = $(this).find('a').attr('href');
			if (link == hash) {
				$(this).addClass('active');
				$(this).focus();
				var offset = $(this).offset();
				if(offset) {
					$('html,body').animate({
						scrollTop: $('.fadi-contents').offset().top - 114
					}, 500); 
				}
			}
		});
		$('#my-tab-content div').each(function() {
			link = $(this).attr('id');
			if ('#'+link == hash) {
				$(this).addClass('active');
			}
		});
  	}
}
var rapid=false;
		$(function() {
            $('#item1').hover(function(){ // hover in
                $('#profile-img').attr("src", $(this).attr('data-image-url'));
                $('#item1').addClass('item-active');
                $('#item2').removeClass('item-active');
                $('#item3').removeClass('item-active');
            });

            $('#item2').hover(function(){ // hover in
                $('#profile-img').attr("src", $(this).attr('data-image-url'));
                $('#item2').addClass('item-active');
                $('#item1').removeClass('item-active');
                $('#item3').removeClass('item-active');
            });
            $('#item3').hover(function(){ // hover in
                $('#profile-img').attr("src", $(this).attr('data-image-url'));
                $('#item3').addClass('item-active');
                $('#item1').removeClass('item-active');
                $('#item2').removeClass('item-active');
            });

			$('button.btn-roll').click(function() {
				var target = $('#intro');
				$('html, body').animate({
					scrollTop: target.offset().top
				}, 1000);
				return false;
			});
			$('a.rolling').click(function() {
				var target =$('#' + $(this).attr('aria-controls')) ;
				setTimeout(function(){
				    $('html, body').animate({
						scrollTop: target.offset().top-220
					}, 1000);
				}, 300);
			});	
			$('.uns-btn').click(function() {
				
				var unsElems = $(".unsere-panel");
			  	unsElems.each(function(){
			  		$(this).removeClass('active');
			  	});
			  	$(this).addClass('active');
			});	
			$('a.anchors').click(function() {
				var anchElems = $(".anchors");
			  	anchElems.each(function(){
			  		$(this).removeClass('active');
			  	});
			  	$(this).addClass('active');
			});	
			$('a.btn-roll').click(function() {
				var target = $('#intro');
				$('html, body').animate({
					scrollTop: target.offset().top-114
				}, 1000);
				return false;
			});
			$('.clear-click').click(function() {
				rapid=false;
			});
			$('#panel-14').click(function() {
				setTimeout(function(){
				    var rapElems = $(".rap-tab-1");

				  	var maxcH = 0;
				  	for (var i = 0; i <= rapElems.length - 1; i++) {
				  		$(rapElems[i]).height('auto');	
				  		if ($(rapElems[maxcH]).height() < $(rapElems[i + 1]).height()) {
				  			maxcH = i + 1;
				  		}
				  	}

				  	rapElems.each(function(){
				  		$(this).height($(rapElems[maxcH]).height());
				  	});
				}, 300);
			});
			$('#panel-34').click(function() {
				setTimeout(function(){
				    var h1Elems = $(".h1-div");
			        var maxh1H = 0;
			        for (var i = 0; i <= h1Elems.length - 1; i++) {
			        	$(h1Elems[i]).height("auto");
			            if ($(h1Elems[maxh1H]).height() < $(h1Elems[i + 1]).height()) {
			                maxh1H = i + 1;
			            }
			        }

			        h1Elems.each(function(){
			            $(this).height($(h1Elems[maxh1H]).height());
			        });


			        var pElems = $(".contetnt-p");
			        var maxpH = 0;
			        for (var i = 0; i <= pElems.length - 1; i++) {
			            if ($(pElems[maxpH]).height() < $(pElems[i + 1]).height()) {
			                maxpH = i + 1;
			            }
			        }

			        pElems.each(function(){
			            $(this).height($(pElems[maxpH]).height());
			        });
			        rapid=true;
				}, 300);
			});

			$(".video").click(function () {
				var theModal = $(this).data("target"),
				videoSRC = $(this).attr("data-video"),
				videoSRCauto = videoSRC + "?modestbranding=1&rel=0&controls=1&showinfo=0&html5=1&autoplay=1";
				$(theModal + ' iframe').attr('src', videoSRCauto);
				$(theModal + ' button.close').click(function () {
					$(theModal + ' iframe').attr('src', videoSRC);
				});
			});

			$('#play_video').on('hidden.bs.modal', function (e) {
				$('#play_video').find('iframe').attr('src', '');
			});

			$('.kr-plus-circle').click(function(){
				// $(".kr-plus-circle img").toggle();
				$(this).find('img').toggle();
			});

			if ($('#progress1').length <= 0) {
				return;
			}
			var bar = new ProgressBar.Circle('#progress1', {strokeWidth: 6, easing: 'easeInOut', duration: 1400,color: '#43ccad',
				trailColor: '#dbdcdc',trailWidth: 1,svgStyle: null,text: {autoStyleContainer: false},
				from: { color: '#43ccad', width: 6 },
				to: { color: '#43ccad', width: 6 },
				step: function(state, circle) {
					circle.path.setAttribute('stroke', state.color);
					circle.path.setAttribute('stroke-width', state.width);
					var value = Math.round(circle.value() * 100);
					if (value === 0) 
						circle.setText('');
					 else 
						circle.setText(value + '%');
				}
			});
			var bar2 = new ProgressBar.Circle('#progress2', {strokeWidth: 6, easing: 'easeInOut', duration: 1400,color: '#43ccad',
				trailColor: '#dbdcdc',trailWidth: 1,svgStyle: null,text: {autoStyleContainer: false},
				from: { color: '#43ccad', width: 6 },
				to: { color: '#43ccad', width: 6 },
				step: function(state, circle) {
					circle.path.setAttribute('stroke', state.color);
					circle.path.setAttribute('stroke-width', state.width);
					var value = Math.round(circle.value() * 100);
					if (value === 0) 
						circle.setText('');
					 else 
						circle.setText(value + '%');
				}
			});
			var bar3 = new ProgressBar.Circle('#progress3', {strokeWidth: 6, easing: 'easeInOut', duration: 1400,color: '#43ccad',
				trailColor: '#dbdcdc',trailWidth: 1,svgStyle: null,text: {autoStyleContainer: false},
				from: { color: '#43ccad', width: 6 },
				to: { color: '#43ccad', width: 6 },
				step: function(state, circle) {
					circle.path.setAttribute('stroke', state.color);
					circle.path.setAttribute('stroke-width', state.width);
					var value = Math.round(circle.value() * 100);
					if (value === 0) 
						circle.setText('');
					 else 
						circle.setText(value + '%');
				}
			});
			var bar4 = new ProgressBar.Circle('#progress4', {strokeWidth: 6, easing: 'easeInOut', duration: 1400,color: '#43ccad',
				trailColor: '#dbdcdc',trailWidth: 1,svgStyle: null,text: {autoStyleContainer: false},
				from: { color: '#43ccad', width: 6 },
				to: { color: '#43ccad', width: 6 },
				step: function(state, circle) {
					circle.path.setAttribute('stroke', state.color);
					circle.path.setAttribute('stroke-width', state.width);
					var value = Math.round(circle.value() * 100);
					if (value === 0) 
						circle.setText('');
					 else 
						circle.setText(value + '%');
				}
			});

            bar.animate($('#progress1').attr('data-persent'));  // range 0.0 to 1.0
            bar2.animate($('#progress2').attr('data-persent'));
            bar3.animate($('#progress3').attr('data-persent'));
            bar4.animate($('#progress4').attr('data-persent'));
		});
		
$(window).load(function() {
	$('.flexslider').flexslider({
	    		animation: "slide", prevText: "", nextText: "", slideshow: false, startAt:0
  			});
});



$(document).ready(function() {

			
			
			if (window.location.hash=="#panel-1")
            {
                document.getElementById ("additive").click();   
            }
            else if (window.location.hash=="#panel-2")
            {
                document.getElementById ("CNC").click();   
            }
            else if (window.location.hash=="#panel-3")
            {
                document.getElementById ("Guss").click();   
            }        

	var hash = window.location.hash;
  	if (hash != "") {
		$('#tabs li').each(function() {
			$(this).removeClass('active');
		});
		$('#my-tab-content div').each(function() {
			$(this).removeClass('active');
		});
		var link = "";
		$('#tabs li').each(function() {
			link = $(this).find('a').attr('href');
			if (link == hash) {
				$(this).addClass('active');
			}
		});
		$('#my-tab-content div').each(function() {
			link = $(this).attr('id');
			if ('#'+link == hash) {
				$(this).addClass('active');
				var offset = $(this).offset();
				if(offset) {
					$('html,body').animate({
						scrollTop: $('.fadi-contents').offset().top - 114
					}, 500);  
				}
			}
		});
  	}

    $("#intro-form").validate({
		focusInvalid :false,
		rules: {
			fname: 'required',
			lname: 'required',
			company: 'required',
			email: {
				required: true,
				email: true
			}
		},
		errorPlacement: function(error, element) {
		},
		submitHandler: function(form) {
		form.submit();
		}
  	});

	$("#newsletter-form").validate({
		focusInvalid :false,
		rules: {
			fname: 'required',
			lname: 'required',
			company: 'required',
			USERTYPE:'required',
			EMAILTYPE:'required',
			email: {
				required: true,
				email: true
			}
		},
		errorPlacement: function(error, element) {
		},
		submitHandler: function(form) {
		form.submit();
		}
  	});

	$("#contact-form").validate({
		focusInvalid :false,
		rules: {
			message: 'required',
			email: {
				required: true,
				email: true
			}
		},
		errorPlacement: function(error, element) {
		},
		submitHandler: function(form) {
		form.submit();
		}
  	});

  	/*SCRIPT FOR PLUS MINUS BTN IN SLIDER ON HOMEPAGE*/
  	var pElems = $(".slide-three .plus-step");

  	var maxH = 0;
  	for (var i = 0; i <= pElems.length - 1; i++) {
  		if ($(pElems[maxH]).height() < $(pElems[i + 1]).height()) {
  			maxH = i + 1;
  		}
  	}

  	pElems.each(function(){
  		$(this).height($(pElems[maxH]).height());
  	});
  	/*END OF SCRIPT*/

  	
  	//SCRIPT FOR SUPPLIER/SIGNUP CARD

  	//first changes
  		var CardsmaxH = $('.mid').height()-40; 
		if (CardsmaxH<$('.left').height()) CardsmaxH=$('.left').height();
		if (CardsmaxH<$('.right').height()) CardsmaxH=$('.right').height();
		//makes simple height for blocks
		$('.mid').height(CardsmaxH+40);
		$('.left').height(CardsmaxH);
		$('.right').height(CardsmaxH);
  	//end first	
  	/*SCRIPT FOR FOR BLOCKS WITH TESTIMONIALS ON HOMEPAGE*/
		var pElems = $(".partner-texts");

	  	var maxH = 0;
	  	for (var i = 0; i <= pElems.length - 1; i++) {
	  		$(pElems[maxH]).height('auto');
	  		if ($(pElems[maxH]).height() < $(pElems[i + 1]).height()) {
	  			maxH = i + 1;
	  		}
	  	}
	  	pElems.each(function(){
	  		$(this).height($(pElems[maxH]).height());
	  	});
	  	/*END OF SCRIPT*/

  	//innovation script

  		var cardsElems = $(".in-card-item");
	  	var maxcHcards = 0;
	  	for (var i = 0; i <= cardsElems.length - 1; i++) {
	  		$(cardsElems[i]).height('auto');
	  		if ($(cardsElems[maxcHcards]).height() < $(cardsElems[i + 1]).height()) {
	  			maxcHcards = i + 1;
	  		}
	  	}
	  	cardsElems.each(function(){
	  		$(this).height($(cardsElems[maxcHcards]).height());
	  	});


  	//end innovation
  	//rapid
  		var rapElems = $(".rap-tab-1");

	  	var maxcH = 0;
	  	for (var i = 0; i <= rapElems.length - 1; i++) {
	  		if ($(rapElems[maxcH]).height() < $(rapElems[i + 1]).height()) {
	  			maxcH = i + 1;
	  		}
	  	}

	  	rapElems.each(function(){
	  		$(this).height($(rapElems[maxcH]).height());
	  	});

	  	/*var h1Elems = $(".title-h1");
	  	var maxh1H = 0;
	  	for (var i = 0; i <= h1Elems.length - 1; i++) {
	  		if ($(h1Elems[maxh1H]).height() < $(h1Elems[i + 1]).height()) {
	  			maxh1H = i + 1;
	  		}
	  	}

	  	h1Elems.each(function(){
	  		$(this).height($(h1Elems[maxh1H]).height());
	  	});


	  	var pElems = $(".contetnt-p");
	  	var maxpH = 0;
	  	for (var i = 0; i <= pElems.length - 1; i++) {
	  		if ($(pElems[maxpH]).height() < $(pElems[i + 1]).height()) {
	  			maxpH = i + 1;
	  		}
	  	}

	  	pElems.each(function(){
	  		$(this).height($(pElems[maxpH]).height());
	  	});*/

  	//end rapid

  	//career text-box
  		var tbElems = $(".text-box");

	  	var maxtbH1 = 0;
	  	for (var i = 0; i <= tbElems.length - 1; i++) {
	  		$(tbElems[i]).height('auto');
	  		if ($(tbElems[maxtbH1]).height() < $(tbElems[i + 1]).height()) {
	  			maxtbH1 = i + 1;
	  		}
	  	}

	  	tbElems.each(function(){
	  		$(this).height($(tbElems[maxtbH1]).height());
	  	});
	  	//end career
	  	var btWidth=$("#bots-top").width();
	  	$('#bots-bottom').css({
		    'border-left-width': btWidth/2,
		    'border-right-width': btWidth/2
		});
	  	
  	
  	$(window).resize(function() {
  		//reset blocks height for adaptive
  		$('.mid').height('auto'); 
		$('.left').height('auto'); 
		$('.right').height('auto') 

    	var CardsmaxH = $('.mid').height()-40; 
		if (CardsmaxH<$('.left').height()) CardsmaxH=$('.left').height();
		if (CardsmaxH<$('.right').height()) CardsmaxH=$('.right').height();
		//makes simple height for blocks
		$('.mid').height(CardsmaxH+40);
		$('.left').height(CardsmaxH);
		$('.right').height(CardsmaxH);
		//end


		/*SCRIPT FOR FOR BLOCKS WITH TESTIMONIALS ON HOMEPAGE*/
		var pElems = $(".partner-texts");

	  	var maxH = 0;
	  	for (var i = 0; i <= pElems.length - 1; i++) {
	  		$(pElems[i]).height('auto');
	  		if ($(pElems[maxH]).height() < $(pElems[i + 1]).height()) {
	  			maxH = i + 1;
	  		}
	  	}

	  	pElems.each(function(){
	  		$(this).height($(pElems[maxH]).height());
	  	});
	  	/*END OF SCRIPT*/


	  	//innovation script

  		var cardsElems = $(".in-card-item");
	  	var maxcHcards = 0;
	  	cardsElems.each(function(){
	  		$(this).height('auto');
	  	});
	  	for (var i = 0; i < cardsElems.length - 1; i++) {
	  		if ($(cardsElems[maxcHcards]).height() < $(cardsElems[i + 1]).height()) {
	  			maxcHcards = i + 1;
	  		}
	  	}
	  	cardsElems.each(function(){
	  		$(this).height($(cardsElems[maxcHcards]).height());
	  	});
  	//end innovation

  		//rapid
  		var rapElems = $(".rap-tab-1");

	  	var maxcH1 = 0;
	  	for (var i = 0; i <= rapElems.length - 1; i++) {
	  		$(rapElems[i]).height('auto');
	  		if ($(rapElems[maxcH1]).height() < $(rapElems[i + 1]).height()) {
	  			maxcH1 = i + 1;
	  		}
	  	}

	  	rapElems.each(function(){
	  		$(this).height($(rapElems[maxcH1]).height());
	  	});
	  	if (rapid)
	  	{
	  		var h1Elems = $(".h1-div");
	  		h1Elems.each(function(){
		  		$(this).height('auto');
		  	});
		  	var maxh1H = 0;
		  	for (var i = 0; i <= h1Elems.length - 1; i++) {
		  		$(h1Elems[i]).height('auto');
		  		console.log($(h1Elems[i]).height());
		  		if ($(h1Elems[maxh1H]).height() < $(h1Elems[i + 1]).height()) {
		  			maxh1H = i + 1;
		  		}
		  	}

		  	h1Elems.each(function(){
		  		$(this).height($(h1Elems[maxh1H]).height());
		  	});


		  	var pElems = $(".contetnt-p");
		  	pElems.each(function(){
		  		$(this).height('auto');
		  	});	
		  	var maxpH = 0;
		  	for (var i = 0; i <= pElems.length - 1; i++) {
		  		$(pElems[i]).height('auto');
		  		if ($(pElems[maxpH]).height() < $(pElems[i + 1]).height()) {
		  			maxpH = i + 1;
		  		}
		  	}

		  	pElems.each(function(){
		  		$(this).height($(pElems[maxpH]).height());
		  	});	
	  	}
	  	
	  	$("#supplier-3-block").height($("#supl-items").height());

  	//end rapid

  		//career text-box
  		var tbElems = $(".text-box");

	  	var maxtbH1 = 0;
	  	for (var i = 0; i <= tbElems.length - 1; i++) {
	  		$(tbElems[i]).height('auto');
	  		if ($(tbElems[maxtbH1]).height() < $(tbElems[i + 1]).height()) {
	  			maxtbH1 = i + 1;
	  		}
	  	}

	  	tbElems.each(function(){
	  		$(this).height($(tbElems[maxtbH1]).height());
	  	});
	  	//end career
	  	var btWidth=$("#bots-top").width();
	  	$('#bots-bottom').css({
		    'border-left-width': btWidth/2,
		    'border-right-width': btWidth/2
		});
		var pElems = $(".slide-three .plus-step");

	  	var maxH = 0;
	  	for (var i = 0; i <= pElems.length - 1; i++) {
	  		$(pElems[maxH]).height('auto');
	  		if ($(pElems[maxH]).height() < $(pElems[i + 1]).height()) {
	  			maxH = i + 1;
	  		}
	  	}
	  	pElems.each(function(){
	  		$(this).height($(pElems[maxH]).height());
	  	});

	});
  	$("#supplier-3-block").height($("#supl-items").height());
	

  	//end
});
//# sourceMappingURL=all.js.map
$(window).resize(function(){

	$('.aboutus-child').css({
		position:'absolute',
		top: ($('.aboutus').height() - $('.aboutus-child').outerHeight())/2
	});

			var itemElems = $(".item h3");
		  	var maxitem = 0;
		  	for (var i = 0; i <= itemElems.length - 1; i++) {
		  		$(itemElems[i]).height('auto');
		  		console.log($(itemElems[i]).height());
		  		if ($(itemElems[maxitem]).height() < $(itemElems[i + 1]).height()) {
		  			maxitem = i + 1;
		  		}
		  	}

		  	itemElems.each(function(){
		  		$(this).height($(itemElems[maxitem]).height());
		  	});
});

// Для запуска функции:
$(window).resize();


