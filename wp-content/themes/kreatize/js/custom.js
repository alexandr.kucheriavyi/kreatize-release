function changeLanguage(language)
{
	history.pushState("", document.title, window.location.pathname + window.location.search);
	document.getElementById("locale").value = language;
	document.getElementById("language").submit();
}

function goToDI(url)
{
	var hash = url;
  	if (hash != "") {
		$('#tabs li').each(function() {
			$(this).removeClass('active');
		});
		$('#my-tab-content div').each(function() {
			$(this).removeClass('active');
		});
		var link = "";
		$('#tabs li').each(function() {
			link = $(this).find('a').attr('href');
			if (link == hash) {
				$(this).addClass('active');
				$(this).focus();
				var offset = $(this).offset();
				if(offset) {
					$('html,body').animate({
						scrollTop: $('.fadi-contents').offset().top - 114
					}, 500); 
				}
			}
		});
		$('#my-tab-content div').each(function() {
			link = $(this).attr('id');
			if ('#'+link == hash) {
				$(this).addClass('active');
			}
		});
  	}
}

		$(function() {
			$('#item1').hover(function(){ // hover in
				$('#profile-img').attr("src", "/img/supplier_screen_01.jpg");
				$('#item1').addClass('item-active');
				$('#item2').removeClass('item-active');
				$('#item3').removeClass('item-active');
			});

			$('#item2').hover(function(){ // hover in
				$('#profile-img').attr("src", "/img/supplier_screen_02.jpg");
				$('#item2').addClass('item-active');
				$('#item1').removeClass('item-active');
				$('#item3').removeClass('item-active');
			});
			$('#item3').hover(function(){ // hover in
				$('#profile-img').attr("src", "/img/supplier_screen_03.jpg");
				$('#item3').addClass('item-active');
				$('#item1').removeClass('item-active');
				$('#item2').removeClass('item-active');
			});

			$('button.btn-roll').click(function() {
				var target = $('#intro');
				$('html, body').animate({
					scrollTop: target.offset().top-114
				}, 1000);
				return false;
			});

			$('a.btn-roll').click(function() {
				var target = $('#intro');
				$('html, body').animate({
					scrollTop: target.offset().top-114
				}, 1000);
				return false;
			});

			$(".video").click(function () {
				var theModal = $(this).data("target"),
				videoSRC = $(this).attr("data-video"),
				videoSRCauto = videoSRC + "?modestbranding=1&rel=0&controls=1&showinfo=0&html5=1&autoplay=1";
				$(theModal + ' iframe').attr('src', videoSRCauto);
				$(theModal + ' button.close').click(function () {
					$(theModal + ' iframe').attr('src', videoSRC);
				});
			});

			$('#play_video').on('hidden.bs.modal', function (e) {
				$('#play_video').find('iframe').attr('src', '');
			});

			$('.kr-plus-circle').click(function(){
				// $(".kr-plus-circle img").toggle();
				$(this).find('img').toggle();
			});

			if ($('#progress1').length <= 0) {
				return;
			}
			var bar = new ProgressBar.Circle('#progress1', {strokeWidth: 6, easing: 'easeInOut', duration: 1400,color: '#43ccad',
				trailColor: '#dbdcdc',trailWidth: 1,svgStyle: null,text: {autoStyleContainer: false},
				from: { color: '#43ccad', width: 6 },
				to: { color: '#43ccad', width: 6 },
				step: function(state, circle) {
					circle.path.setAttribute('stroke', state.color);
					circle.path.setAttribute('stroke-width', state.width);
					var value = Math.round(circle.value() * 100);
					if (value === 0) 
						circle.setText('');
					 else 
						circle.setText(value + '%');
				}
			});
			var bar2 = new ProgressBar.Circle('#progress2', {strokeWidth: 6, easing: 'easeInOut', duration: 1400,color: '#43ccad',
				trailColor: '#dbdcdc',trailWidth: 1,svgStyle: null,text: {autoStyleContainer: false},
				from: { color: '#43ccad', width: 6 },
				to: { color: '#43ccad', width: 6 },
				step: function(state, circle) {
					circle.path.setAttribute('stroke', state.color);
					circle.path.setAttribute('stroke-width', state.width);
					var value = Math.round(circle.value() * 100);
					if (value === 0) 
						circle.setText('');
					 else 
						circle.setText(value + '%');
				}
			});
			var bar3 = new ProgressBar.Circle('#progress3', {strokeWidth: 6, easing: 'easeInOut', duration: 1400,color: '#43ccad',
				trailColor: '#dbdcdc',trailWidth: 1,svgStyle: null,text: {autoStyleContainer: false},
				from: { color: '#43ccad', width: 6 },
				to: { color: '#43ccad', width: 6 },
				step: function(state, circle) {
					circle.path.setAttribute('stroke', state.color);
					circle.path.setAttribute('stroke-width', state.width);
					var value = Math.round(circle.value() * 100);
					if (value === 0) 
						circle.setText('');
					 else 
						circle.setText(value + '%');
				}
			});
			var bar4 = new ProgressBar.Circle('#progress4', {strokeWidth: 6, easing: 'easeInOut', duration: 1400,color: '#43ccad',
				trailColor: '#dbdcdc',trailWidth: 1,svgStyle: null,text: {autoStyleContainer: false},
				from: { color: '#43ccad', width: 6 },
				to: { color: '#43ccad', width: 6 },
				step: function(state, circle) {
					circle.path.setAttribute('stroke', state.color);
					circle.path.setAttribute('stroke-width', state.width);
					var value = Math.round(circle.value() * 100);
					if (value === 0) 
						circle.setText('');
					 else 
						circle.setText(value + '%');
				}
			});

			bar.animate(0.45);  // range 0.0 to 1.0
			bar2.animate(0.80);
			bar3.animate(0.95);
			bar4.animate(1.0);
		});
		
$(window).load(function() {
	$('.flexslider').flexslider({
	    		animation: "slide", prevText: "", nextText: "", slideshow: false, startAt:0
  			});
});

$(document).ready(function() {

	var hash = window.location.hash;
  	if (hash != "") {
		$('#tabs li').each(function() {
			$(this).removeClass('active');
		});
		$('#my-tab-content div').each(function() {
			$(this).removeClass('active');
		});
		var link = "";
		$('#tabs li').each(function() {
			link = $(this).find('a').attr('href');
			if (link == hash) {
				$(this).addClass('active');
			}
		});
		$('#my-tab-content div').each(function() {
			link = $(this).attr('id');
			if ('#'+link == hash) {
				$(this).addClass('active');
				var offset = $(this).offset();
				if(offset) {
					$('html,body').animate({
						scrollTop: $('.fadi-contents').offset().top - 114
					}, 500);  
				}
			}
		});
  	}

    $("#intro-form").validate({
		focusInvalid :false,
		rules: {
			fname: 'required',
			lname: 'required',
			company: 'required',
			email: {
				required: true,
				email: true
			}
		},
		errorPlacement: function(error, element) {
		},
		submitHandler: function(form) {
		form.submit();
		}
  	});

	$("#newsletter-form").validate({
		focusInvalid :false,
		rules: {
			fname: 'required',
			lname: 'required',
			company: 'required',
			USERTYPE:'required',
			EMAILTYPE:'required',
			email: {
				required: true,
				email: true
			}
		},
		errorPlacement: function(error, element) {
		},
		submitHandler: function(form) {
		form.submit();
		}
  	});

	$("#contact-form").validate({
		focusInvalid :false,
		rules: {
			message: 'required',
			email: {
				required: true,
				email: true
			}
		},
		errorPlacement: function(error, element) {
		},
		submitHandler: function(form) {
		form.submit();
		}
  	});
});