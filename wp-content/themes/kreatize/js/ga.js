(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-90733995-1', 'auto');
ga('send', 'pageview');

$(".playbutton").click(function(){
    ga('send', {
       hitType: 'event',
        eventCategory: 'Video',
        eventAction: 'View video on homepage',
        eventLabel: 'Fall Campaign'
    });
});

$(".index-jetzt").click(function(){
    ga('send', {
       hitType: 'event',
        eventCategory: 'CTA_Jetzttesten',
        eventAction: 'Click',
        eventLabel: 'Fall Campaign'
    });
});

var form = document.getElementById('footer-newsletter');
form.addEventListener('submit', function(event) {

    event.preventDefault();

    ga('send', 'event', 'Newsletter Sign-up', 'submit', {
        hitCallback: function() {
            form.submit();
        }
    });
});

var supForm = document.getElementById('intro-form');
supForm.addEventListener('submit', function(event) {
    event.preventDefault();

    ga('send', 'event', 'Supplier sign-up', 'submit', {
        hitCallback: function() {
            supForm.submit();
        }
    });
});