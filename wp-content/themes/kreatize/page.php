<?php get_header(); ?>
    <div id="content" >
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <h1 style="text-align: center" class="cl-titil-40"><?php the_title(); ?></h1>
        <section class="container-fluid">
            <div class="container" >

                    <article id="post-<?php the_ID(); ?>" <?php post_class('cl-sans-16-22'); ?>>

                                <?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?>
                                <?php the_content(); ?>

                    </article>
                    <?php if ( ! post_password_required() ) comments_template( '', true ); ?>

            </div>
        </section>
        <?php endwhile; endif; ?>
    </div>
<?php get_footer(); ?>