<?php
require_once 'core/App.php';
use kreatize\App as App;
$kreatize = App::getInstance();
?>

<?php get_header(); ?>

    <div id="content" class="process">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <section class="container process-details">
            <div>
                <div class="col-md-5 text-center">
                    <?php the_post_thumbnail( 'full' ); ?>
                </div>
                <div class="col-md-7">
                    <?php $kreatize->service('breadcrumbs')->processPage();?>
                    <h2 class="cl-titil-40"><?php the_title(); ?></h2><?php //edit_post_link(); ?>
                    <?php the_content(); ?>
                </div>
            </div>
        </section>
<?php endwhile; endif; ?>


<?php
$query = $kreatize->service('processesPostType')->getNewQueryPostsByTag(get_the_ID());
?>

        <?php if ($query->have_posts()) {?>
            <section class="manu-process">
                <div class="container">
                    <!-- Tab panels -->
                    <div class="tab-content">
                        <!--start of tab 1-->
                        <div role="tabpanel" class="tab-pane active" id="panel-1">
                            <div class="row masonry-container">

                                <?php while ($query->have_posts() ) { $query->the_post(); ?>
                                    <a class="process-link" href="<?php echo get_permalink();?>">
                                        <div class="col-md-3 col-sm-6 item">
                                            <?php the_post_thumbnail( 'full' ); ?>
                                            <h3 class="cl-sans-16-22"><?php the_title(); ?></h3>
                                        </div>
                                    </a>
                                <?php } ?>
                            </div><!--End masonry-container  -->
                        </div><!--End panel-1  -->
                    </div><!--End tab-content  -->
                </div><!--End tabpanel  -->
            </section>

        <?php }?>

<?php wp_reset_postdata();?>




    </div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>