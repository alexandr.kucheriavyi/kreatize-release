<?php /* Template Name: About us Page */ ?>
<?php if (!defined('ABSPATH')) {exit;} ?>

<?php
require_once get_stylesheet_directory().'/core/App.php';
use kreatize\App as App;
$kreatize = App::getInstance();
$pageOptions =  $kreatize->service('AboutUsPageOptions');
?>

<?php get_header(); ?>
    <section id="first-block" style="background: linear-gradient(to right,rgba(100, 198, 225, 0.5), rgba(113, 210, 189, 0.8)),  url(<?php echo $pageOptions->headerImage(); ?>) no-repeat center center;background-size: cover;">
        <div class="first-block aboutus">
            <!--<img src="/wp-content/themes/kreatize/img/firstBlock.jpg" alt="">-->
            <div class="col-md-12">
                <div class="child-first-block col-md-8 col-md-offset-2 aboutus-child" style="display: block;">
                    <h1 class="cl-titil-65"><?php echo $pageOptions->headerQuote(); ?></h1>
                    <h5 class="cl-sans-16-22"><?php echo $pageOptions->headerQuoteAuthor(); ?></h5>
                </div>
            </div>
        </div>
    </section>

    <section id="aboutus" >
        <div class="container">
            <div class="row">
                <div class="col-md-12  text-center">
                    <h2 class="cl-titil-40"><?php echo $pageOptions->ourStoryTitle(); ?></h2>
                    <p class="cl-titil-20">
                        <strong>
                                <?php echo $pageOptions->ourStoryTextBlock(1); ?>
                            </strong>
                    </p>



                    <p style="margin-bottom: 100px;" class="cl-sans-16-22">
                        <?php echo $pageOptions->ourStoryTextBlock(2); ?>
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section id="img-about" style="padding-top: 0px;">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="col-md-4 col-sm-4 img-about" >
                        <img class="img " src="<?php echo $pageOptions->ourStoryTextPhoto(1); ?>" alt="about_01.jpg"></img>
                    </div>
                    <div class="col-md-4 col-sm-4 img-about" >
                        <img class="img " src="<?php echo $pageOptions->ourStoryTextPhoto(2); ?>" alt="about_01.jpg"></img>
                    </div>
                    <div class="col-md-4 col-sm-4 img-about" >
                        <img class="img " src="<?php echo $pageOptions->ourStoryTextPhoto(3); ?>" alt="about_01.jpg"></img>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Services Section -->
    <section id="services">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center wer-wir">
                    <h2 class="cl-titil-40"><?php echo $pageOptions->whoWeAreTitle(); ?></h2>
                    <p class="cl-titil-20"><?php echo $pageOptions->whoWeAreText(); ?></p>
                </div>
            </div>
            <div class="row text-center teams-people">
                <?php $employees = $pageOptions->getEmployeesData(); ?>

                <?php foreach ($employees as $employee) {?>

                <div class="col-md-2">
                    <img src="<?php echo $employee['photo']; ?>" class="img-circle" alt="">
                    <h5 class="cl-titil-16-22"><b><?php echo $employee['name']; ?></b></h5>
                    <p class="cl-sans-16-22"><?php echo $employee['position']; ?></p>
                    <a class="fb-btn-team" href="<?php echo $employee['linkedin_url']; ?>" target="_blank" style="font-size:21px;">
                        <i class="fa fa-linkedin-square" aria-hidden="true"></i>
                    </a>

                </div>

                <?php }?>

                <div class="col-md-2">
                    <img src="<?php echo $pageOptions->whoWeAreDefaultPhoto(); ?>" class="img-circle" alt=""><br>
                    <button onclick="window.location.href = '<?php echo $pageOptions->whoWeAreApplyUrl(); ?>';" class="btn btn-primary text-uppercase btn-roll"><strong><?php echo $pageOptions->whoWeAreButtonText(); ?></strong></button>
                </div></div>
        </div>
    </section>




    <section id="aboutus">
        <div class="container">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8 text-center" >
                    <p class="cl-titil-28">
                        <?php echo $pageOptions->footerQuote(); ?>
                    </p>
                    <h5 style="margin: -20px 100px 90px 0; text-align: right; " class="cl-sans-16-22"><?php echo $pageOptions->footerQuoteAuthor(); ?></h5>

                </div>
            </div>
        </div>
    </section>
<?php get_sidebar(); ?>
<?php get_footer(); ?>