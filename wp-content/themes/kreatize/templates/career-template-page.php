<?php /* Template Name: Career Page */ ?>
<?php if (!defined('ABSPATH')) {exit;} ?>

<?php
require_once get_stylesheet_directory().'/core/App.php';
use kreatize\App as App;
$kreatize = App::getInstance();
$pageOptions =  $kreatize->service('CareerPageOptions');
?>

<?php get_header(); ?>
    <section id="first-block" style="background: linear-gradient(to right,rgba(100, 198, 225, 0.5), rgba(113, 210, 189, 0.8)),  url(<?php echo $pageOptions->headerImage(); ?>) no-repeat center center;background-size: cover;">
        <div class="first-block">
            <!--<img src="/wp-content/themes/kreatize/img/firstBlock.jpg" alt="">-->
            <div class="col-md-12">
                <div class="child-first-block col-md-6 col-md-offset-3">
                    <h1 class="cl-titil-65"><?php echo $pageOptions->headerTitle(); ?> </h1>
                </div>
            </div>
        </div>
    </section>






    <section id="portfolio" class="bg-light-gray">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center child-pf" style="margin-left: -15px;">
                    <div class="text-center heading cl-titil-28"><?php echo $pageOptions->vacanciesTitle(); ?></div>
                    <p class="section-subheading text-teams-p cl-sans-16-22"> <?php echo $pageOptions->vacanciesText(); ?>
                    </p>
                </div>
            </div>
            <?php $vacancies = $pageOptions->getVacanciesData(); ?>

            <?php
                $counter = 1;

                foreach ($vacancies as $vacancy) {
            ?>
            <?php if ($counter == 1 || ($counter/4) == 1) {?>
                <div class="row">
                <?php }?>

                    <div class="col-md-3 col-sm-12 text-box" lang="en">

                        <div class="portfolio-caption" >
                            <div class="text-center heading cl-titil-28">
                                <?php echo $vacancy['title'];?>
                                <?php if ($vacancy['special_information']) {?>
                                    <br>
                                    <span class="cl-sans-20">(<?php echo $vacancy['special_information']; ?>)</span>
                            <?php }?>

                            </div>
                            <p><?php echo $vacancy['description'];?>
                                <a href="<?php echo $vacancy['file'];?>" target="_blank" class="dl-career-arrow"><img src="/wp-content/themes/kreatize/img/arrow_career.png"></a>
                            </p>

                        </div>
                    </div>

                <?php if (($counter/3) == 1) {?>
                    </div>
                <?php }?>
            <?php
                $counter++;
                }
            ?>
            <div class="col-md-12 text-center" style="padding-top: 76px;">
                <a href="<?php echo $pageOptions->vacanciesApplyButtonUrl(); ?>" class="btn btn-primary navbar-btn text-uppercase btnUpload btn-button"><strong><?php echo $pageOptions->vacanciesButtonText(); ?></strong></a>
            </div>
        </div>
    </section>

    <section id="net-icons">
        <div class="container">
            <div class="row">
                <div class="col-md-12  ">
                    <div class="heading cl-titil-28 text-center" style="color: #fff; "><?php echo $pageOptions->socialFooterTitle(); ?></div>
                    <div class="col-md-12 career">
                        <div class="col-md-3"><a href="<?php echo $pageOptions->socialFooterFacebookUrl(); ?>" class="fa fa-facebook" title="" target="_blank"></a></div>
                        <div class="col-md-3"><a href="<?php echo $pageOptions->socialFooterLinkedinUrl(); ?>" class="fa fa-linkedin" title="" target="_blank"></a></div>
                        <div class="col-md-3"><a href="<?php echo $pageOptions->socialFooterTwitterUrl(); ?>" class="fa fa-tumblr" title="" target="_blank"></a></div>
                        <div class="col-md-3"><a href="<?php echo $pageOptions->socialFooterYouTubeUrl(); ?>" class="fa fa-youtube" title="" target="_blank"></a></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php get_sidebar(); ?>
<?php get_footer(); ?>