<?php /* Template Name: Apply Now Page */ ?>
<?php if (!defined('ABSPATH')) {exit;} ?>

<?php
require_once get_stylesheet_directory().'/core/App.php';
use kreatize\App as App;
$kreatize = App::getInstance();
$pageOptions =  $kreatize->service('ContactRPageOptions');
$wpGlobus = $kreatize->service('WPGlobus');
?>

<?php get_header(); ?>
    <div id="content" class="contact">
        <section class="container-fluid">
            <div class="container">
                <div class="text-area">
                    <h1 class="cl-titil-40"><?php echo $pageOptions->title(); ?></h1>
                    <div class="detailed-text col-md-12 text-center">
                        <p class="cl-titil-20"><?php echo $pageOptions->text(1); ?></p>
                        <p class="cl-titil-20"><?php echo $pageOptions->text(2); ?></p>

                    <?php
                    if ($wpGlobus->isEnglish()) {
                        echo do_shortcode('[contact-form-7 id="181" title="Apply Now Page form English" html_class="form intro-form"]');
                    } else {
                        echo do_shortcode('[contact-form-7 id="183" title="Contact-r Page form Deutsch"]');
                    }
                    ?>
                </div>
            </div>
    </div>
    </section>
    </div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>