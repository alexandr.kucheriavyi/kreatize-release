<?php /* Template Name: Faq Page */ ?>
<?php if (!defined('ABSPATH')) {exit;} ?>

<?php
require_once get_stylesheet_directory().'/core/App.php';
use kreatize\App as App;
$kreatize = App::getInstance();
$pageOptions =  $kreatize->service('FaqPageOptions');
$breadcrumbs =  $kreatize->service('Breadcrumbs');
?>

<?php get_header(); ?>
    <div id="content" class="faq">
        <section class="intro" id="intro" style="
    background: linear-gradient(to right, rgba(57, 160, 193, 0.8), rgba(67, 204, 173, 0.8)), url(<?php echo $pageOptions->headerImage(); ?>) no-repeat center center;
    background-size: cover;
">
            <div class="container text-center">
                <h1 class="cl-titil-65"><?php echo $pageOptions->headerTitle(); ?></h1>
            </div>
        </section>

        <section style="padding-top: 0px;">
            <div class="container fadi-contents">
                <?php echo $breadcrumbs->page();?>
                <div role="tabpanel" class="">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs col-md-3 cl-titil-20"  role="tablist" style="padding:0 5px;">
                        <li role="presentation" class="active">
                            <a href="#panel-1" aria-controls="panel-1" role="tab" data-toggle="tab"><?php echo $pageOptions->tabName(1); ?></a>
                        </li>
                        <li role="presentation">
                            <a href="#panel-2" aria-controls="panel-2" role="tab" data-toggle="tab"><?php echo $pageOptions->tabName(2); ?></a>
                        </li>
                        <li role="presentation" class="guss">
                            <a href="#panel-3" aria-controls="panel-3" role="tab" data-toggle="tab"><?php echo $pageOptions->tabName(3); ?></a>
                        </li>
                        <li role="presentation" class="guss">
                            <a href="#panel-4" aria-controls="panel-4" role="tab" data-toggle="tab"><?php echo $pageOptions->tabName(4); ?></a>
                        </li>
                    </ul>
                    <!-- Tab panels -->
                    <div class="tab-content col-md-8 margin--10">
                        <div role="tabpanel" class="tab-pane active" id="panel-1">
                            <div class="tab-content-right">
                                <h1 class="cl-titil-40"><?php echo $pageOptions->tabTitle(1); ?></h1>
                                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                    <?php $items = $pageOptions->tabItems(1); ?>
                                    <?php
                                    if ($items)  {
                                        $isFirst = true;
                                        foreach ($items as $ident => $item)  {
                                            $opened = ($isFirst) ? 'in' : '';
                                            $isFirst = false;
                                    ?>

                                    <div class="panel panel-default">
                                        <div class="panel-heading container" role="tab" id="headingOne<?php echo $ident; ?>">
                                            <h4 class="panel-title cl-titil-16-22 question-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne<?php echo $ident; ?>" aria-expanded="true" aria-controls="collapseOne<?php echo $ident; ?>">
                                                    <?php echo $item['title']; ?>
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseOne<?php echo $ident; ?>" class="panel-collapse collapse <?php echo $opened; ?>" role="tabpanel" aria-labelledby="headingOne<?php echo $ident; ?>">
                                            <div class="panel-body container">
                                                <p><?php echo $item['text']; ?></p>
                                            </div>
                                        </div>
                                    </div>

                                    <?php
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                        </div><!--End panel-1  -->

                        <div role="tabpanel" class="tab-pane" id="panel-2">
                            <div class="tab-content-right">
                                <h1 class="cl-titil-40"><?php echo $pageOptions->tabTitle(2); ?></h1>
                                <div class="panel-group" id="c-accordion" role="tablist" aria-multiselectable="true">
                                    <?php $items = $pageOptions->tabItems(2); ?>
                                    <?php
                                    if ($items)  {
                                        $isFirst = true;
                                        foreach ($items as $ident => $item)  {
                                            $opened = ($isFirst) ? 'in' : '';
                                            $isFirst = false;
                                            ?>

                                            <div class="panel panel-default">
                                                <div class="panel-heading container" role="tab" id="c-headingOne<?php echo $ident; ?>">
                                                    <h4 class="panel-title cl-titil-16-22 question-title">
                                                        <a role="button" data-toggle="collapse" data-parent="#c-accordion" href="#c-collapseOne<?php echo $ident; ?>" aria-expanded="true" aria-controls="c-collapseOne<?php echo $ident; ?>">
                                                            <?php echo $item['title']; ?>
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="c-collapseOne<?php echo $ident; ?>" class="panel-collapse collapse <?php echo $opened; ?>" role="tabpanel" aria-labelledby="c-headingOne<?php echo $ident; ?>">
                                                    <div class="panel-body container">
                                                        <p><?php echo $item['text']; ?></p>
                                                    </div>
                                                </div>
                                            </div>

                                            <?php
                                        }
                                    }
                                    ?>

                                </div>
                            </div>
                        </div>


                        <div role="tabpanel" class="tab-pane" id="panel-3">
                            <div class="tab-content-right">
                                <h1 class="cl-titil-40"><?php echo $pageOptions->tabTitle(3); ?></h1>
                                <div class="panel-group" id="s-accordion" role="tablist" aria-multiselectable="true">
                                    <?php $items = $pageOptions->tabItems(3); ?>
                                    <?php
                                    if ($items)  {
                                        $isFirst = true;
                                        foreach ($items as $ident => $item)  {
                                            $opened = ($isFirst) ? 'in' : '';
                                            $isFirst = false;
                                            ?>

                                            <div class="panel panel-default">
                                                <div class="panel-heading container" role="tab" id="s-headingOne<?php echo $ident; ?>">
                                                    <h4 class="panel-title cl-titil-16-22 question-title">
                                                        <a role="button" data-toggle="collapse" data-parent="#s-accordion" href="#s-collapseOne<?php echo $ident; ?>" aria-expanded="true" aria-controls="s-collapseOne<?php echo $ident; ?>">
                                                            <?php echo $item['title']; ?>
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="s-collapseOne<?php echo $ident; ?>" class="panel-collapse collapse <?php echo $opened; ?>" role="tabpanel" aria-labelledby="s-headingOne<?php echo $ident; ?>">
                                                    <div class="panel-body container">
                                                        <p><?php echo $item['text']; ?></p>
                                                    </div>
                                                </div>
                                            </div>

                                            <?php
                                        }
                                    }
                                    ?>

                                </div>
                            </div><!--End panel-3  -->
                        </div>

                        <div role="tabpanel" class="tab-pane" id="panel-4">
                            <div class="tab-content-right">
                                <h1 class="cl-titil-40"><?php echo $pageOptions->tabTitle(4); ?></h1>
                                <p><?php echo $pageOptions->tabFourText(); ?></p>
                                <?php $methods = $pageOptions->tabFourPaymentsMethods();
                                if ($methods) {
                                ?>
                                <table class="payment-methods">
                                    <?php foreach ($methods as $method) { ?>
                                    <tr>
                                        <td class="title cl-titil-16-22"><strong><?php echo $method['title']; ?></strong></td>
                                        <td>
                                            <?php if ($method['images']) foreach ($method['images'] as $image) {?>
                                            <img height="<?php echo $image['height']; ?>" src="<?php echo $image['url']; ?>" alt="visa">
                                            <?php } ?>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </table>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
        </section>
    </div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>