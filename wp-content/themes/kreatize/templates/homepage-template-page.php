<?php /* Template Name: Homepage Page */ ?>
<?php if (!defined('ABSPATH')) {exit;} ?>

<?php
require_once get_stylesheet_directory().'/core/App.php';
use kreatize\App as App;
$kreatize = App::getInstance();
$pageOptions =  $kreatize->service('homePageOptions');
$sliderOptions =  $kreatize->service('homePageSliderOptions');
?>

<?php get_header(); ?>
    <div id="content" class="services">
        <section class="intro" id="intro" style="position: relative;
    background: -webkit-linear-gradient(rgba(45, 83, 111, 0.7), rgba(45, 83, 111, 0.7)), url(<?php echo $pageOptions->videoImage(); ?>) no-repeat center center;
    background: linear-gradient(rgba(45, 83, 111, 0.7), rgba(45, 83, 111, 0.7)), url(<?php echo $pageOptions->videoImage(); ?>) no-repeat center center;
    background-size: cover;
">
            <div class="container text-center">
                <h1 class="cl-titil-65"><?php echo $pageOptions->videoText(); ?></h1>
                <button class="btn btn-lg video" data-video="<?php echo $pageOptions->videoUrl(); ?>" data-toggle="modal" data-target="#play_video"><img class="playbutton" src="/wp-content/themes/kreatize/img/video_button.png"/></button>
                <a href="<?php echo $pageOptions->videoButtonUrl(); ?>" class="btn btn-primary navbar-btn text-uppercase btnUpload btn-button ind-but index-jetzt"><strong><?php echo $pageOptions->videoButtonText(); ?></strong></a>
            </div>
        </section>

        <section class="container-fluid suppliers">
            <div class="flexslider">
                <ul class="slides">
                    <li class="slide-one">
                        <!--slide 1-->
                        <div class="slide-wrap">
                            <h1 class="text-center cl-titil-40"><?php echo $sliderOptions->slideOneMainTitle(); ?></h1>
                            <div class="col-md-8 col-md-offset-2"><h3 class="text-center cl-titil-20"><?php echo $sliderOptions->slideOneMainText(); ?></h3></div>
                            <div class="row container first-xs-index">

                                <div class="col-md-3 col-xs-12">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <img src="<?php echo $sliderOptions->slideOneItemImage(1); ?>" alt="">
                                    </div>
                                    <div class="step-texts col-md-6 col-sm-6 col-xs-6">
                                        <h2 class="cl-titil-20">1. <?php echo $sliderOptions->slideOneItemTitle(1); ?></h2>
                                        <p class="cl-sans-16-22"><?php echo $sliderOptions->slideOneItemText(1); ?></p>
                                    </div>
                                </div>

                                <div class="col-md-3 col-xs-12">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <img src="<?php echo $sliderOptions->slideOneItemImage(2); ?>" alt="">
                                    </div>
                                    <div class="step-texts col-md-6 col-sm-6 col-xs-6">
                                        <h2 class="cl-titil-20">2. <?php echo $sliderOptions->slideOneItemTitle(2); ?></h2>
                                        <p class="cl-sans-16-22"><?php echo $sliderOptions->slideOneItemText(2); ?></p>
                                    </div>
                                </div>

                                <div class="col-md-3 col-xs-12">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <img src="<?php echo $sliderOptions->slideOneItemImage(3); ?>" alt="">
                                    </div>
                                    <div class="step-texts col-md-6 col-sm-6 col-xs-6">
                                        <h2 class="cl-titil-20">3. <?php echo $sliderOptions->slideOneItemTitle(3); ?></h2>
                                        <p class="cl-sans-16-22"><?php echo $sliderOptions->slideOneItemText(3); ?></p>
                                    </div>
                                </div>

                                <div class="col-md-3 col-xs-12">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <img src="<?php echo $sliderOptions->slideOneItemImage(4); ?>" alt="">
                                    </div>
                                    <div class="step-texts col-md-6 col-sm-6 col-xs-6">
                                        <h2 class="cl-titil-20">4. <?php echo $sliderOptions->slideOneItemTitle(4); ?></h2>
                                        <p class="cl-sans-16-22"><?php echo $sliderOptions->slideOneItemText(4); ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end of slide 1-->
                    </li>
                    <li class="slide-two" style=" background-image: url(<?php echo $sliderOptions->slideTwoItemBgImage(1); ?>);">
                        <!--slide 2-->
                        <div class="slide-wrap">
                            <h1 class="text-center cl-titil-40"><?php echo $sliderOptions->slideTwoMainTitle(); ?></h1>
                            <div class="row container online-administration first-xs-index">
                                <div class="col-md-4 col-xs-12">
                                    <img src="<?php echo $sliderOptions->slideTwoItemIcon(1); ?>" alt="">
                                    <h3 class="cl-titil-20" data-bg-image="1"><?php echo $sliderOptions->slideTwoItemTitle(1); ?></h3>
                                    <p class="cl-sans-16-22"><?php echo $sliderOptions->slideTwoItemText(1); ?></p>
                                    <img style="display: none;" class="data-bg-image-1" src="<?php echo $sliderOptions->slideTwoItemBgImage(1); ?>">

                                    <img src="<?php echo $sliderOptions->slideTwoItemIcon(2); ?>" alt="">
                                    <h3 class="cl-titil-20" data-bg-image="2"><?php echo $sliderOptions->slideTwoItemTitle(2); ?></h3>
                                    <p class="cl-sans-16-22"><?php echo $sliderOptions->slideTwoItemText(2); ?></p>
                                    <img style="display: none;" class="data-bg-image-2" src="<?php echo $sliderOptions->slideTwoItemBgImage(2); ?>">
                                </div>

                                <div class="col-md-4 col-xs-12">
                                    <img src="<?php echo $sliderOptions->slideTwoItemIcon(3); ?>" alt="">
                                    <h3 class="cl-titil-20" data-bg-image="3"><?php echo $sliderOptions->slideTwoItemTitle(3); ?></h3>
                                    <p class="cl-sans-16-22"><?php echo $sliderOptions->slideTwoItemText(3); ?></p>
                                    <img style="display: none;" class="data-bg-image-3" src="<?php echo $sliderOptions->slideTwoItemBgImage(3); ?>">

                                    <img src="<?php echo $sliderOptions->slideTwoItemIcon(4); ?>" alt="">
                                    <h3 class="cl-titil-20" data-bg-image="4"><?php echo $sliderOptions->slideTwoItemTitle(4); ?></h3>
                                    <p class="cl-sans-16-22"><?php echo $sliderOptions->slideTwoItemText(4); ?></p>
                                    <img style="display: none;" class="data-bg-image-4" src="<?php echo $sliderOptions->slideTwoItemBgImage(4); ?>">
                                </div>

                                <div class="col-md-4 col-xs-12">
                                </div>
                            </div>
                        </div>
                        <!--end of slide 2-->
                    </li>
                    <li class="slide-three">
                        <!--slide 3-->
                        <div class="slide-wrap">
                            <h1 class="text-center cl-titil-40"><?php echo $sliderOptions->slideThreeMainTitle(); ?></h1>
                            <div class="row container first-xs-index third-xs-index">

                                <div class="col-md-3 col-xs-12">
                                    <div class=" plus-step">
                                        <img src="<?php echo $sliderOptions->slideThreeItemIcon(1); ?>" alt=""> <h3 class="cl-titil-20"><?php echo $sliderOptions->slideThreeItemTitle(1); ?></h3>
                                        <p class="cl-sans-16-22 "><?php echo $sliderOptions->slideThreeItemText(1); ?></p>
                                    </div>
                                    <a class="kr-plus-circle" data-toggle="collapse" data-target="#step-one"><img src="/wp-content/themes/kreatize/img/kr-plus-circle.png"/><img style="display:none;" src="/wp-content/themes/kreatize/img/kr-minus-circle.png"/></a>
                                    <div id="step-one" class="collapse">
                                        <p class="cl-sans-16-22"><?php echo $sliderOptions->slideThreeItemHiddenText(1); ?></p>
                                    </div>
                                </div>

                                <div class="col-md-3 col-xs-12">
                                    <div class=" plus-step">
                                        <img src="<?php echo $sliderOptions->slideThreeItemIcon(2); ?>" alt=""> <h3 class="cl-titil-20"><?php echo $sliderOptions->slideThreeItemTitle(2); ?></h3>
                                        <p class="cl-sans-16-22 "><?php echo $sliderOptions->slideThreeItemText(2); ?></p>
                                    </div>
                                    <a class="kr-plus-circle" data-toggle="collapse" data-target="#step-two"><img src="/wp-content/themes/kreatize/img/kr-plus-circle.png"/><img style="display:none;" src="/wp-content/themes/kreatize/img/kr-minus-circle.png"/></a>
                                    <div id="step-two" class="collapse">
                                        <p class="cl-sans-16-22"><?php echo $sliderOptions->slideThreeItemHiddenText(2); ?></p>
                                    </div>
                                </div>


                                <div class="col-md-3 col-xs-12">
                                    <div class=" plus-step">
                                        <img src="<?php echo $sliderOptions->slideThreeItemIcon(3); ?>" alt=""> <h3 class="cl-titil-20"><?php echo $sliderOptions->slideThreeItemTitle(3); ?></h3>
                                        <p class="cl-sans-16-22 "><?php echo $sliderOptions->slideThreeItemText(3); ?></p>
                                    </div>
                                    <a class="kr-plus-circle" data-toggle="collapse" data-target="#step-three"><img src="/wp-content/themes/kreatize/img/kr-plus-circle.png"/><img style="display:none;" src="/wp-content/themes/kreatize/img/kr-minus-circle.png"/></a>
                                    <div id="step-three" class="collapse">
                                        <p class="cl-sans-16-22"><?php echo $sliderOptions->slideThreeItemHiddenText(3); ?></p>
                                    </div>
                                </div>


                                <div class="col-md-3 col-xs-12">
                                    <div class=" plus-step">
                                        <img src="<?php echo $sliderOptions->slideThreeItemIcon(4); ?>" alt=""> <h3 class="cl-titil-20"><?php echo $sliderOptions->slideThreeItemTitle(4); ?></h3>
                                        <p class="cl-sans-16-22 "><?php echo $sliderOptions->slideThreeItemText(4); ?></p>
                                    </div>
                                    <a class="kr-plus-circle" data-toggle="collapse" data-target="#step-four"><img src="/wp-content/themes/kreatize/img/kr-plus-circle.png"/><img style="display:none;" src="/wp-content/themes/kreatize/img/kr-minus-circle.png"/></a>
                                    <div id="step-four" class="collapse">
                                        <p class="cl-sans-16-22"><?php echo $sliderOptions->slideThreeItemHiddenText(4); ?></p>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <!--end of slide 3-->
                    </li>
                </ul>
            </div>
        </section>

        <section class="manu-process">
            <div class="container">
                <h1 class="text-center cl-titil-40"><?php echo $pageOptions->processesTitle(); ?></h1>
                <button onclick="location.href='<?php echo $pageOptions->processesMobileButtonUrl(); ?>';" class="btn btn-primary text-uppercase visible-xs" style="margin:auto;"><strong><?php echo $pageOptions->processesMobileButtonText(); ?></strong></button>
                <div role="tabpanel" class="hidden-xs">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs nav-pills" role="tablist">
<?php
                        $name =  $pageOptions->processesListTagName(1);
                        $activeTabSet = false;
?>
<?php
                        if ($name) {
                            $activeTabSet = true;
?>
                        <li role="presentation" class="active">
                            <a href="#panel-1" aria-controls="panel-1" role="tab" data-toggle="tab" class="cl-titil-28"><?php echo $name; ?></a>
                        </li>
<?php
                        }
?>
<?php
                        $name =  $pageOptions->processesListTagName(2);
?>
<?php
                        if ($name) {
                            $active = ($activeTabSet) ? '' : 'class="active"';
                            $activeTabSet = true;
?>
                        <li role="presentation" <?php echo $active; ?>>
                            <a href="#panel-2" aria-controls="panel-2" role="tab" data-toggle="tab" class="cl-titil-28"><?php echo $name; ?></a>
                        </li>
<?php
                        }
?>
<?php
                        $name =  $pageOptions->processesListTagName(3);
?>
<?php
                        if ($name) {
                            $active = ($activeTabSet) ? '' : 'active';
?>
                        <li role="presentation" class="guss <?php echo $active; ?>">
                            <a href="#panel-3" aria-controls="panel-3" role="tab" data-toggle="tab" class="cl-titil-28"><?php echo $name; ?></a>
                        </li>
<?php
                        }
?>
                    </ul>
                    <!-- Tab panels -->
                    <div class="tab-content">
<?php
                        $activeTabSet = false;
                        $query = $pageOptions->processesListQueryByTag(1);
?>
<?php
                        if ($query && $query->have_posts()) {
                            $activeTabSet = true;
?>
                        <div role="tabpanel" class="tab-pane active" id="panel-1">
                            <div class="row masonry-container">


<?php
                        while ($query->have_posts() ) {
                            $query->the_post();
?>
                        <a class="process-link" href="<?php echo get_permalink();?>">
                            <div class="col-md-3 col-sm-6 item">
                                <?php the_post_thumbnail( 'full' ); ?>
                                <h3 class="cl-titil-16-22"><?php the_title(); ?></h3>
                            </div>
                        </a>
<?php
                        }
?>

                            </div><!--End masonry-container  -->
                        </div><!--End panel-1  -->
<?php
                        }
                        wp_reset_postdata();
?>
<?php
                        $query = $pageOptions->processesListQueryByTag(2);
?>
<?php
                        if ($query && $query->have_posts()) {
                            $active = ($activeTabSet) ? '' : 'active';
                            $activeTabSet = true;
?>
                        <div role="tabpanel" class="tab-pane <?php echo $active; ?>" id="panel-2">

                            <div class="row masonry-container">

<?php
                                    while ($query->have_posts() ) {
                                        $query->the_post();
?>
                                        <a class="process-link" href="<?php echo get_permalink();?>">
                                            <div class="col-md-3 col-sm-6 item">
                                                <?php the_post_thumbnail( 'full' ); ?>
                                                <h3 class="cl-titil-16-22"><?php the_title(); ?></h3>
                                            </div>
                                        </a>
<?php
                                    }
?>
                            </div><!--End masonry-container  -->
                        </div><!--End panel-2  -->
<?php
                        }
                        wp_reset_postdata();
?>
<?php
                        $query = $pageOptions->processesListQueryByTag(3);
?>
<?php
                        if ($query && $query->have_posts()) {
                            $active = ($activeTabSet) ? '' : 'active';
?>

                        <div role="tabpanel" class="tab-pane <?php echo $active; ?>" id="panel-3">
                            <div class="row masonry-container">
<?php
                                    while ($query->have_posts() ) {
                                        $query->the_post();
?>
                                        <a class="process-link" href="<?php echo get_permalink();?>">
                                            <div class="col-md-3 col-sm-6 item">
                                                <?php the_post_thumbnail( 'full' ); ?>
                                                <h3 class="cl-titil-16-22"><?php the_title(); ?></h3>
                                            </div>
                                        </a>
<?php
                                    }
?>
                            </div><!--End masonry-container  -->
                        </div><!--End panel-3  -->
<?php
                        }
                        wp_reset_postdata();
?>

                    </div><!--End tab-content  -->
                </div><!--End tabpanel  -->
            </div>
        </section>

        <section class="container partners">
            <h1 class="text-center cl-titil-40"><?php echo $pageOptions->partnersTitle(); ?></h1>
            <div>
                <div class="col-md-4 partner-content">
                    <div class="item">
                        <div class="partner-img">
                            <img src="<?php echo $pageOptions->partnersLogo(1); ?>">
                        </div>
                        <div class="partner-texts ">
                            <span class="quote">&#8221;</span><p class="cl-sans-16-22"><?php echo $pageOptions->partnersQuote(1); ?></p><span class="quote-ende">&#8220;</span>
                            <p class="author cl-sans-16-22"><?php echo $pageOptions->partnersAuthor(1); ?></p>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 partner-content">
                    <div class="item">
                        <div class="partner-img">
                            <img src="<?php echo $pageOptions->partnersLogo(2); ?>">
                        </div>
                        <div class="partner-texts mid">
                            <span class="quote">&#8221;</span><p class="cl-sans-16-22"><?php echo $pageOptions->partnersQuote(2); ?></p><span class="quote-ende">&#8220;</span>
                            <p class="author cl-sans-16-22"><?php echo $pageOptions->partnersAuthor(2); ?></p>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 partner-content">
                    <div class="item">
                        <div class="partner-img">
                            <img src="<?php echo $pageOptions->partnersLogo(3); ?>">
                        </div>
                        <div class="partner-texts">
                            <span class="quote">&#8221;</span><p class="cl-sans-16-22"><?php echo $pageOptions->partnersQuote(3); ?></p><span class="quote-ende">&#8220;</span>
                            <p class="author cl-sans-16-22"><?php echo $pageOptions->partnersAuthor(3); ?></p>
                        </div>
                    </div>
                </div>
            </div>
            <!--end of partner testimony-->
            <!--<div class="handling col-md-12">
                <div class="col-md-3" style="">
                    <h2>45%</h3>
                    <hr style="width:40.5%;"/>
                    <p>weniger Emails und Telefonate</p>
                </div>

                <div class="col-md-3">
                    <h2>80%</h3>
                    <hr style="width:72%;"/>
                    <p>schneller Angebote erstellen</p>
                </div>

                <div class="col-md-3">
                    <h2>95%</h3>
                    <hr style="width:85.5%;"/>
                    <p>reduzierte Angebotsbearbeitungskosten</p>
                </div>

                <div class="col-md-3">
                    <h2>100%</h3>
                    <hr style="width:90%;"/>
                    <p>qualifizierte Anfragen</p>
                </div>
            </div>-->
            <div class="handling col-md-12">
                <div class="col-md-3 col-sm-6 col-xs-12 pad-r-l-0">
                    <span class="cl-titil-40"><?php echo $pageOptions->partnersResultPercent(1); ?>%</span>
                    <div class="meter">
                        <span style="width: <?php echo $pageOptions->partnersResultPercent(1); ?>%;"></span>
                    </div>
                    <span class="cl-sans-16-22"><?php echo $pageOptions->partnersResultText(1); ?></span>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12 pad-r-l-0">
                    <span class="cl-titil-40"><?php echo $pageOptions->partnersResultPercent(2); ?>%</span>
                    <div class="meter">
                        <span style="width: <?php echo $pageOptions->partnersResultPercent(2); ?>%;"></span>
                    </div>
                    <span class="cl-sans-16-22"><?php echo $pageOptions->partnersResultText(2); ?></span>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12 pad-r-l-0">
                    <span class="cl-titil-40"><?php echo $pageOptions->partnersResultPercent(3); ?>%</span>
                    <div class="meter">
                        <span style="width: <?php echo $pageOptions->partnersResultPercent(3); ?>%;"></span>
                    </div>
                    <span class="cl-sans-16-22"><?php echo $pageOptions->partnersResultText(3); ?></span>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12 pad-r-l-0   ">
                    <span class="cl-titil-40"><?php echo $pageOptions->partnersResultPercent(4); ?>%</span>
                    <div class="meter">
                        <span style="width: <?php echo $pageOptions->partnersResultPercent(4); ?>%;"></span>
                    </div>
                    <span class="cl-sans-16-22"><?php echo $pageOptions->partnersResultText(4); ?></span>
                </div>
            </div>
            <div class="col-md-12 text-center" style="margin-top: 60px;">
                <a href="/<?php echo $pageOptions->partnersButtonUrl(); ?>"  class="btn btn-primary text-uppercase btn-button index-jetzt"><strong><?php echo $pageOptions->partnersButtonText(); ?></strong></a>
            </div>
        </section>

        <section class="customer-testimony">
            <div class="container">
                <div class="row">
                    <div class="col-md-2 hidden-xs hidden-sm" style="padding:0px;">
                        <img class="img-circle pull-right" src="<?php echo $pageOptions->customersSayPhoto(); ?>" alt="Jan Roosen"></img>
                    </div>
                    <div class="col-md-8 text-center">
                        <h1 class="text-center cl-titil-40" ><?php echo $pageOptions->customersSayTitle(); ?></h1>
                        <p class="cl-titil-20"><?php echo $pageOptions->customersSayQuote(); ?></p>
                        <br/><img class="logo-customer hidden-xs" src="<?php echo $pageOptions->customersSayLogo(); ?>"><p class="cl-sans-16-22"><?php echo $pageOptions->customersSayInformation(); ?></p>
                        <div class="col-md-2 visible-xs visible-sm text-center">
                            <img class="img-circle" src="<?php echo $pageOptions->customersSayPhoto(); ?>" alt="Jan Roosen">
                        </div>
                        <br class="hidden-sm hidden-xs"/>
                        <a href="<?php echo $pageOptions->customersSayButtonUrl(); ?>" class="btn btn-primary text-uppercase btn-button"><strong><?php echo $pageOptions->customersSayButtonText(); ?></strong></a>
                    </div>
                    <div class="col-md-2 text-center">
                    </div>
                </div>
            </div>
        </section>
    </div>







    </section>
<?php get_sidebar(); ?>
<?php get_footer(); ?>