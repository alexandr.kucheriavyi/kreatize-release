<?php /* Template Name: Innovation Page */ ?>
<?php if (!defined('ABSPATH')) {exit;} ?>

<?php
require_once get_stylesheet_directory().'/core/App.php';
use kreatize\App as App;
$kreatize = App::getInstance();
$pageOptions =  $kreatize->service('InnovationPageOptions');
$homepageSliderOptions =  $kreatize->service('HomePageSliderOptions');
?>

<?php get_header(); ?>
    <section id="innovation-intro" >
        <div class="first-block" style="
                background: url(<?php echo $pageOptions->headerImage(); ?>) no-repeat center center;
                background-size: cover;
                ">
            <div class="container">
                <div class="col-md-10 col-md-offset-1 text-center" >
                    <h1 class="cl-titil-65 innovation-h1" ><?php echo $pageOptions->headerText(); ?></h1>
                    <div class="col-md-12">
                        <a href="<?php echo $pageOptions->headerButtonUrl(); ?>" class="btn btn-primary navbar-btn text-uppercase btnUpload btn-button "><strong><?php echo $pageOptions->headerButtonText(); ?></strong></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="innovation-first-text">
        <div class="container">

            <div class="col-md-12">
                <h1 class="cl-titil-40 text-center"><?php echo $pageOptions->sectionTwoTitle(); ?></h1>
                <?php echo $pageOptions->sectionTwoContent(); ?>
                <div class="row in-cards">
                    <div class="col-md-4">
                        <div class="col-md-12 in-card-item" style="background: #E0F5FB; padding-left: 0px;">
                            <div class="col-md-4 "><img src="<?php echo $pageOptions->cardImage(1); ?>" alt="" width="100%"></div>
                            <div class="col-md-8">
                                <strong class="cl-titil-28"><?php echo $pageOptions->cardText(1, 1); ?></strong><br>
                                <span class="cl-titil-40"><?php echo $pageOptions->cardText(1, 2); ?></span><br>
                                <h4 class="cl-titil-20">
                                    <strong><?php echo $pageOptions->cardText(1, 3); ?></strong>
                                </h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 ">
                        <div class="col-md-12 in-card-item  card-item-2" style="background: #EFFAFD;">
                            <div>
                                <strong class="cl-titil-28"><?php echo $pageOptions->cardText(2, 1); ?></strong><br>
                                <span class="cl-titil-40"><?php echo $pageOptions->cardText(2, 2); ?></span><br>
                                <h4 class="cl-titil-20">
                                    <strong><?php echo $pageOptions->cardText(2, 3); ?></strong>
                                </h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5" style="padding-right: 5px;">
                        <div class="col-md-12 in-card-item" >
                            <div>
                                <strong class="cl-titil-20"><?php echo $pageOptions->cardText(3, 1); ?></strong><br>
                                <h4 class="cl-titil-20">
                                    <?php echo $pageOptions->cardText(3, 2); ?>
                                </h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <section class="innovation-second-text" >
        <div class="container">
            <h1 class="text-center cl-titil-40"><?php echo $homepageSliderOptions->slideOneMainTitle(); ?></h1>
            <div class="row container">
                <div class="col-md-3 col-xs-12">
                    <div class="col-md-6 col-sm-3 col-xs-4">
                        <img src="<?php echo $homepageSliderOptions->slideOneItemImage(1); ?>" alt="">
                    </div>
                    <div class="step-texts col-md-6 col-sm-9 col-xs-8 pad-0">
                        <h2 class="cl-titil-28"><strong>1.<br><?php echo $homepageSliderOptions->slideOneItemTitle(1); ?></strong></h2>
                        <p><?php echo $homepageSliderOptions->slideOneItemText(1); ?></p>
                    </div>
                </div>

                <div class="col-md-3 col-xs-12">
                    <div class="col-md-6 col-sm-3 col-xs-4">
                        <img src="<?php echo $homepageSliderOptions->slideOneItemImage(2); ?>" alt="">
                    </div>
                    <div class="step-texts col-md-6 col-sm-9 col-xs-8 pad-0">
                        <h2 class="cl-titil-28"><strong>2.<br><?php echo $homepageSliderOptions->slideOneItemTitle(2); ?></strong></h2>
                        <p><?php echo $homepageSliderOptions->slideOneItemText(2); ?></p>
                    </div>
                </div>

                <div class="col-md-3 col-xs-12">
                    <div class="col-md-6 col-sm-3 col-xs-4">
                        <img src="<?php echo $homepageSliderOptions->slideOneItemImage(3); ?>" alt="">
                    </div>
                    <div class="step-texts col-md-6 col-sm-9 col-xs-8 pad-0">
                        <h2 class="cl-titil-28"><strong>3.<br><?php echo $homepageSliderOptions->slideOneItemTitle(3); ?></strong></h2>
                        <p><?php echo $homepageSliderOptions->slideOneItemText(3); ?></p>
                    </div>
                </div>

                <div class="col-md-3 col-xs-12">
                    <div class="col-md-6 col-sm-3 col-xs-4">
                        <img src="<?php echo $homepageSliderOptions->slideOneItemImage(4); ?>" alt="">
                    </div>
                    <div class="step-texts col-md-6 col-sm-9 col-xs-8 pad-0">
                        <h2 class="cl-titil-28"><strong>4.<br><?php echo $homepageSliderOptions->slideOneItemTitle(4); ?></strong></h2>
                        <p><?php echo $homepageSliderOptions->slideOneItemText(4); ?></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="innovation-third-text services">
        <div class="container manu-process" >

            <div class="row" >
                <div role="tabpanel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs nav-pills" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#panel-1" aria-controls="panel-1" role="tab" data-toggle="tab" class="cl-titil-28"><?php echo $pageOptions->tabName(1); ?></a>
                        </li>
                        <li role="presentation">
                            <a href="#panel-2" aria-controls="panel-2" role="tab" data-toggle="tab" class="cl-titil-28"><?php echo $pageOptions->tabName(2); ?></a>
                        </li>
                    </ul>
                    <!-- Tab panels -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="panel-1" >
                            <?php echo $pageOptions->tabContent(1); ?>
                        </div><!--End panel-1  -->

                        <div role="tabpanel" class="tab-pane" id="panel-2" >
                            <?php echo $pageOptions->tabContent(2); ?>
                        </div><!--End panel-2  -->

                    </div><!--End tab-content  -->
                </div><!--End tabpanel  -->
            </div>

        </div>
    </section>
    <section class="innovation-4-th-text">
        <div class="container">
            <div class="col-md-12 text-center" >
                <h1 class="cl-titil-40 text-center"><?php echo $pageOptions->lastSectionText(); ?></h1>
                <a href="<?php echo $pageOptions->lastSectionButtonUrl(); ?>" class="btn btn-primary navbar-btn text-uppercase btnUpload btn-button"><strong><?php echo $pageOptions->lastSectionButtonText(); ?></strong></a>
            </div>
        </div>
    </section>

<?php get_sidebar(); ?>
<?php get_footer(); ?>