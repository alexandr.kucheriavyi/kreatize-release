<?php /* Template Name: Press Page */ ?>
<?php if (!defined('ABSPATH')) {exit;} ?>

<?php
require_once get_stylesheet_directory().'/core/App.php';
use kreatize\App as App;
$kreatize = App::getInstance();
$pageOptions =  $kreatize->service('PressPageOptions');

$breadcrumbs =  $kreatize->service('Breadcrumbs');
?>

<?php get_header(); ?>
    <section id="presse" style="padding-top: 0px;">
        <div class="first-block">
            <!--<img src="/wp-content/themes/kreatize/img/firstBlock.jpg" alt="">-->
            <div class="col-md-12">
                <div class="child-first-block col-md-10 col-md-offset-1" style="display: block;">
                    <h1 class="cl-titil-65"><?php echo $pageOptions->headerTitle(); ?></h1>
                </div>
            </div>
            <a class="rolling" href="#presse-nav" aria-expanded="true" aria-controls="anchor-Pressespiegel"><i class="glyphicon glyphicon-menu-down circle" aria-hidden="true"></i></a>

        </div>

    </section>

    <!-- Content -->
    <section style="padding-top: 0px;" >
        <div class="container fadi-contents">

           <?php echo $breadcrumbs->page();?>
            <div class="container manu-process" >

                <div class="col-md-12 text-center presse-nav" id="presse-anchor-nav">

                    <a href="#Pressespiegel" aria-controls="anchor-Pressespiegel" id="prpie" class="cl-titil-20 rolling anchors active"><?php echo $pageOptions->navigationItem(1); ?></a><br class="visible-xs">
                    <a href="#Pressemitteilungen" aria-controls="anchor-Pressemitteilungen" class="cl-titil-20 rolling anchors"><?php echo $pageOptions->navigationItem(2); ?></a><br class="visible-xs">
                    <a href="#Downloads" aria-controls="anchor-Downloads" class="cl-titil-20 rolling anchors"><?php echo $pageOptions->navigationItem(3); ?></a><br class="visible-xs">
                </div>

                <div class="row presse-row">
                    <div class="col-md-8 pad-r-l-0">
                        <h3 style="margin-bottom: 50px;  padding-left: 5px;" class="cl-titil-40" id="anchor-Pressespiegel"><?php echo $pageOptions->pressReviewTitle(); ?></h3>

                        <h4 class="datepost cl-titil-20 "><?php echo $pageOptions->pressReviewItemDate(1); ?></h4>
                        <img src="/wp-content/themes/kreatize/img/checkmark_kreatize.png" class="imageok" >
                        <p class="textpost cl-sans-16-22">
                            <b class="cl-titil-16-22"><?php echo $pageOptions->pressReviewItemTitle(1); ?></b><br>
                            <?php echo $pageOptions->pressReviewItemText(1); ?>
                        </p>

                        <h4 class="datepost cl-titil-20 "><?php echo $pageOptions->pressReviewItemDate(2); ?></h4>
                        <img src="/wp-content/themes/kreatize/img/checkmark_kreatize.png" class="imageok" >
                        <p class="textpost cl-sans-16-22">
                            <b class="cl-titil-16-22"><?php echo $pageOptions->pressReviewItemDate(2); ?></b><br>
                            <?php echo $pageOptions->pressReviewItemText(2); ?>
                        </p>

                        <h4 class="datepost cl-titil-20"><?php echo $pageOptions->pressReviewItemDate(3); ?></h4>
                        <img src="/wp-content/themes/kreatize/img/checkmark_kreatize.png" class="imageok" >
                        <p class="textpost cl-sans-16-22">
                            <b class="cl-titil-16-22"><?php echo $pageOptions->pressReviewItemDate(3); ?></b><br>
                            <?php echo $pageOptions->pressReviewItemText(3); ?>
                            <br><br><br>
                            <b class="endposts">> <a href="<?php echo $pageOptions->blogLinkUrl(); ?>" style="color: #000;" target="_blank"><?php echo $pageOptions->blogLinkText(); ?></a></b>
                        <div class="linedown"></div>
                        </p>
                        

                    </div>
                    <div class="col-md-2 col-md-offset-1 Pressekontakt " >
                        <h3 class="cl-titil-40"><?php echo $pageOptions->contactsTitle(); ?></h3>
                        <p  class="cl-sans-16-22 text-center">
                            <img src="<?php echo $pageOptions->contactsPhoto(); ?>" class="img-circle " style=" margin-bottom: 15px; "><br>
                            <?php echo $pageOptions->contactsName(); ?><br>
                            <?php echo $pageOptions->contactsEmail(); ?><br>
                            <?php echo $pageOptions->contactsPhone(); ?>
                        </p>
                        <h3 class="cl-titil-40"><?php echo $pageOptions->contactFormTitle(); ?></h3>

                        <?php
                        $mailChimp = $kreatize->service('MailChimpForm');
                        $content =  do_shortcode('[mc4wp_form id="193"]');
                        echo $mailChimp->replacePressFormValues($content);
                        ?>
                    </div>


                </div>
            </div>
    </section>
    <section id="new-layout">
        <div class="container">
            <h3 style="margin-bottom: 50px; padding-left: 5px; " class=" cl-titil-40" id="anchor-Pressemitteilungen"><?php echo $pageOptions->pressReleasesTitle(); ?></h3>
            <?php $items = $pageOptions->pressReleasesItems(); ?>
            <?php if ($items) {
                foreach ($items as $item) {
            ?>
            <div class="col-md-1 cl-titil-20">
                <?php echo $item['date']; ?>
            </div>
            <div class="col-md-11">
                <p class="cl-sans-16-22">
                    <b class="cl-titil-16-22"> <?php echo $item['title']; ?></b><br>
                    <?php echo $item['text']; ?><a href="<?php echo $item['file']; ?>" target="_blank" class="dl-career-arrow"><img src="/wp-content/themes/kreatize/img/arrow_career.png">PDF</a>
                </p>
            </div>
            <?php
                }
            }
            ?>
        </div>
    </section>

    <div class="services presse-bot">
        <section class="container partners " style="padding-top: 30px !important;">
            <h3 class="cl-titil-40" id="anchor-Downloads"><?php echo $pageOptions->downloadsTitle(); ?></h3>
            <div class="row " style="padding-left: 5px;">
                <div class="col-md-4 partner-content text-center " >
                    <div class="item">
                        <div class="left-img">
                            <img src="<?php echo $pageOptions->downloadsBlockImage(1); ?>">
                        </div>
                        <div data-file-url="<?php echo $pageOptions->downloadsBlockFileUrl(1); ?>" class="partner-texts download-file" >
                            <strong class="cl-titil-20"><?php echo $pageOptions->downloadsBlockTitle(1); ?></strong>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 partner-content text-center">
                    <div class="item">
                        <div class="center-img">
                            <div style="padding: 0px !important;">
                                <img src="<?php echo $pageOptions->downloadsBlockImage(2, 1); ?>"><img src="<?php echo $pageOptions->downloadsBlockImage(2, 2); ?>">

                            </div>

                        </div>
                        <div data-file-url="<?php echo $pageOptions->downloadsBlockFileUrl(2); ?>" class="partner-texts download-file" >
                            <strong class="cl-titil-20"><?php echo $pageOptions->downloadsBlockTitle(2); ?></strong>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 partner-content text-center">
                    <div class="item">
                        <div class="right-img">
                            <img src="<?php echo $pageOptions->downloadsBlockImage(3); ?>" >
                        </div>
                        <div data-file-url="<?php echo $pageOptions->downloadsBlockFileUrl(2); ?>" class="partner-texts download-file" >
                            <strong class="cl-titil-20"><?php echo $pageOptions->downloadsBlockTitle(3); ?></strong>
                        </div>
                    </div>
                </div>
            </div>


        </section>
    </div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>