<?php /* Template Name: Rapid Prototyping Page */ ?>
<?php if (!defined('ABSPATH')) {exit;} ?>


<?php
require_once get_stylesheet_directory().'/core/App.php';
use kreatize\App as App;
$kreatize = App::getInstance();
$pageOptions =  $kreatize->service('RapidPrototypingPageOptions');
?>

<?php get_header(); ?>
    <div id="content" class="sign-up" >
        <section class="rapid-prototyping" id="intro" >
            <div class="container text-center" >
                <div class="form-group col-md-12 ">
                    <h1 class="cl-titil-65" style="color: #214a61;"><?php echo $pageOptions->headerTitle(); ?></h1>
                    <div class="col-md-12 text-center" ><a href="<?php echo $pageOptions->headerButtonUrl(); ?>" class="mybutton white text-uppercase"><strong><?php echo $pageOptions->headerButtonText(); ?></strong></a></div>
                </div>


            </div>
        </section>
        <section class="container cards">
            <div class="hidden-xs hidden-sm">
                <div class="row">
                    <div class="col-md-4 card wraps">
                        <div class="card-content left">
                            <h1 class="cl-titil-28"><?php echo $pageOptions->cardTitle(1); ?></h1>
                            <p class="cl-sans-16-22"><?php echo $pageOptions->cardText(1); ?></p>
                        </div>
                        <div class="lefttriangle"></div>
                    </div>
                    <div class="col-md-4 card wraps">
                        <div class="card-content mid">
                            <h1 class="cl-titil-28"><?php echo $pageOptions->cardTitle(2); ?></h1>
                            <p class="cl-sans-16-22"><?php echo $pageOptions->cardText(2); ?></p>
                        </div>
                        <div class="midtriangle">
                        </div>
                    </div>
                    <div class="col-md-4 card wraps">
                        <div class="card-content right">
                            <h1 class="cl-titil-28"><?php echo $pageOptions->cardTitle(3); ?></h1>
                            <p class="cl-sans-16-22"><?php echo $pageOptions->cardText(3); ?></p>
                        </div>
                        <div class="righttriangle">
                        </div>
                    </div>
                </div>
            </div>
            <div class="visible-xs visible-sm">
                <div class="col-md-4 card">
                    <div class="card-content">
                        <h1 class="cl-titil-40"><?php echo $pageOptions->cardTitle(1); ?></h1>
                        <p class="cl-titil-20"><?php echo $pageOptions->cardText(1); ?></p>
                    </div>
                </div>
                <div class="col-md-4 card">
                    <div class="card-content">
                        <h1 class="cl-titil-40"><?php echo $pageOptions->cardTitle(2); ?></h1>
                        <p class="cl-titil-16-22"><?php echo $pageOptions->cardText(2); ?></p>
                    </div>
                </div>
                <div class="col-md-4 card bottom" id="bots-top">
                    <div class="card-content">
                        <h1 class="cl-titil-40"><?php echo $pageOptions->cardTitle(3); ?></h1>
                        <p class="cl-titil-16-22"><?php echo $pageOptions->cardText(3); ?></p>
                    </div>
                    <div class="bottom-xss" id="bots-bottom">
                    </div>
                </div>
            </div>
            <div class="text-center col-md-10 col-md-offset-1 cl-sans-20 after-cards" ><strong><?php echo $pageOptions->cardsSectionText(); ?></strong></div>
        </section>
    </div>
    <section class="rapid-text">
        <div class="container">
            <div class="col-md-10 col-md-offset-1">
                <h1 class="cl-titil-40 text-center"><?php echo $pageOptions->rapidTextSectionTitle(); ?></h1>
            </div><div class="col-md-2"></div>
            <div class="col-md-12">
                <p class="cl-sans-16-22"><?php echo $pageOptions->rapidTextSectionText(1); ?></p>
            </div>
            <div class="col-md-12" style="padding-left: 20px; padding-right: 20px;">
                <p class="cl-titil-20    text-center"><?php echo $pageOptions->rapidTextSectionText(2); ?></p>
            </div>
            <div class="col-md-12 text-center" style="margin-top: 60px; margin-bottom: 20px;"><a href="<?php echo $pageOptions->rapidTextSectionButtonUrl(); ?>" class="mybutton white text-uppercase"><strong><?php echo $pageOptions->rapidTextSectionButtonText(); ?></strong></a></div>
        </div>
    </section>
    <section class="rapid-three-blocks services">
        <div class="container manu-process" >

            <div class="col-md-12" >
                <div role="tabpanel" >
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs nav-pills" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#panel-1" id="panel-14" aria-controls="panel-1" role="tab" data-toggle="tab" class="cl-titil-28 clear-click"><?php echo $pageOptions->tabName(1); ?></a>
                        </li><br class="visible-xs visible-sm">
                        <li role="presentation">
                            <a href="#panel-2" aria-controls="panel-2" role="tab" data-toggle="tab" class="cl-titil-28 clear-click"><?php echo $pageOptions->tabName(2); ?></a>
                        </li><br class="visible-xs visible-sm">
                        <li role="presentation">
                            <a href="#panel-3" id="panel-34" aria-controls="panel-3" role="tab" data-toggle="tab" class="cl-titil-28"><?php echo $pageOptions->tabName(3); ?></a>
                        </li><br class="visible-xs visible-sm">
                    </ul>
                    <!-- Tab panels -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="panel-1" >
                            <div class="col-md-12 tab-rapid">

                                <div class="col-md-8">
                                    <div class="rap-tab-1">
                                        <div class="col-md-12">
                                            <h1 class="cl-titil-40" ><?php echo $pageOptions->tabOneTitle(); ?></h1>
                                            <h3 class="cl-titil-20"><strong><?php echo $pageOptions->tabOneText(1); ?></strong></h3>
                                            <p class="cl-sans-16-22"><?php echo $pageOptions->tabOneText(2); ?></p>
                                        </div>
                                        <div class="col-md-11">
                                            <p class="cl-titil-20">„<?php echo $pageOptions->tabOneQuote(); ?>“ </p>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-md-offset-5"><p class="cl-sans-16-22" "><?php echo $pageOptions->tabOneQuoteAuthor(); ?></p></div>

                                </div>
                                <div class="col-md-4 rap-tab-1 hidden-xs hidden-sm" id="supplier-3-block" >
                                    <div class="col-md-12 rap-tab-1" style="display: table;">
                                        <div style="display: table-cell; vertical-align: middle;">
                                            <div class="col-xs-8 col-xs-offset-2 col-sm-8 col-sm-offset-2 col-md-9">
                                                <img src="<?php echo $pageOptions->tabOneImage(); ?>" alt="">
                                            </div>
                                            <div class="visible-xs visible-sm col-xs-12 col-sm-12 text-center" style="padding-top: 20px;">
                                                <p class="cl-sans-16-22" "><?php echo $pageOptions->tabOneImageDescription(); ?></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 hidden-xs hidden-sm"><p class="cl-sans-16-22" "><?php echo $pageOptions->tabOneImageDescription(); ?></p></div>
                                </div>

                            </div>
                        </div><!--End panel-1  -->

                        <div role="tabpanel" class="tab-pane" id="panel-2" >
                            <div class="col-md-12">
                                <div class="col-md-8">
                                    <h1 class="cl-titil-40 wraps" ><?php echo $pageOptions->tabTwoTitle(); ?></h1>
                                    <div class="col-md-12">
                                        <p class="cl-sans-16-22"><?php echo $pageOptions->tabTwoText(1); ?></p>
                                    </div>
                                    <h3 class="cl-titil-20"><strong><?php echo $pageOptions->tabTwoText(2); ?></strong></h3>
                                    <div class="but-div">
                                        <a href="<?php echo $pageOptions->tabTwoButtonContactPageUrl(); ?>" class="mybutton text-uppercase cl-titil-20 white"><strong> <?php echo $pageOptions->tabTwoButtonText(); ?></strong></a>
                                    </div>
                                </div>
                                <div class="col-md-4 lins-rapid-block">
                                    <a href="<?php echo $pageOptions->tabTwoProcessUrl(1); ?>" class='links-rapid'>
                                        <div class="col-md-6">
                                            <img src="<?php echo $pageOptions->tabTwoProcessImage(1); ?>" alt="">
                                            <p class="text-center cl-sans-16-22"><?php echo $pageOptions->tabTwoProcessName(1); ?></p>
                                        </div>
                                    </a>
                                    <a href="<?php echo $pageOptions->tabTwoProcessUrl(2); ?>" class='links-rapid'>
                                        <div class="col-md-6">
                                            <img src="<?php echo $pageOptions->tabTwoProcessImage(2); ?>" alt="">
                                            <p class="text-center cl-sans-16-22"><?php echo $pageOptions->tabTwoProcessName(2); ?></p>
                                        </div>
                                    </a>
                                    <a href="<?php echo $pageOptions->tabTwoProcessUrl(3); ?>" class='links-rapid'>
                                        <div class="col-md-6 col-md-offset-3">
                                            <img src="<?php echo $pageOptions->tabTwoProcessImage(3); ?>" alt="">
                                            <p class="text-center cl-sans-16-22"><?php echo $pageOptions->tabTwoProcessName(3); ?></p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div><!--End panel-2  -->

                        <div role="tabpanel" class="tab-pane" id="panel-3" >
                            <div class="col-md-12">
                                <div class="col-md-6 third-pane-1-1" >
                                    <div class="col-md-12 third-pane-1-2" >
                                        <div class="h1-div"><h1 class="cl-titil-40 title-h1"><?php echo $pageOptions->tabThreeColTitle('left'); ?></h1></div>
                                        <p class="cl-sans-16-22 contetnt-p"><?php echo $pageOptions->tabThreeColText('left'); ?></p>
                                    </div>
                                    <?php $rows = $pageOptions->tabThreeColTableRows('left'); ?>
                                    <?php if ($rows) {?>

                                    <table class="table ">
                                        <thead class="cl-titil-16-22">
                                        <tr>
                                            <th class="cols-1 wraps"><?php echo $rows[0][0]; ?></th>
                                            <th class="cols-2 "><?php echo $rows[0][1]; ?></th>
                                            <th class="cols-3 "><?php echo $rows[0][2]; ?></th>
                                            <th class="cols-4 wraps"><?php echo $rows[0][3]; ?></th>
                                        </tr>
                                        </thead>
                                        <tbody class="cl-titil-16-22">
                                        <tr>
                                            <th class="cols-1 "><?php echo $rows[1][0]; ?></th>
                                            <td class="cols-2 wraps"><?php echo $rows[1][1]; ?></td>
                                            <td class="cols-3 wraps"><?php echo $rows[1][2]; ?></td>
                                            <td class="cols-4 wraps"><?php echo $rows[1][3]; ?></td>
                                        </tr>
                                        <tr>
                                            <th class="cols-1 "><?php echo $rows[2][0]; ?></th>
                                            <td class="cols-2 wraps"><?php echo $rows[2][1]; ?></td>
                                            <td class="cols-3 wraps"><?php echo $rows[2][2]; ?></td>
                                            <td class="cols-4 wraps"><?php echo $rows[2][3]; ?></td>
                                        </tr>
                                        <tr>
                                            <th class="cols-1 wraps"><?php echo $rows[3][0]; ?></th>
                                            <td class="cols-2 wraps"><?php echo $rows[3][1]; ?></td>
                                            <td class="cols-3 wraps"><?php echo $rows[3][2]; ?></td>
                                            <td class="cols-4 wraps"><?php echo $rows[3][3]; ?></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <?php }?>
                                    <div class="col-md-6 col-md-offset-6 rapid-effect">
                                    </div>
                                </div>
                                <div class="col-md-6 third-pane-2-1" >
                                    <div class="col-md-12 third-pane-2-2" >
                                        <div class="h1-div"><h1 class="cl-titil-40 title-h1"><?php echo $pageOptions->tabThreeColTitle('right'); ?></h1></div>
                                        <p class="cl-sans-16-22 contetnt-p"><?php echo $pageOptions->tabThreeColText('right'); ?></p>
                                    </div>
                                    <?php $rows = $pageOptions->tabThreeColTableRows('right'); ?>

                                    <?php if ($rows) {?>

                                        <table class="table ">
                                            <thead class="cl-titil-16-22">
                                            <tr>
                                                <th class="cols-1 wraps"><?php echo $rows[0][0]; ?></th>
                                                <th class="cols-2 "><?php echo $rows[0][1]; ?></th>
                                                <th class="cols-3 "><?php echo $rows[0][2]; ?></th>
                                                <th class="cols-4 wraps"><?php echo $rows[0][3]; ?></th>
                                            </tr>
                                            </thead>
                                            <tbody class="cl-titil-16-22">
                                            <tr>
                                                <th class="cols-1 "><?php echo $rows[1][0]; ?></th>
                                                <td class="cols-2 wraps"><?php echo $rows[1][1]; ?></td>
                                                <td class="cols-3 wraps"><?php echo $rows[1][2]; ?></td>
                                                <td class="cols-4 wraps"><?php echo $rows[1][3]; ?></td>
                                            </tr>
                                            <tr>
                                                <th class="cols-1 "><?php echo $rows[2][0]; ?></th>
                                                <td class="cols-2 wraps"><?php echo $rows[2][1]; ?></td>
                                                <td class="cols-3 wraps"><?php echo $rows[2][2]; ?></td>
                                                <td class="cols-4 wraps"><?php echo $rows[2][3]; ?></td>
                                            </tr>
                                            <tr>
                                                <th class="cols-1 wraps"><?php echo $rows[3][0]; ?></th>
                                                <td class="cols-2 wraps"><?php echo $rows[3][1]; ?></td>
                                                <td class="cols-3 wraps"><?php echo $rows[3][2]; ?></td>
                                                <td class="cols-4 wraps"><?php echo $rows[3][3]; ?></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    <?php }?>
                                    <div class="col-md-6 col-md-offset-6 rapid-effect">
                                    </div>
                                </div>
                            </div>

                        </div><!--End panel-3  -->
                    </div><!--End tab-content  -->
                </div><!--End tabpanel  -->
            </div>

        </div>
    </section>

<?php get_sidebar(); ?>
<?php get_footer(); ?>