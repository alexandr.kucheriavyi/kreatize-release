<?php /* Template Name: Rapid Prototyping Page */ ?>

<?php
require_once get_stylesheet_directory().'/core/App.php';
use kreatize\App as App;
$kreatize = App::getInstance();
$pageOptions =  $kreatize->service('RapidPrototypingPageOptions');
?>

<?php get_header(); ?>
    <div id="content" class="sign-up" >
        <section class="rapid-prototyping" id="intro" >
            <div class="container text-center" >
                <div class="form-group col-md-12 ">
                    <h1 class="cl-titil-65" style="color: #214a61;">Mit KREATIZE® in nur wenigen<br>Schritten Ihren Prototypen realisieren.</h1>
                    <div class="col-md-12 text-center" ><a href="/signup.html" class="mybutton white text-uppercase"><strong>JETZT PROJEKT STARTEN!</strong></a></div>
                </div>


            </div>
        </section>
        <section class="container cards">
            <div class="hidden-xs hidden-sm">
                <div class="row">
                    <div class="col-md-4 card wraps">
                        <div class="card-content left">
                            <h1 class="cl-titil-28">Verkürzte Entwicklungszeit und Produktionsvereinfachung</h1>
                            <p class="cl-sans-16-22">Dank Rapid Prototyping von einer durchschnittlichen Entwicklungszeit von 10 Jahren zu 3 bis 4 Jahren.</P>
                        </div>
                        <div class="lefttriangle"></div>
                    </div>
                    <div class="col-md-4 card wraps">
                        <div class="card-content mid">
                            <h1 class="cl-titil-28">Einsparungen bei Produktionszeit und -kosten </h1>
                            <p class="cl-sans-16-22">Angewendete additive Fertigungsverfahren
                                sind in der Lage ohne individuell angefertigte Werkzeuge fertige Bauteile direkt aus 3D-CAD-Daten herzustellen. Produktionskosten und -zeit werden drastisch reduziert.</P>
                        </div>
                        <div class="midtriangle">
                        </div>
                    </div>
                    <div class="col-md-4 card wraps">
                        <div class="card-content right">
                            <h1 class="cl-titil-28">Produktindividualisierung</h1>
                            <p class="cl-sans-16-22">Rapid Manufacturing Verfahren sind in der Lage, flexibel und teilweise sogar simultan Bauteile für verschiedene Produktvarianten in variablen Stückzahlen zu fertigen.</P>
                        </div>
                        <div class="righttriangle">
                        </div>
                    </div>
                </div>
            </div>
            <div class="visible-xs visible-sm">
                <div class="col-md-4 card">
                    <div class="card-content">
                        <h1 class="cl-titil-40">Verkürzte Entwicklungszeit und Produktionsvereinfachung</h1>
                        <p class="cl-titil-20">Dank Rapid Prototyping von einer durchschnittlichen Entwicklungszeit von 10 Jahren zu 3 bis 4 Jahren..</P>
                    </div>
                </div>
                <div class="col-md-4 card">
                    <div class="card-content">
                        <h1 class="cl-titil-40">Einsparungen bei Produktionszeit und -kosten</h1>
                        <p class="cl-titil-16-22">Angewendete additive Fertigungsverfahren
                            sind in der Lage ohne individuell angefertigte Werkzeuge fertige Bauteile direkt aus 3D-CAD-Daten herzustellen. Produktionskosten und -zeit werden drastisch reduziert.</P>
                    </div>
                </div>
                <div class="col-md-4 card bottom" id="bots-top">
                    <div class="card-content">
                        <h1 class="cl-titil-40">Produktindividualisierung</h1>
                        <p class="cl-titil-16-22">Rapid Manufacturing Verfahren sind in der Lage, flexibel und teilweise sogar simultan Bauteile für verschiedene Produktvarianten in variablen Stückzahlen zu fertigen.</P>
                    </div>
                    <div class="bottom-xss" id="bots-bottom">
                    </div>
                </div>
            </div>
            <div class="text-center col-md-10 col-md-offset-1 cl-sans-20 after-cards" ><strong>Die Auswahl des optimalen Rapid Prototyping oder Rapid Manufacturing Verfahrens stellt insbesondere kleine
                    und mittelständische Unternehmen vor große Herausforderungen. Vor allem die Vielzahl an verschiedenen Verfahren und fehlende Erfahrungswerte stellen Einstiegsbarrieren dar.</strong></div>
        </section>
    </div>
    <section class="rapid-text">
        <div class="container">
            <div class="col-md-10 col-md-offset-1">
                <h1 class="cl-titil-40 text-center">Einfach und schnell das passende Rapid Prototyping und Rapid Manufacturing Verfahren finden.</h1>
            </div><div class="col-md-2"></div>
            <div class="col-md-12">
                <p class="cl-sans-16-22">Der erste Schritt zu Ihrem Bauteil liegt in einem 3D-CAD-Modell, das Sie auf der KREATIZE-Produktionsplattform hochladen. Ihre 3D-CAD-Daten müssen dabei im STL-Format sein, sodass wir Ihre Konstruktionsda-ten bestmöglich in unser System importieren können. Schon beim Hochladen Ihrer Daten beginnt der Smart Project Realizer im Hintergrund Ihre 3D-CAD-Daten zu analysieren und passende Fertigungsverfahren zu prüfen. Basierend auf Ihren weiteren Eingaben zu Werkstoff, Lieferzeit und Stückzahl ermittelt die intelligente Software in Echtzeit die voraussichtliche Lieferzeit und eine Kostenschätzung Ihres gewünschten Bauteils. Anschließend erhalten Sie eine Übersicht aller passenden Fertigungsdienstleister, um mit einem Mausklick Ihr Bauteil sofort beim Anbieter Ihrer Wahl zu bestellen.</p>
            </div>
            <div class="col-md-12" style="padding-left: 20px; padding-right: 20px;">
                <p class="cl-titil-20    text-center"><strong>KREATIZE® bietet über 20 verschiedene Fertigungsverfahren, die größte Werkstoffauswahl und besitzt das größte Netzwerk von Fertigungsdienstleister.</strong> Sie erreichen unseren Kundenberater Sellamawe Woldemariam per E-Mail unter <strong>sw@kreatize.com</strong> und telefonisch unter <strong>+49 (0)30 70 24 88 18</strong></p>
            </div>
            <div class="col-md-12 text-center" style="margin-top: 60px; margin-bottom: 20px;"><a href="/signup.html" class="mybutton white text-uppercase"><strong>JETZT ANMELDEN!</strong></a></div>
        </div>
    </section>
    <section class="rapid-three-blocks services">
        <div class="container manu-process" >

            <div class="col-md-12" >
                <div role="tabpanel" >
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs nav-pills" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#panel-1" id="panel-14" aria-controls="panel-1" role="tab" data-toggle="tab" class="cl-titil-28 clear-click">Rapid Prototyping</a>
                        </li><br class="visible-xs visible-sm">
                        <li role="presentation">
                            <a href="#panel-2" aria-controls="panel-2" role="tab" data-toggle="tab" class="cl-titil-28 clear-click">Rapid Manufacuring</a>
                        </li><br class="visible-xs visible-sm">
                        <li role="presentation">
                            <a href="#panel-3" id="panel-34" aria-controls="panel-3" role="tab" data-toggle="tab" class="cl-titil-28">Anwendungsbeispiele</a>
                        </li><br class="visible-xs visible-sm">
                    </ul>
                    <!-- Tab panels -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="panel-1" >
                            <div class="col-md-12 tab-rapid">

                                <div class="col-md-8">
                                    <div class="rap-tab-1">
                                        <div class="col-md-12">
                                            <h1 class="cl-titil-40" >KREATIZE® und die TU Graz</h1>
                                            <h3 class="cl-titil-20"><strong>Neustes Rapid Prototyping Verfahren. 70% Kostenersparnis.</strong></h3>
                                            <p class="cl-sans-16-22">Mit einem der neusten Rapid Prototyping Verfahren in Kombination mit Feinguss unterstützt KREATIZE® das Institut für Hydraulische Strömungsmaschinen (HFM) der Technischen Universität Graz bei der Realisierung von technischen Funktionsbauteilen. Dadurch konnte eine Zeitersparnis von 50% und eine Kostenersparnis von über 70% bei der Realisierung von Pumpenbauteilen bei einer Lieferzeit von 5 Wochen erzielt werden.</p>
                                        </div>
                                        <div class="col-md-11">
                                            <p class="cl-titil-20">„In sehr kurzer Zeit wurde über KREATIZE® das passende Fertigungsverfahren bzw. der passende Partner für die anspruchsvolle Prototypenfertigung gefunden.
                                                Der ganze Ablauf war einfach, schnell und zuverlässig.“ </p>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-md-offset-5"><p class="cl-sans-16-22" ">Dipl. Ing. Markus Mosshammer, TU Graz </p></div>

                                </div>
                                <div class="col-md-4 rap-tab-1 hidden-xs hidden-sm" id="supplier-3-block" >
                                    <div class="col-md-12 rap-tab-1" style="display: table;">
                                        <div style="display: table-cell; vertical-align: middle;">
                                            <div class="col-xs-8 col-xs-offset-2 col-sm-8 col-sm-offset-2 col-md-9">
                                                <img src="/wp-content/themes/kreatize/img/Img_01.png" alt="">
                                            </div>
                                            <div class="visible-xs visible-sm col-xs-12 col-sm-12 text-center" style="padding-top: 20px;">
                                                <p class="cl-sans-16-22" ">Laufrad – Rapid Prototyping, Stahlfeinguss </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 hidden-xs hidden-sm"><p class="cl-sans-16-22" ">Laufrad – Rapid Prototyping, Stahlfeinguss </p></div>
                                </div>

                            </div>
                        </div><!--End panel-1  -->

                        <div role="tabpanel" class="tab-pane" id="panel-2" >
                            <div class="col-md-12">
                                <div class="col-md-8">
                                    <h1 class="cl-titil-40 wraps" >KREATIZE® FERTIGUNGSVERFAHREN IM ÜBERBLICK!</h1>
                                    <div class="col-md-12">
                                        <p class="cl-sans-16-22">KREATIZE® bietet durch seine Fertigungspartner additive und subtraktive Fertigungsverfahren sowie Gussverfahren an. Wir helfen Ihnen dabei, Ihre perfekte Produktionsmöglichkeit zu finden. Durch die zahlreichen Fertigungspartner von KREATIZE® bietet sich Ihnen die Möglichkeit aus einer Vielzahl von verschiedenen Metallen, Kunststoffen und weiteren Werkstoffen den perfekten für Ihr Produkt zu wählen.</p>
                                    </div>
                                    <h3 class="cl-titil-20"><strong>Einfach 3D-CAD-Modell hochladen!</strong></h3>
                                    <div class="but-div">
                                        <a href="/signup.html" class="mybutton text-uppercase cl-titil-20 white"><strong> JETZT PROJEKT STARTEN!</strong></a>
                                    </div>
                                </div>
                                <div class="col-md-4 lins-rapid-block">
                                    <a href="/unsere-verfahren.html#panel-1" class='links-rapid'>
                                        <div class="col-md-6">
                                            <img src="/wp-content/themes/kreatize/img/img-selektives-laserschmelzen.svg" alt="">
                                            <p class="text-center cl-sans-16-22">Additive Fertigung/ 3D-Druck</p>
                                        </div>
                                    </a>
                                    <a href="/unsere-verfahren.html#panel-2" class='links-rapid'>
                                        <div class="col-md-6">
                                            <img src="/wp-content/themes/kreatize/img/img-cnc-frasen.svg" alt="">
                                            <p class="text-center cl-sans-16-22">CNC-Fräsen</p>
                                        </div>
                                    </a>
                                    <a href="/unsere-verfahren.html#panel-3" class='links-rapid'>
                                        <div class="col-md-6 col-md-offset-3">
                                            <img src="/wp-content/themes/kreatize/img/img-guss.svg" alt="">
                                            <p class="text-center cl-sans-16-22">Guss</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div><!--End panel-2  -->

                        <div role="tabpanel" class="tab-pane" id="panel-3" >
                            <div class="col-md-12">
                                <div class="col-md-6 third-pane-1-1" >
                                    <div class="col-md-12 third-pane-1-2" >
                                        <div class="h1-div"><h1 class="cl-titil-40 title-h1">Rapid Prototyping mit Additiver Fertigungs</h1></div>
                                        <p class="cl-sans-16-22 contetnt-p">Mit einem der neusten Rapid Prototyping Verfahren in Kombination mit Feinguss unterstützt KREATIZE® das Institut für Hydraulische Strömungsmaschinen (HFM) der Technischen Universität Graz bei der Realisierung von technischen Funktionsbauteilen. Dadurch konnte eine Zeitersparnis von 50% und eine Kostenersparnis von über 70% bei der Realisierung von Pumpenbauteilen bei einer Lieferzeit von 5 Wochen erzielt werden.</p>
                                    </div>
                                    <table class="table ">
                                        <thead class="cl-titil-16-22">
                                        <tr>
                                            <th class="cols-1 wraps"></th>
                                            <th class="cols-2 ">Selektives Lasersintern</th>
                                            <th class="cols-3 ">CNC-Fräsen</th>
                                            <th class="cols-4 wraps">Vorteile des Selektiven Lasersintern</th>
                                        </tr>
                                        </thead>
                                        <tbody class="cl-titil-16-22">
                                        <tr>
                                            <th class="cols-1 ">Werkstoff</th>
                                            <td class="cols-2 wraps">PA12</td>
                                            <td class="cols-3 wraps">PVC</td>
                                            <td class="cols-4 wraps"></td>
                                        </tr>
                                        <tr>
                                            <th class="cols-1 ">Lieferzeit</th>
                                            <td class="cols-2 wraps">2 Tage</td>
                                            <td class="cols-3 wraps">3 Wochen</td>
                                            <td class="cols-4 wraps">90% Zeitersparnis</td>
                                        </tr>
                                        <tr>
                                            <th class="cols-1 wraps">Gesamtkosten</th>
                                            <td class="cols-2 wraps">€ 340</td>
                                            <td class="cols-3 wraps">€ 600</td>
                                            <td class="cols-4 wraps">43% Kostenersparnis</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <div class="col-md-6 col-md-offset-6 rapid-effect">
                                    </div>
                                </div>
                                <div class="col-md-6 third-pane-2-1" >
                                    <div class="col-md-12 third-pane-2-2" >
                                        <div class="h1-div"><h1 class="cl-titil-40 title-h1">Rapid Prototyping als Kombination von Additiver Fertigung & Feinguss</h1></div>
                                        <p class="cl-sans-16-22 contetnt-p">Nem ea nulparis endis a qui vero ommolup tinciae et eos nihit diandae. Ut eumqui volenim porerrovit volupicia nos rere offic tem que vent. Vollabo rehendellab idis rectent incturi busant.</p>
                                    </div>
                                    <table class="table ">
                                        <thead class="cl-titil-16-22">
                                        <tr>
                                            <th class="cols-1 wraps"></th>
                                            <th class="cols-2 ">Selektives Lasersintern</th>
                                            <th class="cols-3 ">CNC-Fräsen</th>
                                            <th class="cols-4 wraps">Vorteile des Selektiven Lasersintern</th>
                                        </tr>
                                        </thead>
                                        <tbody class="cl-titil-16-22">
                                        <tr>
                                            <th class="cols-1 ">Werkstoff</th>
                                            <td class="cols-2 wraps">PA12</td>
                                            <td class="cols-3 wraps">PVC</td>
                                            <td class="cols-4 wraps"></td>
                                        </tr>
                                        <tr>
                                            <th class="cols-1 ">Lieferzeit</th>
                                            <td class="cols-2 wraps">2 Tage</td>
                                            <td class="cols-3 wraps">3 Wochen</td>
                                            <td class="cols-4 wraps">90% Zeitersparnis</td>
                                        </tr>
                                        <tr>
                                            <th class="cols-1 wraps">Gesamtkosten</th>
                                            <td class="cols-2 wraps">€ 340</td>
                                            <td class="cols-3 wraps">€ 600</td>
                                            <td class="cols-4 wraps">43% Kostenersparnis</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <div class="col-md-6 col-md-offset-6 rapid-effect">
                                    </div>
                                </div>
                            </div>

                        </div><!--End panel-3  -->
                    </div><!--End tab-content  -->
                </div><!--End tabpanel  -->
            </div>

        </div>
    </section>

<?php get_sidebar(); ?>
<?php get_footer(); ?>