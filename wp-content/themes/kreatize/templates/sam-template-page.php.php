<?php /* Template Name: Sam Page */ ?>
<?php if (!defined('ABSPATH')) {exit;} ?>

<?php
require_once get_stylesheet_directory().'/core/App.php';
use kreatize\App as App;
$kreatize = App::getInstance();
$pageOptions =  $kreatize->service('SamPageOptions');
$wpGlobus = $kreatize->service('WPGlobus');
?>

<?php get_header(); ?>
    <div id="content" class="sign-up" >
        <section class="sam" id="intro" style="
    background: linear-gradient(rgba(48, 182, 216, 0.85), rgba(48, 182, 216, 0.85)), url(<?php echo $pageOptions->headerImage(); ?>) no-repeat center center;
    background-size: cover;
">
            <div class="container text-center" >
                <div class="form-group col-md-8 col-md-offset-2">
                    <h1 class="cl-titil-40"><strong><?php echo $pageOptions->headerTitle(1); ?></strong><br><?php echo $pageOptions->headerTitle(2); ?></h1>
                </div>
                <?php
                if ($wpGlobus->isEnglish()) {
                    echo do_shortcode('[contact-form-7 id="188" title="Sam Page form English" html_class="form intro-form"]');
                } else {
                    echo do_shortcode('[contact-form-7 id="189" title="Sam Page form Dutsch" html_class="form intro-form"]');
                }
                ?>
                <div class="robot-head hidden-xs hidden-sm" >


                    <div class="say-head cl-titil-16-22"><?php echo $pageOptions->samSaisInHeader(); ?></div>
                    <img src="/wp-content/themes/kreatize/img/sam_white.png" class="white-sam" width="40%">
                </div>

            </div>
        </section>
        <section class="container cards">
            <div class="hidden-xs hidden-sm">
                <div class="row">
                    <div class="col-md-4 card">
                        <div class="card-content left">
                            <h1 class="cl-titil-28"><?php echo $pageOptions->cardTitle(1); ?></h1>
                            <p class="cl-sans-16-22"><?php echo $pageOptions->cardText(1); ?></P>
                        </div>
                        <div class="lefttriangle"></div>
                    </div>
                    <div class="col-md-4 card">
                        <div class="card-content mid">
                            <h1 class="cl-titil-28"><?php echo $pageOptions->cardTitle(2); ?></h1>
                            <p class="cl-sans-16-22"><?php echo $pageOptions->cardText(3); ?></P>
                        </div>
                        <div class="midtriangle">
                        </div>
                    </div>
                    <div class="col-md-4 card">
                        <div class="card-content right">
                            <h1 class="cl-titil-28"><?php echo $pageOptions->cardTitle(3); ?></h1>
                            <p class="cl-sans-16-22"><?php echo $pageOptions->cardText(3); ?></P>
                        </div>
                        <div class="righttriangle">
                        </div>
                    </div>
                </div>
            </div>
            <div class="visible-xs visible-sm">
                <div class="col-md-4 card">
                    <div class="card-content">
                        <h1 class="cl-titil-40"><?php echo $pageOptions->cardTitle(1); ?></h1>
                        <p class="cl-titil-20"><?php echo $pageOptions->cardText(1); ?></P>
                    </div>
                </div>
                <div class="col-md-4 card">
                    <div class="card-content">
                        <h1 class="cl-titil-40"><?php echo $pageOptions->cardTitle(2); ?></h1>
                        <p class="cl-titil-16-22"><?php echo $pageOptions->cardText(2); ?></P>
                    </div>
                </div>
                <div class="col-md-4 card bottom" id="bots-top">
                    <div class="card-content">
                        <h1 class="cl-titil-40"><?php echo $pageOptions->cardTitle(3); ?></h1>
                        <p class="cl-titil-16-22"><?php echo $pageOptions->cardText(3); ?></P>
                    </div>
                    <div class="bottom-xss" id="bots-bottom">
                    </div>
                </div>
            </div>
        </section>


        <section class="container text-and-robot">
            <h1 class="cl-titil-40 text-center"><?php echo $pageOptions->footerTitle(); ?></h1>
            <div class="col-md-8">
                <p class="cl-titil-20"><?php echo $pageOptions->footerText(); ?></p>
                <div class="col-xs-12" style="padding: 0px;"><a href="#" class="mybutton white btn-roll"><strong><?php echo $pageOptions->footerButtonText(); ?></strong></a></div>
            </div>
            <div class="col-md-4 hidden-xs">
                <div class="robot-say" >
                    <div class="say cl-titil-16-22"><?php echo $pageOptions->samSaisInFooter(); ?></div>
                    <div class="say-triangle"></div>
                </div>
                <img class="black-sam" src="/wp-content/themes/kreatize/img/sam_dark.png">
            </div>
        </section>



    </div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>