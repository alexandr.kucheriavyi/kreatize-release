<?php /* Template Name: Sign Up Page */ ?>
<?php if (!defined('ABSPATH')) {exit;} ?>

<?php
require_once get_stylesheet_directory().'/core/App.php';
use kreatize\App as App;
$kreatize = App::getInstance();
$pageOptions =  $kreatize->service('SignupPageOptions');
$HomepageOptions =  $kreatize->service('HomePageOptions');
$wpGlobus = $kreatize->service('WPGlobus');
?>

<?php get_header(); ?>
    <div id="content" class="sign-up" >
        <section class="intro" id="intro" style="background: linear-gradient(rgba(45, 83, 111, 0.7), rgba(45, 83, 111, 0.7)), url(<?php echo $pageOptions->headerImage() ;?>) no-repeat center center; background-size: cover;">
            <div class="container text-center">
                <div class="form-group col-md-8 col-md-offset-2">
                    <h1 class="cl-titil-40"><?php echo $pageOptions->headerText() ;?></h1>
                </div>
                <?php
                if ($wpGlobus->isEnglish()) {
                    echo do_shortcode('[contact-form-7 id="184" title="SignupPage form English" html_class="form intro-form"]');
                } else {
                    echo do_shortcode('[contact-form-7 id="185" title="SignupPage form Deutsch"  html_class="form intro-form"]');
                }
                ?>
            </div>
        </section>
        <section class="container cards">
            <div class="hidden-xs hidden-sm">
                <div class="row">
                    <div class="col-md-4 card">
                        <div class="card-content left">
                            <h1 class="cl-titil-28"><?php echo $pageOptions->cardTitle(1) ;?></h1>
                            <p class="cl-sans-16-22"><?php echo $pageOptions->cardText(1) ;?></P>
                        </div>
                        <div class="lefttriangle"></div>
                    </div>
                    <div class="col-md-4 card">
                        <div class="card-content mid">
                            <h1 class="cl-titil-28"><?php echo $pageOptions->cardTitle(2) ;?></h1>
                            <p class="cl-sans-16-22"><?php echo $pageOptions->cardText(2) ;?></P>
                        </div>
                        <div class="midtriangle">
                        </div>
                    </div>
                    <div class="col-md-4 card">
                        <div class="card-content right">
                            <h1 class="cl-titil-28"><?php echo $pageOptions->cardTitle(3) ;?></h1>
                            <p class="cl-sans-16-22"><?php echo $pageOptions->cardText(3) ;?></P>
                        </div>
                        <div class="righttriangle">
                        </div>
                    </div>
                </div>
            </div>
            <div class="visible-xs visible-sm">
                <div class="col-md-4 card">
                    <div class="card-content">
                        <h1 class="cl-titil-40"><?php echo $pageOptions->cardTitle(1) ;?></h1>
                        <p class="cl-titil-20"><?php echo $pageOptions->cardText(1) ;?></P>
                    </div>
                </div>
                <div class="col-md-4 card">
                    <div class="card-content">
                        <h1 class="cl-titil-40"><?php echo $pageOptions->cardTitle(2) ;?></h1>
                        <p class="cl-titil-16-22"><?php echo $pageOptions->cardText(2) ;?></P>
                    </div>
                </div>
                <div class="col-md-4 card bottom" id="bots-top">
                    <div class="card-content">
                        <h1 class="cl-titil-40"><?php echo $pageOptions->cardTitle(3) ;?></h1>
                        <p class="cl-titil-16-22"><?php echo $pageOptions->cardText(3) ;?></P>
                    </div>
                </div>
                <div class="bottom-xss" id="bots-bottom">
                </div>
            </div>
        </section>


        <section class="container profile">
            <h1 class="cl-titil-20"><?php echo $pageOptions->cardsBottomText() ;?></h1>
        </section>

        <section class="customer-testimony">
            <div class="container">
                <div class="row">
                    <div class="col-md-2 hidden-xs hidden-sm" style="padding:0px;">
                        <img class="img-circle pull-right" src="<?php echo $HomepageOptions->customersSayPhoto(); ?>" alt="Jan Roosen"></img>
                    </div>
                    <div class="col-md-8 text-center">
                        <h1 class="text-center cl-titil-40" ><?php echo $HomepageOptions->customersSayTitle(); ?></h1>
                        <p class="cl-titil-20"><?php echo $HomepageOptions->customersSayQuote(); ?></p>
                        <br/><img class="logo-customer hidden-xs" src="<?php echo $HomepageOptions->customersSayLogo(); ?>"><p class="cl-sans-16-22"><?php echo $HomepageOptions->customersSayInformation(); ?></p>
                        <div class="col-md-2 visible-xs visible-sm text-center">
                            <img class="img-circle" src="<?php echo $HomepageOptions->customersSayPhoto(); ?>" alt="Jan Roosen">
                        </div>
                        <br class="hidden-sm hidden-xs"/>
                        <a href="<?php echo $HomepageOptions->customersSayButtonUrl(); ?>" class="btn btn-primary text-uppercase btn-button"><strong><?php echo $HomepageOptions->customersSayButtonText(); ?></strong></a>
                    </div>
                    <div class="col-md-2 text-center">
                    </div>
                </div>
            </div>
        </section>

    </div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>