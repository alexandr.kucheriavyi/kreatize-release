<?php /* Template Name: Supplier Prototyping Page */ ?>
<?php if (!defined('ABSPATH')) {exit;} ?>

<?php
require_once get_stylesheet_directory().'/core/App.php';
use kreatize\App as App;
$kreatize = App::getInstance();
$pageOptions =  $kreatize->service('SupplierPageOptions');
$homepageOptions =  $kreatize->service('HomePageOptions');
$wpGlobus = $kreatize->service('WPGlobus');
?>

<?php get_header(); ?>
    <div id="content" class="supplier" lang="en">
        <section class="intro" id="intro" style="
    background: linear-gradient(rgba(45, 83, 111, 0), rgba(45, 83, 111, 0)), url(<?php echo $pageOptions->headerImage(); ?>) no-repeat center center;
    background-size: cover;
">
            <div class="container text-center">
                <h1 class="hidden-xs cl-titil-40"><?php echo $pageOptions->headerText(); ?></h1>
                <?php
                if ($wpGlobus->isEnglish()) {
                    echo do_shortcode('[contact-form-7 id="186" title="SupplierPage form English" html_class="form intro-form"]');
                } else {
                    echo do_shortcode('[contact-form-7 id="187" title="SupplierPage form Deutsch" html_class="form intro-form"]');
                }
                ?>
            </div>
        </section>

        <section class="container cards">
            <div class="hidden-xs hidden-sm">
                <div class="row">
                    <div class="col-md-4 card">
                        <div class="card-content left" id="left">
                            <h1 class="cl-titil-28"><?php echo $pageOptions->cardTitle(1); ?></h1>
                            <p class="cl-sans-16-22"><?php echo $pageOptions->cardText(1); ?></P>
                        </div>
                        <div class="lefttriangle">
                        </div>
                    </div>
                    <div class="col-md-4 card">
                        <div class="card-content mid" id='mid'>
                            <h1 class="cl-titil-28"><?php echo $pageOptions->cardTitle(2); ?></h1>
                            <p class="cl-sans-16-22"><?php echo $pageOptions->cardText(2); ?></P>
                        </div>
                        <div class="midtriangle">
                        </div>
                    </div>
                    <div class="col-md-4 card">
                        <div class="card-content right" id="right">
                            <h1 class="cl-titil-28"><?php echo $pageOptions->cardTitle(3); ?></h1>
                            <p class="cl-sans-16-22"><?php echo $pageOptions->cardText(3); ?></P>
                        </div>
                        <div class="righttriangle">
                        </div>
                    </div>
                </div>

            </div>
            <div class="visible-xs visible-sm">
                <div class="col-md-4 card">
                    <div class="card-content">
                        <h1 class="cl-titil-28"><?php echo $pageOptions->cardTitle(1); ?></h1>
                        <p class="cl-sans-16-22"><?php echo $pageOptions->cardText(1); ?></P>
                    </div>
                </div>
                <div class="col-md-4 card">
                    <div class="card-content">
                        <h1 class="cl-titil-28"><?php echo $pageOptions->cardTitle(2); ?></h1>
                        <p class="cl-sans-16-22"><?php echo $pageOptions->cardText(2); ?></P>
                    </div>
                </div>
                <div class="col-md-4 card bottom" id="bots-top">
                    <div class="card-content">
                        <h1 class="cl-titil-28"><?php echo $pageOptions->cardTitle(3); ?></h1>
                        <p class="cl-sans-16-22"  ><?php echo $pageOptions->cardText(3); ?></P>
                    </div>
                    <div class="bottom-xss" id="bots-bottom">
                    </div>
                </div>
            </div>
        </section>

        <section class="container profile">
            <div class="hidden-xs hidden-sm ">
                <h1 class="cl-titil-20"><?php echo $pageOptions->cardsSectionText(); ?></h1>
                <div class="col-md-4 items" id="supl-items">
                    <div data-image-url="<?php echo $pageOptions->profileItemImage(1); ?>" class="item-content" id="item1" >
                        <h1 class="cl-titil-20"><?php echo $pageOptions->profileItemTitle(1); ?></h1>
                        <p class="cl-titil-16-22"><?php echo $pageOptions->profileItemText(1); ?></P>
                    </div>
                    <div data-image-url="<?php echo $pageOptions->profileItemImage(2); ?>" class="item-content"  id="item2" >
                        <h1 class="cl-titil-20"><?php echo $pageOptions->profileItemTitle(2); ?></h1>
                        <p class="cl-titil-16-22"><?php echo $pageOptions->profileItemText(2); ?></P>
                    </div>
                    <div data-image-url="<?php echo $pageOptions->profileItemImage(3); ?>" class="item-content" id="item3" >
                        <h1 class="cl-titil-20"><?php echo $pageOptions->profileItemTitle(3); ?></h1>
                        <p class="cl-titil-16-22"><?php echo $pageOptions->profileItemText(3); ?></P>
                    </div>
                </div>
                <div class="col-md-8" id="supplier-3-block" style="display: table;">
                    <div style="display: table-cell; vertical-align: middle;">
                        <img id="profile-img" class="supl-image"  width="100%" src="<?php echo $pageOptions->profileItemImage(1); ?>" style="border-radius: 5px 5px 0 0; border: 1px #CCCCCC solid;">
                    </div>
                </div>

            </div>
            <div class="visible-xs visible-sm">
                <div class="col-md-5 items">
                    <h1><?php echo $pageOptions->cardsSectionMobileText(); ?></h1>
                    <div class="item-content" id="item4" onmouseover="onMouseItemCom(0)">
                        <h1 class="cl-titil-20"><?php echo $pageOptions->profileItemTitle(1); ?></h1>
                        <p class="cl-titil-16-22"><?php echo $pageOptions->profileItemText(1); ?></P>
                    </div>
                    <div  class="item-content" id="item5" onmouseover="onMouseItemCom(1)">
                        <h1 class="cl-titil-20"><?php echo $pageOptions->profileItemTitle(2); ?></h1>
                        <p class="cl-titil-16-22"><?php echo $pageOptions->profileItemText(2); ?></P>
                    </div>
                    <div  class="item-content" id="item6" onmouseover="onMouseItemCom(2)">
                        <h1 class="cl-titil-20"><?php echo $pageOptions->profileItemTitle(3); ?></h1>
                        <p class="cl-titil-16-22"><?php echo $pageOptions->profileItemText(3); ?></P>
                    </div>
                </div>
            </div>
        </section>

        <section class="container-fluid divg">
            <div class="container vertical-center">
                <div class="col-md-6 col-sm-6 div-cell divg-block">
                    <h2 class="cl-titil-40"><?php echo $pageOptions->advantagesItemTitle(1); ?></h2>
                    <p class="cl-sans-16-22" style="margin-bottom: 20px;"><?php echo $pageOptions->advantagesItemText(1, 1); ?></p>
                    <p class="cl-sans-16-22"><?php echo $pageOptions->advantagesItemText(1, 2); ?></p>
                    <?php if ($pageOptions->isEnabledAdvantagesButton(1)) {?>
                    <button type="submit" class="btn btn-primary navbar-btn text-uppercase btn-roll"><strong><?php echo $pageOptions->advantagesItemButtonText(1); ?></strong></button>
                    <?php } ?>
                </div>
                <div class="col-md-6 col-sm-6 div-cell">
                    <img width="100%" src="<?php echo $pageOptions->advantagesItemImage(1); ?>" alt="img-reporting">
                    <p class="text-center screen-title cl-sans-16 hidden-xs"><?php echo $pageOptions->advantagesItemImageDescription(1); ?></p>
                </div>
            </div>
        </section>

        <section class="container-fluid divw">
            <div class="container vertical-center">
                <div class="col-md-6 col-sm-6 div-cell visible-xs">
                    <h1 class="cl-titil-40"><?php echo $pageOptions->advantagesItemTitle(2); ?></h1>
                    <p class="cl-sans-16-22" style="margin-bottom: 20px;"><?php echo $pageOptions->advantagesItemText(2, 1); ?></p>
                    <p class="cl-sans-16-22"><?php echo $pageOptions->advantagesItemText(2, 2); ?></p>
                    <?php if ($pageOptions->isEnabledAdvantagesButton(2)) {?>
                    <button type="submit" class="btn btn-primary navbar-btn text-uppercase btn-roll" style="padding: 6px 12px;"><strong><?php echo $pageOptions->advantagesItemButtonText(2); ?></strong></button>
                    <?php } ?>
                </div>
                <div class="col-md-6 col-sm-6 div-cell xs-div-cell">
                    <img width="100%" src="<?php echo $pageOptions->advantagesItemImage(2); ?>"" id="imgright" alt="img-order" >
                    <p class="text-center screen-title cl-sans-16 hidden-xs"><?php echo $pageOptions->advantagesItemImageDescription(2); ?></p>
                </div>

                <div class="col-md-5 col-sm-6 div-cell hidden-xs divw-block"  >
                    <h2 class="cl-titil-40"><?php echo $pageOptions->advantagesItemTitle(2); ?></h2>
                    <p  class="cl-sans-16-22" style="margin-bottom: 20px;"><?php echo $pageOptions->advantagesItemText(2, 1); ?></p>
                    <p class="cl-sans-16-22" style="margin-bottom: 30px;"><?php echo $pageOptions->advantagesItemText(2, 2); ?></p>
                    <?php if ($pageOptions->isEnabledAdvantagesButton(2)) {?>
                    <div class="col-md-12 providers-link " style="padding-left: 0px;">
                        <button type="submit" class="btn btn-primary navbar-btn text-uppercase btn-roll"><strong><?php echo $pageOptions->advantagesItemButtonText(2); ?></strong></button>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </section>

        <section class="container-fluid divg">
            <div class="container vertical-center">
                <div class="col-md-6 col-sm-6 div-cell divg-block">
                    <h2 class="cl-titil-40"><?php echo $pageOptions->advantagesItemTitle(3); ?></h2>
                    <p class="cl-sans-16-22" style="margin-bottom: 20px;"><?php echo $pageOptions->advantagesItemText(3, 1); ?></p>
                    <p class="cl-sans-16-22"><?php echo $pageOptions->advantagesItemText(3, 2); ?></p>
                    <?php if ($pageOptions->isEnabledAdvantagesButton(3)) {?>
                    <button type="submit" class="btn btn-primary navbar-btn text-uppercase btn-roll"><strong><?php echo $pageOptions->advantagesItemButtonText(3); ?></strong></button>
                    <?php } ?>
                </div>
                <div class="col-md-6 col-sm-6 div-cell">
                    <img width="100%" src="<?php echo $pageOptions->advantagesItemImage(3); ?>" alt="img-price-calculation">
                    <p class="text-center screen-title cl-sans-16 hidden-xs"><?php echo $pageOptions->advantagesItemImageDescription(3); ?></p>
                </div>
            </div>
        </section>

        <section class="container partners">
            <h1 class="text-center cl-titil-40"><?php echo $homepageOptions->partnersTitle(); ?></h1>
            <div>
                <div class="col-md-4 partner-content">
                    <div class="item">
                        <div class="partner-img">
                            <img src="<?php echo $homepageOptions->partnersLogo(1); ?>">
                        </div>
                        <div class="partner-texts">
                            <span class="quote">&#8221;</span><p class="cl-sans-16-22"><?php echo $homepageOptions->partnersQuote(1); ?></p><span class="quote-ende ">&#8220;</span>
                            <p class="author cl-sans-16-22"><?php echo $homepageOptions->partnersAuthor(1); ?></p>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 partner-content">
                    <div class="item">
                        <div class="partner-img">
                            <img src="<?php echo $homepageOptions->partnersLogo(2); ?>" class="gray">
                        </div>
                        <div class="partner-texts mid">
                            <span class="quote">&#8221;</span><p class="cl-sans-16-22"><?php echo $homepageOptions->partnersQuote(2); ?></p><span class="quote-ende">&#8220;</span>
                            <p class="author cl-sans-16-22"><?php echo $homepageOptions->partnersAuthor(2); ?></p>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 partner-content">
                    <div class="item">
                        <div class="partner-img">
                            <img src="<?php echo $homepageOptions->partnersLogo(3); ?>">
                        </div>
                        <div class="partner-texts">
                            <span class="quote">&#8221;</span><p class="cl-sans-16-22"><?php echo $homepageOptions->partnersQuote(3); ?></p><span class="quote-ende">&#8220;</span>
                            <p class="author  cl-sans-16-22"><?php echo $homepageOptions->partnersAuthor(3); ?></p>
                        </div>
                    </div>
                </div>

            </div>
            <!--end of partner testimony-->
            <div class="handling col-md-12">
                <div class="col-md-3 col-sm-6 col-xs-6 text-center">
                    <div data-persent="<?php echo $homepageOptions->partnersResultPercent(1, true); ?>" class="progress cl-titil-28" id="progress1"></div>
                    <p class="cl-titil-20"><?php echo $homepageOptions->partnersResultText(1); ?></p>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-6 text-center">
                    <div data-persent="<?php echo $homepageOptions->partnersResultPercent(2, true); ?>" class="progress cl-titil-28" id="progress2"></div>
                    <p class="cl-titil-20"><?php echo $homepageOptions->partnersResultText(2); ?></p>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-6 text-center">
                    <div data-persent="<?php echo $homepageOptions->partnersResultPercent(3, true); ?>" class="progress cl-titil-28" id="progress3"></div>
                    <p class="cl-titil-20"><?php echo $homepageOptions->partnersResultText(3); ?></p>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-6 text-center">
                    <div data-persent="<?php echo $homepageOptions->partnersResultPercent(4, true); ?>" class="progress cl-titil-28    " id="progress4"></div>
                    <p class="cl-titil-20"><?php echo $homepageOptions->partnersResultText(4); ?></p>
                </div>
            </div>
            <div class="col-md-12 providers-link text-center">
                <button class="btn btn-primary text-uppercase btn-roll link-btn"><strong><?php echo $homepageOptions->partnersButtonText(); ?></strong></button>
            </div>
        </section>
    </div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>