<?php /* Template Name: Terms and Conditions Page */ ?>
<?php if (!defined('ABSPATH')) {exit;} ?>

<?php
require_once get_stylesheet_directory().'/core/App.php';
use kreatize\App as App;
$kreatize = App::getInstance();
$pageOptions =  $kreatize->service('TermsAndConditionsPageOptions');
$breadcrumbs =  $kreatize->service('Breadcrumbs');
?>

<?php get_header(); ?>
    <div id="content" class="agb">
        <section class="intro" id="intro" style="
    background: linear-gradient(to right, rgba(57, 160, 193, 0.6), rgba(67, 204, 173, 0.7)), url(<?php echo $pageOptions->headerImage(); ?>) no-repeat bottom right;
    background-size: cover;
    background-position: bottom center;
">
            <div class="container text-center">
                <h1 class="cl-titil-65"><?php echo $pageOptions->headerTitle(); ?></h1>
            </div>
        </section>

        <section style="padding-top: 0px;">
            <div class="container fadi-contents">
                <?php echo $breadcrumbs->page();?>
                <div role="tabpanel" class="">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs col-md-3 cl-titil-20" id="tabs" role="tablist" style="padding:0 5px;">
                        <li role="presentation" class="active">
                            <a href="#agb" class="active" aria-controls="agb" role="tab" data-toggle="tab"><?php echo $pageOptions->tabName(1); ?></a>
                        </li>
                        <li role="presentation">
                            <a href="#datenschutz" aria-controls="datenschutz" role="tab" data-toggle="tab"><?php echo $pageOptions->tabName(2); ?></a>
                        </li>
                        <li role="presentation">
                            <a href="#impressum" aria-controls="impressum" role="tab" data-toggle="tab"><?php echo $pageOptions->tabName(3); ?></a>
                        </li>
                    </ul>
                    <!-- Tab panels -->
                    <div class="tab-content col-md-8 cl-sans-16-22" id="my-tab-content">
                        <div role="tabpanel" class="tab-pane active" id="agb">
                            <div class="tab-content-right ">
                                <div class="panel-group " id="accordion" role="tablist" aria-multiselectable="true">
                                    <?php $items = $pageOptions->tabOneItems(); ?>
                                    <?php
                                    if ($items)  {
                                        $isFirst = true;
                                        foreach ($items as $ident => $item)  {
                                            $opened = ($isFirst) ? 'in' : '';
                                            $isFirst = false;
                                            ?>

                                            <div class="panel panel-default">
                                                <div class="panel-heading container" role="tab" id="headingOne<?php echo $ident; ?>">
                                                    <h4 class="panel-title cl-titil-16-22">
                                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne<?php echo $ident; ?>" aria-expanded="true" aria-controls="collapseOne<?php echo $ident; ?>" class="rolling">
                                                            <?php echo $item['title']; ?>
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne<?php echo $ident; ?>" class="panel-collapse collapse <?php echo $opened; ?>" role="tabpanel" aria-labelledby="headingOne<?php echo $ident; ?>">
                                                    <div class="panel-body container">
                                                        <p>
                                                            <?php echo $item['text']; ?>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>

                                            <?php
                                        }
                                    }
                                    ?>

                                </div>

                            </div>
                        </div><!--End panel-1  -->

                        <div role="tabpanel " class="tab-pane" id="datenschutz">
                            <div class="tab-content-right ">
                                <p>
                                    <?php echo $pageOptions->tabTwoText(); ?>
                                </p>
                            </div>
                        </div><!--End panel-2  -->


                        <div role="tabpanel" class="tab-pane" id="impressum">
                            <div class="tab-content-right">
                                <!--<h1>Impressum</h1>-->
                                <p>
                                    <?php echo $pageOptions->tabThreeText(); ?></p>
                            </div>
                        </div><!--End panel-3  -->

                    </div>
        </section>
    </div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>