<?php /* Template Name: Thank you Page */ ?>
<?php if (!defined('ABSPATH')) {exit;} ?>

<?php
require_once get_stylesheet_directory().'/core/App.php';
use kreatize\App as App;
$kreatize = App::getInstance();
$pageOptions =  $kreatize->service('ThankyouPageOptions');
?>

<?php get_header(); ?>
    <div id="content" class="thk">
        <section class="container-fluid">
            <div class="container">
                <div class="col-md-12 ">
                    <h1 class="cl-titil-40 text-center"><?php echo $pageOptions->title(); ?></h1>
                </div>
                <div class="col-md-6 col-md-offset-2 col-sm-8 col-xs-8 thk-block">
                    <p class="cl-titil-20"><?php echo $pageOptions->text(1); ?></p>
                    <p class="cl-titil-20 thk-second-p"><?php echo $pageOptions->text(2); ?></p>
                    <p class="cl-titil-20"><strong><?php echo $pageOptions->text(3); ?></strong></p>
                </div>
            </div>
        </section>
    </div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>